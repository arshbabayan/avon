<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::query()->insert(
            [
                ['prefix' => 'new'],
                ['prefix' => 'profitable-offer'],
                ['prefix' => 'stocks'],
            ]
        );
    }
}
