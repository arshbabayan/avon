<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->insert(
            [
                [
                    'name' => 'Administrator One',
                    'email' => 'admin_one',
                    'password' => Hash::make('admin_111'),
                    'role_id' => 9,
                    'remember_token' => null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Administrator Two',
                    'email' => 'admin_two',
                    'password' => Hash::make('admin_222'),
                    'role_id' => 9,
                    'remember_token' => null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Administrator Three',
                    'email' => 'admin_three',
                    'password' => Hash::make('admin_333'),
                    'role_id' => 9,
                    'remember_token' => null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'name' => 'Administrator Four',
                    'email' => 'admin_four',
                    'password' => Hash::make('admin_444'),
                    'role_id' => 9,
                    'remember_token' => null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]
        );
    }
}
