<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::query()->insert([
            [
                'prefix' => 'am',
                'icon' => 'am.png',
            ],
            [
                'prefix' => 'ru',
                'icon' => 'ru.png',
            ],
            [
                'prefix' => 'en',
                'icon' => 'en.png',
            ]
        ]);
    }
}
