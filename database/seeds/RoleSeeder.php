<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()->insert(
            [
                ['prefix' => 'representative'],
                ['prefix' => 'coordinator'],
                ['prefix' => 'senior_coordinator'],
                ['prefix' => 'youth_leader'],
                ['prefix' => 'leader'],
                ['prefix' => 'senior_leader'],
                ['prefix' => 'youth_director'],
                ['prefix' => 'director'],
                ['prefix' => 'admin'],
            ]
        );
    }
}
