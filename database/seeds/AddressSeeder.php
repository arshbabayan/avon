<?php

use Illuminate\Database\Seeder;
use App\Models\Language;
use App\Models\Address;
use App\Models\AddressTranslations;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addressData = [];
        $percents = [
            0.5,
            2
        ];
        foreach ($percents as $percent){
            array_push($addressData,[
                'percent' => $percent,
            ]);
        }
        Address::query()->insert($addressData);

        $addresses = Address::all();
        $languages = Language::all();
        $translates = [
            1 => [
                1 => 'Ք. Երևան, Ազատության պողոտա 3',
                2 => 'Г. Ереван, азатутян проспект 3',
                3 => 'C. Yerevan, Azatutyan Avenue 3',
            ],
            2 => [
                1 => 'Ք. Ստեփանավան, Բաղրամյան փ., 19/8 շ.',
                2 => 'Г. Степанаван, Баграмяна, 19/8 ш.',
                3 => 'C. Stepanavan, Baghramyan, 19/8.',
            ]
        ];
        $addressesTranslationsData = [];

        foreach ($addresses as $address){
            foreach ($languages as $language){
                array_push($addressesTranslationsData,[
                    'address_id' => $address->id,
                    'language_id' => $language->id,
                    'title' => $translates[$address->id][$language->id]
                ]);
            }
        }

        AddressTranslations::query()->insert($addressesTranslationsData);
    }
}
