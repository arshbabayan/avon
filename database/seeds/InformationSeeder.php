<?php

use Illuminate\Database\Seeder;
use App\Models\Information;
use App\Models\InformationTranslations;
use App\Models\Language;

class InformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $informationData = [];
        $prefixes = [
            'business-model',
            'about-company',
            'payment-orders',
            'delivery-points',
            'home',
            'socials',
            'how-to-join-our-team',
            'how-to-become-a-coordinator',
            'how-to-become-a-regional-manager',
            'terms-of-use',
            'confidentiality',
            'order'
        ];

        foreach ($prefixes as $prefix){
            array_push($informationData,[
                'prefix' => $prefix,
                'info' => json_encode((Object)[])
            ]);
        }
        Information::query()->insert($informationData);

        $information = Information::all();
        $languages = Language::all();
        $informationTranslationsData = [];

        foreach ($information as $item){
            foreach ($languages as $language){
                array_push($informationTranslationsData,[
                    'information_id' => $item->id,
                    'language_id' => $language->id,
                    'info' => json_encode((Object)[])
                ]);
            }
        }

        InformationTranslations::query()->insert($informationTranslationsData);
    }
}
