<?php

use Illuminate\Database\Seeder;
use App\Models\Catalog;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Catalog::query()->create(
            [
                'thumbnail' => 'image-placeholder.png',
                'name' => 'CO0',
                'deadline' => date('Y-m-d', strtotime(date('Y-m-d'). ' + 21 days')),
                'active' => true,
            ]
        );
    }
}
