<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->integer('catalog_id')->nullable();
            $table->integer('status')->default(0);
            $table->float('payed')->default(0);
            $table->float('will_be_pay')->default(0);
            $table->integer('address_id')->default(0);

            $table->float('products_with_discount_total')->default(0);
            $table->float('affordable_discount')->default(0);
            $table->float('products_with_discount_finally_total')->default(0);

            $table->float('products_not_discount_total')->default(0);
            $table->float('products_not_discount_finally_total')->default(0);

            $table->float('products_finally_total')->default(0);
            $table->float('bonus')->default(0);
            $table->float('order_total')->default(0);

            $table->float('transportation_costs')->default(0);
            $table->float('service_fee')->default(0);
            $table->float('packaging')->default(0);
            $table->float('journal')->default(0);

            $table->float('finally_total')->default(0);

            $table->float('used_prepayment')->default(0);
            $table->float('unused_discount')->default(0);
            $table->float('unused_prepayment')->default(0);

            $table->timestamp('in_processed_at')->nullable();
            $table->timestamp('payed_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
