<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('address')->nullable();
            $table->string('passport')->nullable();
            $table->string('phone')->nullable();
            $table->float('balance')->default(0);
            $table->boolean('blocked')->default(false);
            $table->boolean('verified')->default(false);
            $table->string('easy_start')->default('start');
            $table->string('easy_start_step')->default(0);
            $table->float('bonus')->default(0);
            $table->float('max_percent')->default(0);
            $table->integer('inactive_catalogs_count')->default(0);
            $table->integer('register_catalog')->default(0);
            $table->string('email')->nullable();
            $table->string('code')->nullable();
            $table->string('password')->nullable();
            $table->integer('coordinator_id')->default(0);
            $table->unsignedInteger('role_id')->default(1);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->rememberToken();
            $table->string('email_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->timestamp('blocked_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
