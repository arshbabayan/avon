<?php

return [
    'report' => 'Հաշվետվություն',
    'reported_at' => 'հաշվետվույթան արտահանման օրը',
    'catalog_report' => 'կատալոգի հաշվետվություն',
    'code' => 'Կոդ',
    'name_surname' => 'Անուն Ազգանուն',
    'position' => 'Մակարդակ ',
    'coordinator' => 'Կոորդինատոր',
    'phone' => 'Հեռախոս',
    'personal_circulation' => 'Անձնական Շրջանառություն',
    'registered_maximum' => 'գրանցված մաքսիմում',
    'group_circulation' => 'Խմբակային  Շրջանառություն',
    'register_catalog' => 'Գրանցման կատալոգ',
    'discount_and_bonus' => 'Զեղչ/բոնուս',
    'balance' => 'Հաշվեկշիռ',
    'status' => 'Կարգավիճակ',
    'start' => 'Մեկնարկ',
    'new_user' => 'Նորեկ',
    'inactive_catalogs' => 'Ոչ ակտիվ :count C (Catalogue)',
    'more_than_1' => '4999-14999',
    'more_than_2' => '15000 և ավելին',
];
