<?php

return [
    'thank_title' => 'Հարգելի Հաճախորդ, շնորհակալություն պատվերի համար',
    'thank_description' => 'Ձեր պատվերի հետ կապված ցանկացած հարցով կարող եք գրել մեզ հետևյալ հասցեով՝',
    'about_order' => 'Պատվերի նկարագիր',
    'consultant' => 'Ներկայացուցիչ՝',
    'consultant_number' => 'Ներկայացուցչի համար՝',
    'order_number' => 'Պատվերի համար՝',
    'delivery_address' => 'Առաքման հասցե՝',
    'delivery_status' => 'Վճարման կարգավիճակ՝',
    'delivery_status_will_be_pay' => 'Ենթակա է վճարման պատվերը ստանալիս',
    'delivery_status_payed' => 'Վճարված է',
    'product_name' => 'Ապրանքի անվանում',
    'product_code' => 'Ապրանքի կոդ',
    'price' => 'Գին',
    'count' => 'Քանակ',
    'total' => 'Ընդհանուր գումար՝',
    'dram' => 'ԴՐԱՄ',
    'products_with_discount' => 'Զեղջված ապրանքներ',
    'products_with_discount_total' => 'Ընդհանուր արժեքը',
    'affordable_discount' => 'Հասանելի զեղջի չափը',
    'products_with_discount_finally_total' => 'Ընդհանուր արժեքը զեղջված',

    'products_not_discount' => 'Չզեղջված ապրանքներ',
    'products_not_discount_total' => 'Ընդհանուր արժեքը',
    'products_not_discount_finally_total' => 'Ընդհանուր արժեքը',

    'final_calculation' => 'Վերջնահաշվարկ',

    'products_finally_total' => 'Ապրանքներ',
    'basket_bonus' => 'Բոնուս',
    'transportation_costs' => 'Տրանսպորտային ծախսեր',
    'service_fee' => 'Սպասարկման վճար',
    'packaging' => 'Փաթեթավորում',
    'journal' => 'Ամսագիր',
    'finally_total' => 'Ընդհանուր պատվերի գումարը',

    'finally_total_without_bonus' => 'Պատվերի գումարն առանց զեղջի',
    'used_bonus' => 'Օգտագործված զեղջ',
    'finally_total_with_bonus' => 'Ապրանքագրի գումարը',

    'used_prepayment' => 'Օգտագործված կանխավճար',
    'for_pay' => 'Վճարման ենթակա',
    'unused_discount' => 'Չօգտագործված Զեղջ',
    'unused_prepayment' => 'Չօգտագործված Կանխավճար',
];
