@extends('layouts.main')

@section('title') @lang('main.basket') @endsection

@section('content')
    @php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

    <main class="basket-page container text-center my-2 my-md-5">
        <h4 class="bg-pink text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">@lang('main.basket')</h4>

        <div class="basket-full @if(count($basket['cache']) === 0) d-none @endif">
            <h5 class="text-md-left mt-3">
                @lang('main.products_in_basket')
                (<span class="basket-count">{{ array_sum($basket['cache']) }}</span>)
            </h5>

            <div class="row align-items-center">
                @if(count($basket['products_with_discount']) > 0)
                    <div class="products_with_discount col-12 p-0 mb-5">
                        <h4 class="color-pink">@lang('main.products_with_discount')</h4>
                        <div class="basket-container">
                            <div class="row basket-title m-0 bb-2-pink">
                                <div class="col-md-6 br-2-pink ">@lang('main.in_basket_name')</div>
                                <div class="col-md-2 br-2-pink ">@lang('main.in_basket_price')</div>
                                <div class="col-md-2 br-2-pink ">@lang('main.in_basket_count')</div>
                                <div class="col-md-2">@lang('main.in_basket_total')</div>
                            </div>
                            <div class="basket-content m-0">
                                @foreach($basket['products_with_discount'] as $basket_product)
                                    <div class="row basket-item m-0 bb-2-pink" data-product="{{ $basket_product->id }}">
                                        <div class="col-md-6 br-2-pink">
                                            <div class="media align-items-center flex-column flex-md-row">
                                                <img class="img-fluid mr-1" width="200"
                                                     src="{{ $basket_product->topImage() }}"
                                                     alt="{{ $basket_product->translate('title') }}"
                                                     title="{{ $basket_product->translate('title') }}">
                                                <div class="media-body">
                                                    <div>
                                                        <h6>{{ $basket_product->translate('title') }}</h6>
                                                        <p>@lang('main.articul') <b>{{ $basket_product->articul }}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 br-2-pink d-flex flex-column justify-content-center align-items-center">
                                            <p class="basket-item-price-area @if(!$basket_product->doublePrices($basket['cache'][$basket_product->id])) d-none @endif text-muted m-0">
                                                <del>
                                                    <span class="basket-item-price">{{ $basket_product->price($basket['cache'][$basket_product->id]) }}</span>
                                                    @lang('main.dram')
                                                </del>
                                                <span class="text-lowercase">@lang('main.in_basket_price')</span>
                                            </p>
                                            <span class="font-weight-bold text-pink font-size-sm-22">
                                        <span class="basket-item-discount">
                                            {{ $basket_product->discount($basket['cache'][$basket_product->id]) }}
                                        </span>
                                        @lang('main.dram')
                                    </span>
                                        </div>
                                        <div class="col-md-2 br-2-pink">
                                            <div class="num-counter mt-4">
                                                <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                                                <input readonly class="basket-item-count" type="text" value="{{ $basket['cache'][$basket_product->id] }}">
                                                <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                                                <br>
                                                <br>
                                                <a class="product-remove-basket text-pink" role="button" style="cursor: pointer">@lang('main.remove')</a>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex font-weight-bold text-pink font-size-sm-22 justify-content-center align-items-center">
                                    <span class="basket-item-total">
                                        {{ $basket['cache'][$basket_product->id] * $basket_product->discount($basket['cache'][$basket_product->id]) }}
                                    </span>
                                            @lang('main.dram')
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row basket-footer m-0">
                                <div class="col-md-4 br-2-pink d-flex justify-content-center align-items-center">
                                    @lang('main.products_with_discount_total')
                                    <span class="font-weight-bold text-pink font-size-sm-22 ml-2">
                                <span class="products_with_discount_total">
                                    {{ $basket['products_with_discount_total'] }}
                                </span>
                                @lang('main.dram')
                            </span>
                                </div>
                                <div class="col-md-4 br-2-pink d-flex justify-content-center align-items-center">
                                    @lang('main.affordable_discount')
                                    <span class="font-weight-bold text-pink font-size-sm-22 ml-2">
                                <span class="affordable_discount">
                                    {{ $basket['affordable_discount'] }}
                                </span>
                                %
                            </span>
                                </div>
                                <div class="col-md-4 d-flex justify-content-center align-items-center">
                                    @lang('main.products_with_discount_finally_total')
                                    <span class="font-weight-bold text-pink font-size-sm-22 ml-2">
                                <span class="products_with_discount_finally_total">
                                    {{ $basket['products_with_discount_finally_total'] }}
                                </span>
                                @lang('main.dram')
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(count($basket['products_not_discount']) > 0)
                    <div class="products_not_discount col-12 p-0 mb-5">
                        <h4 class="color-pink">@lang('main.products_not_discount')</h4>
                        <div class="basket-container">
                            <div class="row basket-title m-0 bb-2-pink">
                                <div class="col-md-6 br-2-pink ">@lang('main.in_basket_name')</div>
                                <div class="col-md-2 br-2-pink ">@lang('main.in_basket_price')</div>
                                <div class="col-md-2 br-2-pink ">@lang('main.in_basket_count')</div>
                                <div class="col-md-2">@lang('main.in_basket_total')</div>
                            </div>
                            <div class="basket-content m-0">
                                @foreach($basket['products_not_discount'] as $basket_product)
                                    <div class="row basket-item m-0 bb-2-pink" data-product="{{ $basket_product->id }}">
                                        <div class="col-md-6 br-2-pink">
                                            <div class="media align-items-center flex-column flex-md-row">
                                                <img class="img-fluid mr-1" width="200"
                                                     src="{{ $basket_product->topImage() }}"
                                                     alt="{{ $basket_product->translate('title') }}"
                                                     title="{{ $basket_product->translate('title') }}">
                                                <div class="media-body">
                                                    <div>
                                                        <h6>{{ $basket_product->translate('title') }}</h6>
                                                        <p>@lang('main.articul') <b>{{ $basket_product->articul }}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 br-2-pink d-flex flex-column justify-content-center align-items-center">
                                            <p class="basket-item-price-area @if(!$basket_product->doublePrices($basket['cache'][$basket_product->id])) d-none @endif text-muted m-0">
                                                <del>
                                                    <span class="basket-item-price">{{ $basket_product->price($basket['cache'][$basket_product->id]) }}</span>
                                                    @lang('main.dram')
                                                </del>
                                                <span class="text-lowercase">@lang('main.in_basket_price')</span>
                                            </p>
                                            <span class="font-weight-bold text-pink font-size-sm-22">
                                            <span class="basket-item-discount">
                                                {{ $basket_product->discount($basket['cache'][$basket_product->id]) }}
                                            </span>
                                            @lang('main.dram')
                                        </span>
                                        </div>
                                        <div class="col-md-2 br-2-pink">
                                            <div class="num-counter mt-4">
                                                <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                                                <input readonly class="basket-item-count" type="text" value="{{ $basket['cache'][$basket_product->id] }}">
                                                <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                                                <br>
                                                <br>
                                                <a class="product-remove-basket text-pink" role="button" style="cursor: pointer">@lang('main.remove')</a>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex font-weight-bold text-pink font-size-sm-22 justify-content-center align-items-center">
                                        <span class="basket-item-total">
                                            {{ $basket['cache'][$basket_product->id] * $basket_product->discount($basket['cache'][$basket_product->id]) }}
                                        </span>
                                            @lang('main.dram')
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row basket-footer m-0">
                                <div class="col-md-12 br-2-pink d-flex justify-content-center align-items-center">
                                    @lang('main.products_not_discount_finally_total')
                                    <span class="font-weight-bold text-pink font-size-sm-22 ml-2">
                                    <span class="products_not_discount_finally_total">
                                        {{ $basket['products_not_discount_finally_total'] }}
                                    </span>
                                    @lang('main.dram')
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="delivery_point_area col-12 p-0 mb-5">
                    <h4 class="color-pink">@lang('main.choose_delivery_point')</h4>
                    <div class="basket-container">
                        @foreach($addresses as $key => $address)
                            <div class="font-size-md-20 text-left font-weight-bold mt-4 mb-4">
                                <label class="checkcontainer">
                                    {{ $address->translate('title') }}
                                    <input value="{{ $address->id }}" type="radio" name="address" @if ($key === 0) checked @endif>
                                    <span class="radiobtn"></span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="select_journal_area col-12 p-0 mb-5">
                    <h4 class="color-pink">@lang('main.do_yo_want_select_journal')</h4>
                    <div class="basket-container">
                        <div class="font-size-md-20 text-left font-weight-bold mt-4 mb-4">
                            <label class="checkcontainer">
                                @lang('main.no')
                                <input value="no" type="radio" name="journal" checked>
                                <span class="radiobtn"></span>
                            </label>
                        </div>
                        <div class="font-size-md-20 text-left font-weight-bold mt-4 mb-4">
                            <label class="checkcontainer">
                                @lang('main.yes')
                                <input value="yes" type="radio" name="journal">
                                <span class="radiobtn"></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="final_calculation col-12 p-0 mb-5">
                    <h4 class="color-pink">@lang('main.final_calculation')</h4>
                    <div class="basket-container">
                        <div>
                            <div class="font-size-md-20 font-weight-bold">@lang('main.products_finally_total')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>+</span>
                                <span class="products_finally_total mr-1">{{ $basket['products_finally_total'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div>
                            <div class="font-size-md-20 font-weight-bold">@lang('main.basket_bonus')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>-</span>
                                <span class="basket_bonus mr-1">{{ $basket['basket_bonus'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div>
                            <div class="font-size-md-20 font-weight-bold">@lang('main.transportation_costs')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>+</span>
                                <span class="transportation_costs mr-1">{{ $basket['transportation_costs'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div>
                            <div class="font-size-md-20 font-weight-bold">@lang('main.service_fee')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>+</span>
                                <span class="service_fee mr-1">{{ $basket['service_fee'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div>
                            <div class="font-size-md-20 font-weight-bold">@lang('main.packaging')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>+</span>
                                <span class="packaging mr-1">{{ $basket['packaging'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div class="journal-area">
                            <div class="font-size-md-20 font-weight-bold">@lang('main.journal')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span>+</span>
                                <span class="journal mr-1">{{ $basket['journal'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                        <div class="bt-2-pink">
                            <div class="font-size-md-20 font-weight-bold">@lang('main.finally_total')</div>
                            <div class="font-size-md-20 color-pink font-weight-bold">
                                <span class="finally_total mr-1">{{ $basket['finally_total'] }}</span>
                                @lang('main.dram')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-between align-items-center">
                <div>
                    @if($user && $user->role_id !== 9)
                        <a href="{{ route('locale.profile.index', $locale['prefix']) }}" class="btn bg-pink text-white outline-none">@lang('main.continue_orders')</a><br>
                    @elseif(!is_null($active_catalog))
                        <a href="{{ route('locale.catalog.book', ['locale' => $locale['prefix'], 'name' => $active_catalog['name']]) }}" class="btn bg-pink text-white">@lang('main.continue_orders')</a>
                    @endif
                </div>
                <div>
                    @if($user && $user->role_id !== 9)
                        <a role="button" data-toggle="modal" data-target="#checkout_order" class="btn bg-pink text-white outline-none">@lang('main.checkout')</a><br>
                    @else
                        <span>
                        Чтобы оформлять заказ нужно
                        <span class="color-pink cursor-pointer" data-toggle="modal" data-target="#entrance_for_representatives">войти</span>
                        в аккаунт
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <h5 class="basket-empty @if(array_sum($basket['cache']) > 0) d-none @endif text-md-left mt-3">@lang('main.your_basket_is_empty')</h5>
    </main>

    @include('main.popups.checkout-order')
@endsection

@section('script_end')
    <script src="{{ asset('main/custom/js/Pages/basket.js') }}"></script>
@endsection
