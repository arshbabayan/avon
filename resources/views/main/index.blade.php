@extends('layouts.main')

@section('title') @lang('main.home') @endsection

@section('content')
    <main>
        @if(count($sliders) > 0)
            <section>
                <div id="carouselExampleControls" class="carousel slide slider-area" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($sliders as $slider_key => $slider)
                            <div class="carousel-item @if($slider_key == 0) active @endif">
                                <a style="cursor: pointer">
                                    <img class="d-block w-100" src="{{ asset('main/sliders/' . $slider->image) }}" alt="">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev ml-md-4" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">@lang('main.previous')</span>
                    </a>
                    <a class="carousel-control-next mr-md-4" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">@lang('main.next')</span>
                    </a>
                </div>
            </section>
        @endif
    </main>

    @include('main.popups.how-to-join-our-team')
    @include('main.popups.how-to-become-a-coordinator')
    @include('main.popups.how-to-become-a-regional-manager')
@endsection
