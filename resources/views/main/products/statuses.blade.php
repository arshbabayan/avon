@extends('layouts.main')

@section('title') @lang('main.' . $status['prefix']) @endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-uppercase text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">@lang('main.' . $status['prefix'])</h4>
        @if(count($products) > 0)
            <div class="row new-item-section my-4">
                @foreach($products as $product)
                    <div class="product-item col-12 col-sm-6 col-lg-3 my-4">
                        <div class="media flex-column flex-md-row text-left">
                            <img class="img-fluid mr-2 mr-md-4" width="80" src="{{ $product->topImage() }}" alt="{{ $product->translate('title') }}" title="{{ $product->translate('title') }}">
                            <div class="media-body">
                                <div class="params d-none">
                                    <span class="real-id">{{ $product->id }}</span>
                                    <span class="real-price">{{ $product->price }}</span>
                                    <span class="real-discount">{{ $product->discount }}</span>
                                    <span class="real-discount-for-more">{{ $product->discount_for_more }}</span>
                                    <span class="real-easy-start">{{ $product->easy_start }}</span>
                                    <span class="real-easy-start-step">{{ $product->easy_start_step }}</span>
                                    <span class="real-easy-start-price">{{ $product->easy_start_price }}</span>
                                </div>
                                <a class="product-title text-dark" href="{{ route('locale.product', ['locale' => $locale['prefix'], 'articul__id' => $product->articul . '__' . $product->id]) }}">
                                    <h6 class="mt-0">{{ $product->translate('title') }}</h6>
                                </a>
                                <p class="product-price-area @if(!$product->doublePrices(1)) d-none @endif font-size-12 text-muted text-nowrap m-0">
                                    <del>
                                        <span class="product-price">{{ $product->price(1) }}</span> @lang('main.dram')
                                    </del>
                                    @lang('main.normal_price')
                                </p>
                                <span class="font-weight-bold text-pink font-size-sm-22">
                                    <span class="product-discount">{{ $product->discount(1) }}</span> @lang('main.dram')
                                </span>
                                <div class="num-counter text-left my-3 @if(!$product->doublePrices(1)) m-top-34 @endif">
                                    <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                                    <input readonly class="product-count" type="text" value="1">
                                    <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                                </div>
                                <a class="product-to-basket btn bg-pink text-white" role="button" style="cursor: pointer">@lang('main.to_basket')</a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-12 mt-4">
                    @include('main.extensions.pagination', ['data' => $products])
                </div>
            </div>
        @else
            <p class="text-center w-100 mt-3">@lang('main.products_empty_data')</p>
        @endif
    </main>
@endsection

@section('script_end')
    <script src="{{ asset('main/custom/js/Pages/products.js') }}"></script>
@endsection
