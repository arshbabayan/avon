@extends('layouts.main')

@section('title') {{ $product->translate('title') }} @endsection

@section('style')
    <!-- Slider css -->
    <link href="{{ asset('main/assets/slick-carousel/slick.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('main/assets/slick-carousel/slick-theme.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Ninja-slider -->
    <link href="{{ asset('main/assets/ninja-slider/ninja-slider.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('main/assets/ninja-slider/ninja-slider.js') }}" type="text/javascript"></script>
@endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <div class="row my-4">
            <div class="col-md-5 ninja-slider-part">
                <div id="ninja-slider" class="bg-white p-0">
                    <div class="ninja-slider-content">
                        <div class="slider-inner">
                            <ul>
                                @if (count($product->images) > 0)
                                    @foreach($product->images as $key => $product_image)
                                        <li>
                                            <a class="ns-img" href="{{ asset('main/products/' . $product_image->image) }}"></a>
                                        </li>
                                    @endforeach
                                @else
                                    <li>
                                        <a class="ns-img" href="{{ $product->topImage() }}"></a>
                                    </li>
                                @endif
                            </ul>
                            <div class="fs-icon" title="Expand/Close"></div>
                        </div>
                        <div id="thumbnail-slider">
                            <div class="inner">
                                <ul>
                                    @if (count($product->images) > 0)
                                        @foreach($product->images as $key => $product_image)
                                            <li>
                                                <a class="thumb" href="{{ asset('main/products/' . $product_image->image) }}"></a>
                                                <span>{{ $key + 1 }}</span>
                                            </li>
                                        @endforeach
                                    @else
                                        <li>
                                            <a class="thumb" href="{{ $product->topImage() }}"></a>
                                            <span>{{ 1 }}</span>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="product-item col-md-7 text-left pl-md-5">
                <div class="params d-none">
                    <span class="real-id">{{ $product->id }}</span>
                    <span class="real-price">{{ $product->price }}</span>
                    <span class="real-discount">{{ $product->discount }}</span>
                    <span class="real-discount-for-more">{{ $product->discount_for_more }}</span>
                    <span class="real-easy-start">{{ $product->easy_start }}</span>
                    <span class="real-easy-start-step">{{ $product->easy_start_step }}</span>
                    <span class="real-easy-start-price">{{ $product->easy_start_price }}</span>
                </div>

                <h3>{{ $product->translate('title') }}</h3>
                <p>@lang('main.articul') <b>{{ $product->articul }}</b></p>

                <p class="product-price-area @if(!$product->doublePrices(1)) d-none @endif text-muted mb-0">
                    <del>
                        <span class="product-price">{{ $product->price(1) }}</span>
                        @lang('main.dram')
                    </del>
                </p>
                <span class="text-pink font-size-md-25">
                    <span class="product-discount">{{ $product->discount(1) }}</span>
                    @lang('main.dram')
                </span>

                <div class="num-counter mt-4">
                    <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                    <input readonly class="product-count" type="text" value="1">
                    <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                    <br><br>
                    <a class="product-to-basket btn bg-pink text-white" role="button" style="cursor: pointer">@lang('main.to_basket')</a>
                </div>
            </div>

            <div class="col-12 second-modal mt-4 mt-md-5">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link text-pink active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                           aria-selected="true">@lang('main.description')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-pink" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                           aria-selected="false">@lang('main.composition')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-pink" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                           aria-selected="false">@lang('main.reviews')</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="p-2 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        {!! $product->translate('description') !!}
                    </div>
                    <div class="p-2 tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        {!! $product->translate('composition') !!}
                    </div>
                    <div class="p-2 tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        {!! $product->translate('reviews') !!}
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script_end')
    <script src="{{ asset('main/custom/js/Pages/product.js') }}"></script>
@endsection
