<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ trans('main.report', [], $locale['prefix']) }} {{ $catalog->name }}</title>
</head>
<body>

<table border="1" style="text-align: left; width: 200%; table-layout: fixed; height: 400px">
    <tbody>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>@lang('report.report')</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="3">
            {{ date('d.m.Y H:i') . '-' . trans('report.reported_at') }}
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">
            {{ $catalog->name . '/' . date('Y', strtotime($catalog->created_at)) . ' '. trans('report.catalog_report') }}
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">
            {{ trans('report.coordinator') . ' ' . $user->code . ' ' . $user->name . ' ' . $user->surname }}
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><b>@lang('report.code')</b></td>
        <td><b>@lang('report.name_surname')</b></td>
        <td><b>@lang('report.position')</b></td>
        <td><b>@lang('report.coordinator')</b></td>
        <td><b>@lang('report.name_surname')</b></td>
        <td><b>@lang('report.phone')</b></td>
        <td><b>@lang('report.address')</b></td>
        <td><b>@lang('report.passport')</b></td>
        <td><b>@lang('report.personal_circulation')</b></td>
        <td><b>%</b></td>
        <td><b>@lang('report.registered_maximum') %</b></td>
        <td><b>@lang('report.group_circulation')</b></td>
        <td><b>@lang('report.register_catalog')</b></td>
        <td><b>@lang('report.discount_and_bonus')</b></td>
        <td><b>@lang('report.balance')</b></td>
        <td><b>@lang('report.status')</b></td>
        <td><b>@lang('report.more_than_1')</b></td>
        <td><b>@lang('report.more_than_2')</b></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>

    <tr>
        <td>{{ $user->code }}</td>
        <td>{{ $user->name . ' ' . $user->surname }}</td>
        <td>0</td>
        <td>{{ $user->coordinator->code ?? '' }}</td>
        <td>{{ $user->coordinator ? $user->coordinator->name . ' ' . $user->coordinator->surname : '' }}</td>
        <td>{{ $user->phone }}</td>
        <td>{{ $user->address }}</td>
        <td>{{ $user->passport }}</td>
        <td>{{ $user->trade($catalog->id) }}</td>
        <td>{{ $user->turnover($catalog->id)['percent'] }}</td>
        <td>{{ $user->max_percent }}</td>
        <td>{{ $user->teamTrade($catalog->id) }}</td>
        <td>{{ $user->registerCatalog()->name }}</td>
        <td>{{ $user->turnover($catalog->id)['bonus'] }}</td>
        <td>
            @if($user->balance > 0)
                +{{ $user->balance }}
            @elseif($user->balance < 0)
                -{{ $user->balance }}
            @else
                0
            @endif
        </td>
        <td>{{ $user->status($catalog->id) }}</td>
        <td>{{ $user->teamMinTradeCount($catalog->id)['more_than_1'] }}</td>
        <td>{{ $user->teamMinTradeCount($catalog->id)['more_than_2'] }}</td>
    </tr>

    </tbody>
</table>

</body>
</html>
