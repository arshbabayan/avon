<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ trans('main.order', [], $locale['prefix']) }} {{ $order->id }}</title>
</head>
<body>

<table style="text-align: left">
    <tbody>
    <tr>
        <td>
            <b>{{ trans('mails.consultant_number', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.consultant', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.order_number', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.delivery_address', [], $locale['prefix']) }}</b>
        </td>
    </tr>
    <tr>
        <td>{{ $order['user']['code'] }}</td>
        <td>{{ $order['user']['name'] . ' ' . $order['user']['surname'] }}</td>
        <td>{{ $order->id }}</td>
        <td>{{ $order->address->translations($locale['id'])->title ?? $order->address->translate('title') }}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
            <b>{{ trans('mails.product', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.price', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.count', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.finish_price', [], $locale['prefix']) }}</b>
        </td>
    </tr>
    @foreach($order->items as $item)
        <tr>
            <td>
                {{ $item->product->translations($locale['id'])->title ?? $item->product->translate('title') }}
                <br>
                {{ $item->product->articul }}
            </td>
            <td>{{ $item->price . ' ' . trans('mails.dram', [], $locale['prefix']) }}</td>
            <td>{{ $item->count }}</td>
            <td>{{ $item->price * $item->count . ' ' . trans('mails.dram', [], $locale['prefix']) }}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
            <b>{{ trans('mails.total_bm', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.total_discount', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.transportation_costs', [], $locale['prefix']) }}</b>
        </td>
        <td>
            <b>{{ trans('mails.service_fee', [], $locale['prefix']) }}</b>
        </td>
    </tr>
    <tr>
        <td>{{ $order->bonus }} {{ trans('mails.dram', [], $locale['prefix']) }}</td>
        <td>{{ $order->discount }} %</td>
        <td>{{ $order->transportation_costs . ' ' . trans('mails.dram', [], $locale['prefix']) }}</td>
        <td>{{ $order->service_fee . ' ' . trans('mails.dram', [], $locale['prefix']) }}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center">
            {{ trans('mails.finish_price', [], $locale['prefix']) }}
            <b>{{ $order->real_total + $order->service_fee + $order->transportation_costs }}</b>
            {{ trans('mails.dram', [], $locale['prefix']) }}
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
