@extends('layouts.main')

@section('title') @lang('main.registration') @endsection

@section('content')
    <main class="container text-center md-5">
        <form id="registration-form-data" class="row">
            <div class="col-sm-5 mx-auto form-registration">
                <label for="name">
                    <input id="name" name="name" type="text" placeholder="@lang('main.name')">
                    <small class="name"></small>
                </label>
                <label for="surname">
                    <input id="surname" name="surname" type="text" placeholder="@lang('main.surname')">
                    <small class="surname"></small>
                </label>
                <label for="address">
                    <input id="address" name="address" type="text" placeholder="@lang('main.address')">
                    <small class="address"></small>
                </label>
                <label for="passport">
                    <input id="passport" name="passport" type="text" placeholder="@lang('main.passport')">
                    <small class="passport"></small>
                </label>
                <label for="phone">
                    <input id="phone" name="phone" type="text" placeholder="@lang('main.phone')">
                    <small class="phone"></small>
                </label>
                <label for="email">
                    <input id="email" name="email" type="text" placeholder="@lang('main.email')">
                    <small class="email"></small>
                </label>
                <label for="coordinator">
                    <input id="coordinator" name="coordinator" type="text" placeholder="@lang('main.coordinator')">
                    <small class="coordinator"></small>
                </label>
            </div>
            <div class="w-100"></div>
            <div class="col-sm-5 mx-auto text-left">
                <div class="form-check my-3">
                    <input id="privacy_policy" name="privacy_policy" class="form-check-input" type="checkbox" value="1">
                    <label class="form-check-label font-size-12" for="privacy_policy">@lang('main.privacy_policy')</label>
                    <small class="privacy_policy color-pink"></small>
                </div>
            </div>
            <div class="w-100"></div>
            <button id="registration-form-submit" class="btn bg-pink text-white mx-auto mt-4 outline-none">@lang('main.send')</button>
        </form>
    </main>
@endsection
