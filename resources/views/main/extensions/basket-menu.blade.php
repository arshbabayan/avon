<div class="col-auto basket-menu basket my-2 my-lg-0">
    <div class="basket-top-area">
        <i class="fas fa-shopping-cart basket-icon text-pink fa-lg"></i>
        <div class="basket-count">
            @if(array_sum($basket['cache']) > 0)
                {{ array_sum($basket['cache']) }}
            @endif
        </div>
    </div>

    <div class="border bg-light p-3 basket-card" style="display: none;">
        <p class="basket-full-empty-text">
            @if(array_sum($basket['cache']) > 0)
                @lang('main.you_added_to_cart'):
            @else
                @lang('main.your_basket_is_empty'):
            @endif
        </p>

        <div class="basket-content-area @if(array_sum($basket['cache']) === 0) d-none @endif">
            <div class="basket-items">
                <div class="basket-custom d-none basket-item media my-3">
                    <img class="basket-item-image img-fluid mr-3" width="70" src="" alt="" title="">
                    <div class="media-body">
                        <h6 class="basket-item-title d-block m-0"></h6>
                        <span class="font-weight-bold text-pink">
                            <span class="basket-item-discount"></span>
                            @lang('main.dram')
                        </span>
                        <br>
                        <span class="text-muted">
                            @lang('main.total'):
                            <span class="basket-item-total"></span>
                        </span>
                    </div>
                </div>
                @foreach($basket['products'] as $basket_product_id => $basket_product)
                    <div class="basket-item media my-3">
                        <img class="basket-item-image img-fluid mr-3" width="70" src="{{ $basket_product->topImage() }}" alt="{{ $basket_product->translate('title') }}" title="{{ $basket_product->translate('title') }}">
                        <div class="media-body">
                            <h6 class="basket-item-title d-block m-0">{{ $basket_product->translate('title') }}</h6>
                            <span class="font-weight-bold text-pink">
                            <span class="basket-item-discount">{{ $basket_product->discount($basket['cache'][$basket_product->id]) }}</span>
                            @lang('main.dram')
                        </span>
                            <br>
                            <span class="text-muted">
                            @lang('main.total'):
                            <span class="basket-item-total">{{ $basket['cache'][$basket_product->id] }}</span>
                        </span>
                        </div>
                    </div>
                    @if(count($basket['products']) > 1 && count($basket['products']) !== $loop->iteration)
                        <hr class="bg-pink">
                    @endif
                @endforeach
            </div>
            <div>
                <a href="{{ route('locale.basket.index', $locale['prefix']) }}" class="text-pink">@lang('main.check_cart')</a>
                <p class="text-muted">
                    @lang('main.total_products'):
                    <span class="basket-count">{{ array_sum($basket['cache']) }}</span>
                </p>
            </div>
        </div>

        <a href="{{ route('locale.basket.index', $locale['prefix']) }}" role="button" class="btn bg-pink text-white d-block outline-none">@lang('main.to_basket')</a>
    </div>
</div>
