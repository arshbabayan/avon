<nav class="navbar navbar-expand-lg navbar-light my-md-2 p-0" style="margin-top: 0!important;">
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav justify-content-between align-items-center w-100">
            <li class="nav-item @if($route_name == 'locale.business-model' || $route_name == 'locale.registration') active-link @endif">
                <a class="nav-link navbar-links" href="{{ route('locale.business-model', $locale['prefix']) }}">@lang('main.business_model')</a>
            </li>
            <li class="nav-item @if($route_name == 'locale.about-company') active-link @endif">
                <a class="nav-link navbar-links" href="{{ route('locale.about-company', $locale['prefix']) }}">@lang('main.about_company')</a>
            </li>
            <li class="nav-item @if($route_name == 'locale.payment-orders') active-link @endif">
                <a class="nav-link navbar-links" href="{{ route('locale.payment-orders', $locale['prefix']) }}">@lang('main.payment_orders')</a>
            </li>
            @foreach($statuses as $status)
                <li class="nav-item @if($route_name == 'locale.statuses' && $url_segments[3] == $status->prefix) active-link @endif">
                    <a class="nav-link navbar-links" href="{{ route('locale.statuses', ['locale' => $locale['prefix'], 'prefix' => $status->prefix]) }}">
                        @lang('main.' . $status->prefix)
                    </a>
                </li>
            @endforeach
            <li class="nav-item @if($route_name == 'locale.delivery-points') active-link @endif">
                <a class="nav-link navbar-links" href="{{ route('locale.delivery-points', $locale['prefix']) }}">@lang('main.delivery_points')</a>
            </li>
            <li class="nav-item @if($route_name == 'locale.contact-us') active-link @endif">
                <a class="nav-link navbar-links" href="{{ route('locale.contact-us', $locale['prefix']) }}">@lang('main.contact_us')</a>
            </li>
        </ul>
    </div>
</nav>
