@extends('layouts.main')

@php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

@section('slick')
    <link href="{{ asset('main/assets/slick-carousel/slick.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('main/assets/slick-carousel/slick-theme.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <div class="multiple-items">
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->trade($active_catalog->id) }} @lang('main.dram')</span>
                <span class="my-2">@lang('main.personal_turnover')</span>
            </div>
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->teamTrade($active_catalog->id) }} @lang('main.dram')</span>
                <span class="my-2">@lang('main.group_circulation')</span>
            </div>
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->turnover($active_catalog->id)['bonus'] }} @lang('main.dram')</span>
                <span class="my-2">@lang('main.available_discount')</span>
            </div>
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->turnover($active_catalog->id)['percent'] }} %</span>
                <span class="my-2">@lang('main.current_level')</span>
            </div>
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->teamMinTradeCount($active_catalog->id)['more_than_2'] }}</span>
                <span class="my-2">@lang('main.active_representatives')</span>
            </div>
            <div>
                <span class="font-weight-bold font-size-md-26 d-block">{{ $user->teamMinTradeCount($active_catalog->id)['more_than_1'] }}</span>
                <span class="my-2">@lang('main.active_representatives_with_minimal_order')</span>
            </div>
        </div>

        <div class="row justify-content-between mt-4">
            <div class="col-sm-auto mr-auto px-0">
                <h5>{{ $user->name . ' ' . $user->surname }} </h5>
                @if ($user->easyStart())
                    <span class="text-pink">
                        @lang('main.easy_start_activated', ['step' => $user->easyStartStep()])
                    </span>
                @endif
            </div>

            <div class="w-100 my-2"></div>

            <div class="col-md-3 bg-light profile-sidebar sidebar py-3 my-2 my-md-0">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a class="text-pink">
                            @lang('main.balance') {{ $user->balance }} @lang('main.dram')
                        </a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a class="text-pink">
                            @lang('main.bonus') {{ $user->bonus }} @lang('main.dram')
                        </a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a class="text-pink" href="#" data-toggle="modal" data-target="#to-pay-modal">@lang('main.to_pay')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item @if($route_name === 'locale.profile.index') active @endif">
                        <a class="text-pink" href="{{ route('locale.profile.index', $locale['prefix']) }}">@lang('main.to_order')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item @if($route_name === 'locale.profile.edit') active @endif">
                        <a class="text-pink" href="{{ route('locale.profile.edit', $locale['prefix']) }}">@lang('main.my_profile')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item @if($route_name === 'locale.profile.orders') active @endif">
                        <a class="text-pink" href="{{ route('locale.profile.orders', $locale['prefix']) }}">@lang('main.my_orders')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item @if($route_name === 'locale.profile.reports') active @endif">
                        <a class="text-pink" href="{{ route('locale.profile.reports', $locale['prefix']) }}">@lang('main.reports')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a class="text-pink" href="{{ route('locale.statuses', ['locale' => $locale['prefix'], 'prefix' => 'stocks']) }}">@lang('main.stocks')</a>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a class="text-pink" href="{{ route('locale.profile.logout', $locale['prefix']) }}">@lang('main.logout')</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 my-2 my-md-0">
                @yield('profile-content')
            </div>
        </div>
    </main>

    @include('main.popups.to-pay')
@endsection

@section('script_end')
    <script src="{{ asset('main/custom/js/Pages/profile.js') }}"></script>
@endsection
