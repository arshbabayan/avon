@if($data->lastPage() > 1 && $data->lastPage() >= request()->get('page'))
    <nav>
        <ul class="my-pagination pagination d-flex justify-content-center">
            {{--Not first page--}}
            @if($data->currentPage() !== 1)
                <li class="page-item">
                    <a class="page-link" href="{{ $data->url(($data->currentPage()-1)) }}" aria-label="@lang('main.next')">
                        <span aria-hidden="true"><i class="fas fa-chevron-left"></i></span>
                        <span class="sr-only">@lang('main.previous')</span>
                    </a>
                </li>
            @endif

            @php
                $printed = 0;
                $start = $data->currentPage() > 2 ? $data->currentPage() - 2 : 1;
            @endphp

            {{--Pages--}}
            @for ($i = $start; $i <= $data->lastPage(); $i++)
                @php $printed++; @endphp
                @if ($data->currentPage() == $i)
                    <li class="page-item">
                        <a class="page-link active-page" style="cursor: pointer">{{ $i }}</a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ $data->url($i) }}">{{ $i }}</a>
                    </li>
                @endif

                @if($printed===5 || $i == $data->lastPage())
                    @break
                @endif
            @endfor

            {{--Not latest page--}}
            @if ($data->currentPage() !== $data->lastPage() )
                <li class="page-item">
                    <a class="page-link" href="{{ $data->url(($data->currentPage()+1)) }}" aria-label="@lang('main.next')">
                        <span aria-hidden="true"><i class="fas fa-chevron-right"></i></span>
                        <span class="sr-only">@lang('main.next')</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
