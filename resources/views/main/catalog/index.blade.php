@extends('layouts.main')

@section('title') {{ $catalog->name }} @endsection

@section('style')
    <!-- Slider css -->
    <link href="{{ asset('main/assets/slick-carousel/slick.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('main/assets/slick-carousel/slick-theme.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Catalog book link and scripts -->
    <link href="{{ asset('main/assets/css/catalog-css/flip.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('main/assets/js/catalog-js/jquery.js') }}"></script>
    <script src="{{ asset('main/assets/js/catalog-js/turn.js') }}"></script>
    <script src="{{ asset('main/assets/js/catalog-js/jquery.fullscreen.js') }}"></script>
    <script src="{{ asset('main/assets/js/catalog-js/jquery.address-1.6.min.js') }}"></script>
    <script src="{{ asset('main/assets/js/catalog-js/wait.js') }}"></script>
    <script src="{{ asset('main/assets/js/catalog-js/onload.js') }}"></script>
    <!-- Ninja-slider -->
    <link href="{{ asset('main/assets/ninja-slider/ninja-slider.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('main/assets/ninja-slider/ninja-slider.js') }}" type="text/javascript"></script>
@endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">{{ $catalog->name }}</h4>

        <div id="fb5-ajax" class="mt-4 mt-md-5">
            <div data-current="book5" class="fb5" id="fb5">
                {{--Start preloader--}}
                <div class="fb5-preloader">
                    <div id="wBall_1" class="wBall">
                        <div class="wInnerBall">
                        </div>
                    </div>
                    <div id="wBall_2" class="wBall">
                        <div class="wInnerBall">
                        </div>
                    </div>
                    <div id="wBall_3" class="wBall">
                        <div class="wInnerBall">
                        </div>
                    </div>
                    <div id="wBall_4" class="wBall">
                        <div class="wInnerBall">
                        </div>
                    </div>
                    <div id="wBall_5" class="wBall">
                        <div class="wInnerBall">
                        </div>
                    </div>
                </div>
                <div class="fb5-bcg-book"></div>
                {{--End preloader--}}


                {{--Start book--}}
                <div id="fb5-container-book">
                    <section id="fb5-deeplinking">
                        <ul>
                            @foreach($catalog->pages as $page_key => $page)
                                <li data-address="page{{ $page_key + 1 }}" data-page="{{ $page_key + 1 }}"></li>
                            @endforeach
                        </ul>
                    </section>

                    <div id="fb5-book" data-catalog_name="{{ $catalog->name }}">
                        @foreach($catalog->pages as $page_key => $page)
                            <div class="catalog-page" data-page_id="{{ $page->id }}">
                                <div class="fb5-cont-page-book">
                                    <img src="{{ asset('main/pages/' . $page->image) }}" class="img-fluid" alt="">
                                    <div class="fb5-meta">
                                        <span class="fb5-description"></span>
                                        <span class="fb5-num"></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <a class="fb5-nav-arrow prev">
                        <img src="{{ asset('main/assets/img/icons/left-chevron.svg') }}" alt="">
                    </a>
                    <a class="fb5-nav-arrow next">
                        <img src="{{ asset('main/assets/img/icons/right-chevron.svg') }}" alt="">
                    </a>
                </div>
                {{--End book--}}


                {{--Start footer--}}
                <div id="fb5-footer">
                    <div class="fb5-menu" id="fb5-center">
                        <ul>
                            <li>
                                <a title="@lang('main.zoom_in')" class="fb5-zoom-in">
                                    <img src="{{ asset('main/assets/img/icons/plus.svg') }}" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a title="@lang('main.reduce')" class="fb5-zoom-out">
                                    <img src="{{ asset('main/assets/img/icons/minus.svg') }}" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a title="@lang('main.auto')" class="fb5-zoom-auto">
                                    <img src="{{ asset('main/assets/img/icons/search.svg') }}" class="img-fluid" alt="">
                                </a>
                            </li>
                            <li>
                                <a title="@lang('main.show_main_page')" class="fb5-home">
                                    <img src="{{ asset('main/assets/img/icons/home.svg') }}" class="img-fluid" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="fb5-menu" id="fb5-right">
                        <ul>
                            <li class="fb5-goto">
                                <label for="fb5-page-number" id="fb5-label-page-number">@lang('main.go_to_page')</label>
                                <input type="text" class="pl-2" id="fb5-page-number">
                                <button type="button">@lang('main.go')</button>
                            </li>
                        </ul>
                    </div>
                </div>
                {{--End preloader--}}
            </div>
        </div>
    </main>


    @include('main.popups.page-products')
    @include('main.popups.show-product')
@endsection

@section('script_start')
    <script src="{{ asset('main/custom/js/Others/book.js') }}"></script>
@endsection

@section('script_end')
    <script src="{{ asset('main/custom/js/Pages/catalog.js') }}"></script>
@endsection
