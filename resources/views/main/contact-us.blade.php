@extends('layouts.main')

@section('title') @lang('main.contact_us') @endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-uppercase text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">@lang('main.contact_us')</h4>

        @php
            $user = \Illuminate\Support\Facades\Auth::user();
            $is_user = $user && $user->role_id !== 9;
        @endphp

        <form id="contact-us-form-data">
            <div class="col-sm-5 mx-auto form-registration">
                <label for="full_name">
                    <input id="full_name" name="full_name" type="text" placeholder="@lang('main.full_name')" value="{{ $is_user ? $user->name . ' ' . $user->surname : '' }}">
                    <small class="full_name"></small>
                </label>
                <label for="email">
                    <input id="email" name="email" type="text" placeholder="@lang('main.email')" value="{{ $is_user ? $user->email : '' }}">
                    <small class="email"></small>
                </label>
                <label for="phone">
                    <input id="phone" name="phone" type="text" placeholder="@lang('main.phone')" value="{{ $is_user ? $user->phone : '' }}">
                    <small class="phone"></small>
                </label>
                <label for="message">
                    <textarea id="message" name="message" placeholder="@lang('main.message')" class="form-textarea" rows="6"></textarea>
                    <small class="message"></small>
                </label>
            </div>
            <div class="w-100"></div>
            <button type="button" id="contact-us-form-submit" class="btn bg-pink text-white mx-auto mt-4 outline-none">@lang('main.send')</button>
        </form>
    </main>
@endsection
