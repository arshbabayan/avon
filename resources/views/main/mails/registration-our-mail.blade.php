<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.name')</b>
    <span>{{ $data['user']['name'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.surname')</b>
    <span>{{ $data['user']['surname'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.address')</b>
    <span>{{ $data['user']['address'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.passport')</b>
    <span>{{ $data['user']['passport'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.phone')</b>
    <span>{{ $data['user']['phone'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.email')</b>
    <span>{{ $data['user']['email'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.code')</b>
    <span>{{ $data['user']['code'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.coordinator')</b>
    <span>
        @if(is_null($data['user']['coordinator']))
            @lang('main.exist')
        @else
            {{ $data['user']['coordinator']['name'] . ' ' . $data['user']['coordinator']['surname'] }} ({{ $data['user']['coordinator']['code'] }})
        @endif
    </span>
</p>
