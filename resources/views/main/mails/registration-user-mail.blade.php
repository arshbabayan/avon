<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.code')</b>
    <span>{{ $data['user']['code'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.password')</b>
    <span>{{ $data['password'] }}</span>
</p>
<a style="background-color: #d356a1; color: white; padding:6px 12px" href="{{ config('app.url') . $locale['prefix'] . '/registration-verify?email_token=' . $data['user']['email_token'] }}">@lang('main.confirm')</a>

