<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.full_name')</b>
    <span>{{ $data['full_name'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.email')</b>
    <span>{{ $data['email'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.phone')</b>
    <span>{{ $data['phone'] }}</span>
</p>
<p>
    <b style="color: #d356a1; margin-right: 5px">@lang('main.message')</b>
    <span>{{ $data['message'] }}</span>
</p>
