<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Avon Cosmetics</title>
</head>
<body style="margin: 0">

<div style="color:#333333;font-family:'arial armenian';font-size:12px;line-height:18px;margin:0;text-align:center">
    @foreach($languages as $lang)
        @if ($lang['prefix'] === 'am')
            <table style="background:#ffffff;margin:0px auto 0px auto;padding:0 24px 0 24px;text-align:left;width:740px">
                <tbody>
                <tr>
                    <td style="padding-bottom:0;padding-top:0">
                        <h1 style="color:#333333;font-family:'arial armenian';font-size:23px;font-weight:bold;line-height:normal;margin-bottom:7px;margin-top:24px">
                            {{ trans('mails.thank_title', [], $lang['prefix']) }}
                        </h1>
                        <p style="margin-bottom:0;margin-top:0">
                            {{ trans('mails.thank_description', [], $lang['prefix']) }}
                            <a href="mailto:{{ config('mail.customers.address') }}" rel="noopener noreferrer" style="color:#003399;font-weight:bold;text-decoration:none" target="_blank">
                                {{ config('mail.customers.address') }}
                            </a>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:0;padding-top:0">
                        <h2 style="border-bottom-color:#333333;border-bottom-style:solid;border-bottom-width:2px;color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;font-weight:bold;margin-bottom:7px;margin-top:36px;padding-bottom:2px">
                            {{ trans('mails.about_order', [], $lang['prefix']) }}
                        </h2>
                        <p style="color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;margin-bottom:17px;padding-bottom:2px">
                            {{ trans('mails.consultant', [], $lang['prefix']) }} <span style="font-weight:bold;">{{ $order['user']['name'] . ' ' . $order['user']['surname'] }}</span>
                        </p>
                        <p style="color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;margin-bottom:17px;padding-bottom:2px">
                            {{ trans('mails.consultant_number', [], $lang['prefix']) }} <span style="font-weight:bold;">{{ $order['user']['code'] }}</span>
                        </p>
                        <p style="color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;margin-bottom:17px;padding-bottom:2px">
                            {{ trans('mails.order_number', [], $lang['prefix']) }} <span style="font-weight:bold;">{{ $order->id }}</span>
                        </p>
                        <p style="color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;margin-bottom:17px;padding-bottom:2px">
                            {{ trans('mails.delivery_address', [], $lang['prefix']) }} <span style="font-weight:bold;">{{ $order->address->translations($lang['id'])->title ?? $order->address->translate('title') }}</span>
                        </p>
                        <p style="color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;margin-bottom:17px;padding-bottom:2px">
                            {{ trans('mails.delivery_status', [], $lang['prefix']) }}
                            <span style="font-weight: bold">
                                @if ($order->status === 0)
                                    {{ trans('mails.delivery_status_will_be_pay', [], $lang['prefix']) }}
                                @else
                                    {{ trans('mails.delivery_status_payed', [], $lang['prefix']) }}
                                @endif
                            </span>
                        </p>

                        @if(count($order->withDiscountProducts) > 0)
                            <h2 style="border-bottom-color:#333333;border-bottom-style:solid;border-bottom-width:2px;color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;font-weight:bold;margin-bottom:22px;margin-top:36px;padding-bottom:2px">
                                {{ trans('mails.products_with_discount', [], $lang['prefix']) }}
                            </h2>
                            <table style="border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                                <tbody>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:285px">
                                        {{ trans('mails.product_name', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:103px">
                                        &nbsp;{{ trans('mails.product_code', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:77px">
                                        {{ trans('mails.price', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;text-align:center;width:auto">
                                        {{ trans('mails.count', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;text-align:right;width:75px">
                                        {{ trans('mails.total', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                                </tbody>
                                <tbody>
                                @foreach($order->withDiscountProducts as $item)
                                    <tr>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                            {{ $item->product->translations($lang['id'])->title ?? $item->product->translate('title') }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;font-style:italic;height:57px;padding-bottom:0;padding-top:0;width:103px">
                                            &nbsp;{{ $item->product->articul }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:77px">
                                            {{ $item->price . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                            {{ $item->count }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:right;width:75px">
                                            {{ $item->price * $item->count . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                        {{ trans('mails.products_with_discount_total', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                        {{ $order->products_with_discount_total . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                        {{ trans('mails.affordable_discount', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                        {{ $order->affordable_discount . ' %' }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                        {{ trans('mails.products_with_discount_finally_total', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                        {{ $order->products_with_discount_finally_total . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif

                        @if(count($order->notDiscountProducts) > 0)
                            <h2 style="border-bottom-color:#333333;border-bottom-style:solid;border-bottom-width:2px;color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;font-weight:bold;margin-bottom:22px;margin-top:36px;padding-bottom:2px">
                                {{ trans('mails.products_not_discount', [], $lang['prefix']) }}
                            </h2>
                            <table style="border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                                <tbody>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:285px">
                                        {{ trans('mails.product_name', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:103px">
                                        &nbsp;{{ trans('mails.product_code', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;width:77px">
                                        {{ trans('mails.price', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;text-align:center;width:auto">
                                        {{ trans('mails.count', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;padding-bottom:4px;padding-top:0;text-align:right;width:75px">
                                        {{ trans('mails.total', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                                </tbody>
                                <tbody>
                                @foreach($order->notDiscountProducts as $item)
                                    <tr>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                            {{ $item->product->translations($lang['id'])->title ?? $item->product->translate('title') }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;font-style:italic;height:57px;padding-bottom:0;padding-top:0;width:103px">
                                            &nbsp;{{ $item->product->articul }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:77px">
                                            {{ $item->price . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                            {{ $item->count }}
                                        </td>
                                        <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:right;width:75px">
                                            {{ $item->price * $item->count . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                        {{ trans('mails.products_not_discount_finally_total', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                        {{ $order->products_not_discount_finally_total . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif

                        <h2 style="border-bottom-color:#333333;border-bottom-style:solid;border-bottom-width:2px;color:#333333;font-family:'arial armenian';font-size:15px;font-style:italic;font-weight:bold;margin-bottom:22px;margin-top:36px;padding-bottom:2px">
                            {{ trans('mails.final_calculation', [], $lang['prefix']) }}
                        </h2>
                        <table style="border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                            <tbody>
                            <tr>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                    {{ trans('mails.products_finally_total', [], $lang['prefix']) }}
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                    + {{ $order->products_finally_total . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                    {{ trans('mails.basket_bonus', [], $lang['prefix']) }}
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                    - {{ $order->bonus . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                    {{ trans('mails.transportation_costs', [], $lang['prefix']) }}
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                    + {{ $order->transportation_costs . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                    {{ trans('mails.service_fee', [], $lang['prefix']) }}
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                    + {{ $order->service_fee . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                    {{ trans('mails.packaging', [], $lang['prefix']) }}
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                </td>
                                <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                    + {{ $order->packaging . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                </td>
                            </tr>
                            @if($order->journal > 0)
                                <tr>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;width:285px">
                                        {{ trans('mails.journal', [], $lang['prefix']) }}
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;text-align:center;width:auto">
                                    </td>
                                    <td style="text-align:right; border-bottom-color:#c8c8c8;border-bottom-style:solid;border-bottom-width:1px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;height:57px;padding-bottom:0;padding-top:0;font-weight: bold">
                                        + {{ $order->journal . ' ' . trans('mails.dram', [], $lang['prefix']) }}
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        <table style="border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                            <tbody>
                            <tr>
                                <td style="padding-bottom:0;padding-top:16px;text-align:right;vertical-align:top">
                                    <table style="width: 100%">
                                        <tbody>
                                        <tr>
                                            <td style="text-align:left; font-family:'garamond';font-size:22px;font-weight:bold;padding-bottom:0;padding-top:0">
                                                {{ trans('mails.finally_total', [], $lang['prefix']) }}
                                            </td>
                                            <td style="font-family:'garamond';font-size:22px;font-weight:bold;padding-bottom:0;padding-top:0">
                                                {{ $order->finally_total }}
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="margin-top:50px;border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                            <tbody>
                            <tr>
                                <td style="padding-bottom:0;padding-top:16px;vertical-align:top">
                                    <table style="width: 100%; line-height: 25px">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.finally_total_without_bonus', [], $lang['prefix']) }}`
                                                <span>{{ $order->finally_total }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.used_bonus', [], $lang['prefix']) }}`
                                                <span>{{ $order->bonus }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.finally_total_with_bonus', [], $lang['prefix']) }}`
                                                <span>{{ $order->finally_total - $order->bonus }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="margin-top:10px;margin-bottom:50px;border-collapse:collapse;border-spacing:0;text-align:left;width:686px">
                            <tbody>
                            <tr>
                                <td style="padding-bottom:0;padding-top:16px;vertical-align:top">
                                    <table style="width: 100%; line-height: 25px">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.used_prepayment', [], $lang['prefix']) }}`
                                                <span>{{ $order->used_prepayment }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.for_pay', [], $lang['prefix']) }}`
                                                <span>{{ $order->finally_total - $order->bonus - $order->used_prepayment }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.unused_discount', [], $lang['prefix']) }}`
                                                <span>{{ $order->unused_discount }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 18px;">
                                                {{ trans('mails.unused_prepayment', [], $lang['prefix']) }}`
                                                <span>{{ $order->unused_prepayment }}</span>
                                                {{ trans('mails.dram', [], $lang['prefix']) }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        @endif
    @endforeach
</div>

</body>
</html>
