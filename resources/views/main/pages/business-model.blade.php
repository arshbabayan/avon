@extends('layouts.main')

@section('title') {{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }} @endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">{{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }}</h4>
        <h6 class="my-4">{{ \App\Helpers\Actions::getInfoTranslate($current_information, 'description') }}</h6>

        <div class="row justify-content-between">
            <div class="col-12 text-left mb-4">
                <p class="lead">
                    {!! \App\Helpers\Actions::getInfoTranslate($current_information, 'content') !!}
                </p>
            </div>

            <div class="card-deck">
                <div class="card text-left rounded-0">
                    <div class="card-body bg-light border-0 p-3">
                        <h4 class="text-pink">@lang('main.become_a_representative')</h4>
                        <p class="text-muted">
                            {!! \App\Helpers\Actions::getInfoTranslate($current_information, 'become_a_representative') !!}
                        </p>
                        @if(\Illuminate\Support\Facades\Auth::guest() || \Illuminate\Support\Facades\Auth::user()->role_id === 9)
                            <a role="button" href="{{ route('locale.registration',$locale['prefix']) }}" class="btn bg-pink text-white px-4 outline-none">Да!</a>
                        @endif
                    </div>
                </div>
                <div class="card text-left rounded-0">
                    <div class="card-body bg-light border-0 p-3">
                        <h4 class="text-pink">@lang('main.become_a_coordinator')</h4>
                        <p class="text-muted">
                            {!! \App\Helpers\Actions::getInfoTranslate($current_information, 'become_a_coordinator') !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
