@extends('layouts.main')

@section('title') {{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }} @endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">
            {{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }}
        </h4>

        <h6 class="my-4">{{ \App\Helpers\Actions::getInfoTranslate($current_information, 'description') }}</h6>

        <div class="text-left">
            {!! \App\Helpers\Actions::getInfoTranslate($current_information, 'content') !!}
        </div>
    </main>
@endsection
