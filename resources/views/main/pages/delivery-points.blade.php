@extends('layouts.main')

@section('title') {{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }} @endsection

@section('content')
    <main class="container text-center my-2 my-md-5">
        <h4 class="bg-pink text-white d-md-inline-block rounded py-1 px-3 my-4 my-md-0">
            {{ \App\Helpers\Actions::getInfoTranslate($current_information, 'title') }}
        </h4>

        <div class="row">
            @foreach($addresses as $address)
                <div class="col-12">
                    <p class="lead my-4">{{ $address->translate('title') }}</p>
                    <div class="mapouter">
                        <div class="gmap_canvas ">
                            <iframe width="1080" height="327" id="gmap_canvas" src="{{ $address->embed }}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </main>
@endsection
