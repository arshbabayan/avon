@extends('main.extensions.profile-layout')

@php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

@section('title') @lang('main.edit') @endsection

@section('profile-content')
    <div class="col-sm-12 text-left">
        <p>
            <span class="color-pink">@lang('main.email'):</span>
            <span>{{ $user->email }}</span>
        </p>
        <p>
            <span class="color-pink">@lang('main.code'):</span>
            <span>{{ $user->code }}</span>
        </p>
        <p>
            <span class="color-pink">@lang('main.coordinator'):</span>
            @if(is_null($user->coordinator))
                <span>Avon Cosmetics</span>
            @else
                <span>{{ $user->coordinator->name . ' ' . $user->coordinator->surname }} ({{ $user->coordinator->code }})</span>
            @endif
        </p>
        <p>
            <span class="color-pink">@lang('main.role'):</span>
            <span>@lang('main.user_role_' . $user->role_id)</span>
        </p>
    </div>
    <form id="profile-edit-form-data" class="row">
        <div class="col-sm-12 form-registration">
            <label for="name">
                <input id="name" name="name" value="{{ $user->name }}" type="text" placeholder="@lang('main.name')">
                <small class="name"></small>
            </label>
            <label for="surname">
                <input id="surname" name="surname" value="{{ $user->surname }}" type="text" placeholder="@lang('main.surname')">
                <small class="surname"></small>
            </label>
            <label for="address">
                <input id="address" name="address" value="{{ $user->address }}" type="text" placeholder="@lang('main.address')">
                <small class="address"></small>
            </label>
            <label for="phone">
                <input id="phone" name="phone" value="{{ $user->phone }}" type="text" placeholder="@lang('main.phone')">
                <small class="phone"></small>
            </label>
            <label for="passport">
                <input id="passport" name="passport" value="{{ $user->passport }}" type="text" placeholder="@lang('main.passport')">
                <small class="passport"></small>
            </label>
        </div>
        <div class="w-100"></div>
        <button id="profile-edit-form-submit" class="btn bg-pink text-white mx-auto mt-4 outline-none">@lang('main.save')</button>
    </form>
@endsection
