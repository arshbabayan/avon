@extends('main.extensions.profile-layout')

@php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

@section('title') {{ $user->name . ' ' . $user->surname }}  @endsection

@section('profile-content')`
    <div>


        @if(session()->has('success_payment'))
            <ul class="alert alert-success" style="cursor: pointer" onclick="$(this).remove();">
                <li>{{ session()->get('success_payment') }}</li>
            </ul>
        @endif
        @if(session()->has('error_payment'))
            <ul class="alert alert-danger" style="cursor: pointer" onclick="$(this).remove();">
                <li>{{ session()->get('error_payment') }}</li>
            </ul>
        @endif
        <form method="POST" action="{{ route('locale.profile.converse_post', app()->getLocale()) }}">
            @csrf 
            <input type="number" min="10" name="price" value="10">
            <button class="btn bg-pink text-white" type="submit"> Добавить баланс </button>
        </form> <br>



              
        <table class="products-table">
            <thead>
                <tr>
                    <td>@lang('main.articul')</td>
                    <td>@lang('main.count')</td>
                    <td>@lang('main.product_name')</td>
                    <td>@lang('main.delete')</td>
                </tr>
            </thead>
            <tbody>
                <tr class="custom-pr-item d-none">
                    <td class="text-center w-12">
                        <input class="pr-articul" type="text" placeholder="@lang('main.articul')">
                    </td>
                    <td class="text-center w-12">
                        <input class="pr-count" type="number" value="1" min="1">
                    </td>
                    <td class="text-left w-64 position-relative">
                        <div class="pr-preloader">
                            <img src="{{ asset('main/pictures/preloader.gif') }}" alt="">
                        </div>
                        <input class="pr-title" type="text" placeholder="@lang('main.product_title')" readonly>
                    </td>
                    <td class="text-center w-12">
                        <i class="pr-delete fa fa-trash color-pink cursor-pointer"></i>
                    </td>
                </tr>
                <tr class="pr-item">
                    <td class="text-center w-12">
                        <input class="pr-articul" type="text" placeholder="@lang('main.articul')">
                    </td>
                    <td class="text-center w-12">
                        <input class="pr-count" type="number" value="1" min="1">
                    </td>
                    <td class="text-left w-64 position-relative">
                        <div class="pr-preloader">
                            <img src="{{ asset('main/pictures/preloader.gif') }}" alt="">
                        </div>
                        <input class="pr-title" type="text" placeholder="@lang('main.product_title')" readonly>
                    </td>
                    <td class="text-center w-12">
                        <i class="pr-delete fa fa-trash color-pink cursor-pointer"></i>
                    </td>
                </tr>

                <tr class="last-tr">
                    <td class="text-center w-12">
                        <i class="pr-add fa fa-plus color-pink cursor-pointer"></i>
                    </td>
                    <td colspan="2"></td>
                    <td class="text-center w-12">
                        <a class="add-to-baskets cursor-pointer btn bg-pink text-white" role="button">@lang('main.to_basket')</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    @if(!is_null($active_catalog) && count($active_catalog->pages) > 0)
        <a href="{{ route('locale.catalog.book', ['locale' => $locale['prefix'], 'name' => $active_catalog['name']]) }}">
            <img class="img-fluid" src="{{ asset('main/pages/' . $active_catalog->pages[0]->image) }}" alt="{{ $active_catalog->name }}" title="{{ $active_catalog->name }}">
        </a>
    @endif
@endsection
