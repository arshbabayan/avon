@extends('main.extensions.profile-layout')

@php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

@section('title') @lang('main.my_orders') @endsection

@section('profile-content')
    @if(count($orders) > 0)
        <div class="col-12">
            @foreach($orders as $order)
                <div class="order-container">
                    <div class="order-title">
                        <div>
                            <p class="label-text">@lang('main.payed')</p>
                            <p class="value-text">{{ $order->payed }} @lang('main.dram')</p>
                        </div>
                        <div>
                            <p class="label-text">@lang('main.will_be_pay')</p>
                            <p class="value-text">{{ $order->will_be_pay }} @lang('main.dram')</p>
                        </div>
                        <div>
                            <p class="label-text">@lang('main.status')</p>
                            <p class="value-text">{{ trans('main.order_status_' . $order->status) }}</p>
                        </div>
                        <div>
                            <p class="label-text">@lang('main.returned')</p>
                            <p class="value-text">{{ count($order->returnedProducts) > 0 ? count($order->returnedProducts) : trans('admin.no') }}</p>
                        </div>
                        <div>
                            <p class="label-text">@lang('main.ordered_at')</p>
                            <p class="value-text">{{ $order->created_at }}</p>
                        </div>
                        <div>
                            <button type="button" class="open-order btn text-white bg-pink outline-none font-size-13 font-size-md-16">
                                @lang('main.open')
                            </button>
                        </div>
                    </div>
                    <div class="order-content" style="display: none">
                        <h3 style="margin: 10px 0">@lang('main.order_number') {{ $order->id }}</h3>

                        <div class="align-items-center">
                            @if(count($order->withDiscountProducts) > 0)
                                <div class="col-12 p-0 mb-4">
                                    <h4 class="color-pink">@lang('main.products_with_discount')</h4>
                                    <div class="basket-container">
                                        <div class="row basket-title m-0 bb-2-pink">
                                            <div class="col-md-6 br-2-pink ">@lang('main.in_basket_name')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_price')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_count')</div>
                                            <div class="col-md-2">@lang('main.in_basket_total')</div>
                                        </div>
                                        <div class="basket-content m-0">
                                            @foreach($order->withDiscountProducts as $item)
                                                <div class="row m-0 bb-2-pink">
                                                    <div class="col-md-6 br-2-pink">
                                                        <div class="media align-items-center flex-column flex-md-row">
                                                            <div>
                                                                <img class="img-fluid mr-1" width="100"
                                                                     src="{{ $item->product->topImage() }}"
                                                                     alt="{{ $item->product->translate('title') }}"
                                                                     title="{{ $item->product->translate('title') }}">
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="m-0">{{ $item->product->translate('title') }}</h6>
                                                            </div>
                                                            <p class="m-0"><b>{{ $item->product->articul }}</b></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex flex-column justify-content-center align-items-center">
                                                        <span class="font-weight-bold text-pink font-size-sm-22">
                                                            <span>{{ $item->price }}</span>
                                                            @lang('main.dram')
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex justify-content-center align-items-center">
                                                        <span class="font-weight-bold text-pink font-size-sm-22">
                                                            <span>{{ $item->count }}</span>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2 d-flex font-weight-bold text-pink font-size-sm-22 justify-content-center align-items-center">
                                                        <span>{{ $item->count * $item->price }}</span> @lang('main.dram')
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="row basket-footer m-0">
                                            <div class="col-md-4 br-2-pink d-flex justify-content-center align-items-center">
                                                @lang('main.products_with_discount_total')
                                                <span class="font-weight-bold text-pink font-size-sm-15 ml-2">
                                                    <span class="products_with_discount_total">
                                                        {{ $order->products_with_discount_total }}
                                                    </span>
                                                    @lang('main.dram')
                                                </span>
                                            </div>
                                            <div class="col-md-4 br-2-pink d-flex justify-content-center align-items-center">
                                                @lang('main.affordable_discount')
                                                <span class="font-weight-bold text-pink font-size-sm-15 ml-2">
                                                    <span class="affordable_discount">
                                                        {{ $order->affordable_discount }}
                                                    </span>
                                                    %
                                                </span>
                                            </div>
                                            <div class="col-md-4 d-flex justify-content-center align-items-center">
                                                @lang('main.products_with_discount_finally_total')
                                                <span class="font-weight-bold text-pink font-size-sm-15 ml-2">
                                                    <span class="products_with_discount_finally_total">
                                                        {{ $order->products_with_discount_finally_total }}
                                                    </span>
                                                    @lang('main.dram')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(count($order->notDiscountProducts) > 0)
                                <div class="col-12 p-0 mb-4">
                                    <h4 class="color-pink">@lang('main.products_not_discount')</h4>
                                    <div class="basket-container">
                                        <div class="row basket-title m-0 bb-2-pink">
                                            <div class="col-md-6 br-2-pink ">@lang('main.in_basket_name')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_price')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_count')</div>
                                            <div class="col-md-2">@lang('main.in_basket_total')</div>
                                        </div>
                                        <div class="basket-content m-0">
                                            @foreach($order->notDiscountProducts as $item)
                                                <div class="row m-0 bb-2-pink">
                                                    <div class="col-md-6 br-2-pink">
                                                        <div class="media align-items-center flex-column flex-md-row">
                                                            <div>
                                                                <img class="img-fluid mr-1" width="100"
                                                                     src="{{ $item->product->topImage() }}"
                                                                     alt="{{ $item->product->translate('title') }}"
                                                                     title="{{ $item->product->translate('title') }}">
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="m-0">{{ $item->product->translate('title') }}</h6>
                                                            </div>
                                                            <p class="m-0"><b>{{ $item->product->articul }}</b></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex flex-column justify-content-center align-items-center">
                                                        <span class="font-weight-bold text-pink font-size-sm-22">
                                                            <span>{{ $item->price }}</span>
                                                            @lang('main.dram')
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex justify-content-center align-items-center">
                                                        <span class="font-weight-bold text-pink font-size-sm-22">
                                                            <span>{{ $item->count }}</span>
                                                        </span>
                                                    </div>
                                                    <div class="col-md-2 d-flex font-weight-bold text-pink font-size-sm-22 justify-content-center align-items-center">
                                                        <span>{{ $item->count * $item->price }}</span> @lang('main.dram')
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="row basket-footer m-0">
                                            <div class="col-md-12 br-2-pink d-flex justify-content-center align-items-center">
                                                @lang('main.products_not_discount_finally_total')
                                                    <span class="font-weight-bold text-pink font-size-sm-15 ml-2">
                                                    <span class="products_not_discount_finally_total">
                                                        {{ $order->products_not_discount_finally_total }}
                                                    </span>
                                                    @lang('main.dram')
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(count($order->returnedProducts) > 0)
                                <div class="col-12 p-0 mb-4 mt-4">
                                    <h4 class="color-pink">@lang('main.returned')</h4>
                                    <div class="basket-container bb-none">
                                        <div class="row basket-title m-0 bb-2-pink">
                                            <div class="col-md-4 br-2-pink ">@lang('main.in_basket_name')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_price')</div>
                                            <div class="col-md-2 br-2-pink ">@lang('main.in_basket_count')</div>
                                            <div class="col-md-2 br-2-pink">@lang('main.in_basket_total')</div>
                                            <div class="col-md-2">@lang('main.canceled_at')</div>
                                        </div>
                                        <div class="basket-content m-0">
                                            @foreach($order->returnedProducts as $item)
                                                <div class="row m-0 bb-2-pink">
                                                    <div class="col-md-4 br-2-pink">
                                                        <div class="media align-items-center flex-column flex-md-row">
                                                            <div>
                                                                <img class="img-fluid mr-1" width="100"
                                                                     src="{{ $item->product->topImage() }}"
                                                                     alt="{{ $item->product->translate('title') }}"
                                                                     title="{{ $item->product->translate('title') }}">
                                                            </div>
                                                            <div class="media-body">
                                                                <h6 class="font-size-sm-11 m-0">{{ $item->product->translate('title') }}</h6>
                                                                <p class="m-0"><b>{{ $item->product->articul }}</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex flex-column justify-content-center align-items-center">
                                                    <span class="font-weight-bold text-pink font-size-sm-22">
                                                        <span>{{ $item->price }}</span>
                                                        @lang('main.dram')
                                                    </span>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex justify-content-center align-items-center">
                                                    <span class="font-weight-bold text-pink font-size-sm-22">
                                                        <span>{{ $item->count }}</span>
                                                    </span>
                                                    </div>
                                                    <div class="col-md-2 br-2-pink d-flex font-weight-bold text-pink font-size-sm-22 justify-content-center align-items-center">
                                                        <span>{{ $item->count * $item->price }}</span> @lang('main.dram')
                                                    </div>
                                                    <div class="col-md-2 d-flex font-weight-bold text-pink font-size-sm-16 justify-content-center align-items-center">
                                                        <span>{{ $item->returned_at }}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if($order->status !== 3)
                                <div class="col-12 p-0 mb-2 final_calculation">
                                    <h4 class="color-pink">@lang('main.final_calculation')</h4>
                                    <div class="basket-container">
                                        <div>
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.products_finally_total')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span>+</span>
                                                <span class="products_finally_total mr-1">{{ $order->products_finally_total }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                        <div>
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.basket_bonus')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span>-</span>
                                                <span class="basket_bonus mr-1">{{ $order->bonus }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                        <div>
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.transportation_costs')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span>+</span>
                                                <span class="transportation_costs mr-1">{{ $order->transportation_costs }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                        <div>
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.service_fee')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span>+</span>
                                                <span class="service_fee mr-1">{{ $order->service_fee }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                        <div>
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.packaging')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span>+</span>
                                                <span class="packaging mr-1">{{ $order->packaging }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                        @if($order->journal)
                                            <div>
                                                <div class="font-size-md-20 font-weight-bold">@lang('main.journal')</div>
                                                <div class="font-size-md-20 color-pink font-weight-bold">
                                                    <span>+</span>
                                                    <span class="journal mr-1">{{ $order->journal }}</span>
                                                    @lang('main.dram')
                                                </div>
                                            </div>
                                        @endif
                                        <div class="bt-2-pink">
                                            <div class="font-size-md-20 font-weight-bold">@lang('main.finally_total')</div>
                                            <div class="font-size-md-20 color-pink font-weight-bold">
                                                <span class="finally_total mr-1">{{ $order->finally_total }}</span>
                                                @lang('main.dram')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if($order->in_processed_at)
                                <div class="col-12 address-area">
                                    <span class="label-text">@lang('main.in_processed_at')</span>
                                    <span class="value-text">{{ $order->in_processed_at }}</span>
                                </div>
                            @endif
                            @if($order->payed_at)
                                <div class="col-12 address-area">
                                    <span class="label-text">@lang('main.payed_at')</span>
                                    <span class="value-text">{{ $order->payed_at }}</span>
                                </div>
                            @endif
                            @if($order->finished_at)
                                <div class="col-12 address-area">
                                    <span class="label-text">@lang('main.finished_at')</span>
                                    <span class="value-text">{{ $order->finished_at }}</span>
                                </div>
                            @endif
                            @if($order->canceled_at)
                                <div class="col-12 address-area">
                                    <span class="label-text">@lang('main.canceled_at')</span>
                                    <span class="value-text">{{ $order->canceled_at }}</span>
                                </div>
                            @endif
                            <div class="col-12 address-area">
                                <span class="label-text">@lang('main.address')</span>
                                <span class="value-text">{{ $order->address->translate('title') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-12 mt-4">
            @include('main.extensions.pagination', ['data' => $orders])
        </div>
    @else
        <p class="color-pink text-center w-100 mt-3">@lang('main.orders_empty_data')</p>
    @endif
@endsection
