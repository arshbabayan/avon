@extends('main.extensions.profile-layout')

@php $user = \Illuminate\Support\Facades\Auth::user(); @endphp

@section('title') @lang('main.reports') @endsection

@section('profile-content')
    <div class="report-area">
        <div class="select-area">
            <select class="catalogs-select">
                <option value="">@lang('main.check_catalog')</option>
                @foreach($catalogs as $catalog)
                    <option value="{{ $catalog->id }}">{{ $catalog->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <form class="download-report-form d-none" method="post" action="{{ route('locale.profile.export-report', ['locale' => $locale['prefix']]) }}">
                @csrf
                <input class="catalog_id" name="catalog_id" type="text">
            </form>
            <button disabled class="btn m-2 text-white bg-pink outline-none download-report" data-dismiss="modal">@lang('main.download') (xlsx)</button>
        </div>
    </div>
@endsection
