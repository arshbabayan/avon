<div class="modal fade" id="show-product-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header border-0 pt-2 pb-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="product-item modal-body text-center mx-3 mt-0">

                <div class="row">
                    <div class="col-md-5 ninja-slider-part">
                        <div id='ninja-slider' class="bg-white p-0">
                            <div class="ninja-slider-content">
                                <div class="slider-inner">
                                    <ul class="product-images-slider"></ul>
                                    <div class="fs-icon" title="Expand/Close"></div>
                                </div>
                                <div id="thumbnail-slider">
                                    <div class="inner">
                                        <ul class="product-images-inner"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7 text-left pl-md-5">
                        <h3 class="product-title"></h3>
                        <p class="m-0">
                            @lang('main.articul') <b class="product-articul"></b>
                        </p>
                        <span class="product-price-area text-muted my-3">
                            <del>
                                <span class="product-price"></span> @lang('main.dram')
                            </del>
                            @lang('main.normal_price')
                        </span>
                        <br>
                        <span class="text-pink font-size-md-25">
                            <span class="product-discount"></span>@lang('main.dram')
                        </span>

                        <div class="num-counter mt-4">
                            <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                            <input readonly class="product-count" type="text">
                            <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                            <br>
                            <br>
                            <a class="product-to-basket btn bg-pink text-white" role="button" style="cursor:pointer">@lang('main.to_basket')</a>
                        </div>
                    </div>

                    <div class="product-info col-12 second-modal mt-4 mt-md-5">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link text-pink active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                   aria-selected="true">@lang('main.description')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-pink" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                                   aria-selected="false">@lang('main.composition')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-pink" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                                   aria-selected="false">@lang('main.reviews')</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent" style="border: 1px solid #dee2e6; border-top: none">
                            <div class="product-description p-2 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"></div>
                            <div class="product-composition p-2 tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"></div>
                            <div class="product-reviews p-2 tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
