<div class="modal fade" id="checkout_order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center mx-2 mx-md-5">
                <h2 class="message-title mb-2">@lang('main.are_you_sure')</h2>
                <div class="d-flex justify-content-center align-items-center">
                    <button id="confirm_order" type="button" class="btn m-2 text-white bg-pink outline-none">@lang('main.yes')</button>
                    <button type="button" class="btn m-2 text-white bg-pink outline-none" data-dismiss="modal">@lang('main.no')</button>
                </div>
            </div>
        </div>
    </div>
</div>
