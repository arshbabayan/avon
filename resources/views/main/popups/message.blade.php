<div class="modal fade" id="message-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center mx-2 mx-md-5">
                <h2 class="message-title mb-2"></h2>
                <p class="message-text"></p>
                <button type="button" class="btn text-white bg-pink outline-none" data-dismiss="modal">@lang('main.close')</button>
            </div>
        </div>
    </div>
</div>
