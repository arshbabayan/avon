<div class="modal fade" id="faq-3" tabindex="-1" role="dialog" aria-labelledby="faq-3Title" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-2 mx-md-5">
                {!! \App\Helpers\Actions::getInfoTranslate($current_information['how-to-become-a-regional-manager'], 'content') !!}
            </div>
        </div>
    </div>
</div>
