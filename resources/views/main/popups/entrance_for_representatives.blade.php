<div class="modal fade" id="entrance_for_representatives" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center mx-2 mx-md-5">
                <h3 class="mb-4">@lang('main.login')</h3>
                <form id="sign-in-form-data">
                    <div class="col-md-6 mx-auto">
                        <p class="color-pink sign-in-message" style="display:none"></p>
                        <div class="form-group">
                            <input name="code" type="text" class="form-control outline-none" placeholder="@lang('main.code')">
                        </div>
                        <div class="form-group">
                            <input name="password" type="password" class="form-control outline-none" placeholder="@lang('main.password')">
                        </div>
                        <button id="sign-in-form-submit" class="btn text-white bg-pink my-3 outline-none">@lang('main.sign_in')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
