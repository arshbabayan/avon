<div class="modal fade" id="page-products-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0 pt-2 pb-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center mx-3 mt-0 mb-4">
                <div class="product-item product-custom media align-items-center flex-column flex-md-row d-none">
                    <img class="product-image img-fluid mr-3" src="" width="100" alt="@lang('main.click_for_more_info')" title="@lang('main.click_for_more_info')">
                    <div class="media-body d-md-flex align-items-center justify-content-between text-left">
                        <div>
                            <h6 class="product-title"></h6>
                            <p class="m-0">
                                @lang('main.articul') <b class="product-articul"></b>
                            </p>
                            <p class="product-price-area text-muted m-0">
                                <del>
                                    <span class="product-price"></span> @lang('main.dram')
                                </del>
                                @lang('main.normal_price')
                            </p>
                            <span class="font-weight-bold text-pink font-size-sm-22">
                                 <span class="product-discount"></span> @lang('main.dram')
                            </span>
                        </div>
                        <div class="num-counter text-md-right mt-3 mt-md-0">
                            <button class="product-count-change" data-type="minus"><i class="fas fa-minus"></i></button>
                            <input readonly class="product-count" type="text">
                            <button class="product-count-change" data-type="plus"><i class="fas fa-plus"></i></button>
                            <a class="product-to-basket btn bg-pink text-white ml-2" role="button" style="cursor: pointer">@lang('main.to_basket')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
