@extends('layouts.admin')

@section('content')
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        @if(session()->has('password_error'))
            <ul class="alert alert-danger list_type_none">
                <li>{{ session()->get('password_error') }}</li>
            </ul>
        @endif

        <form method="post" class="form-horizontal" action="{{ route('locale.admin.update-admin',$locale['prefix']) }}">
            @csrf
            <div class="form-group">
                <label for="email" class="control-label">@lang('admin.name')</label>
                <input disabled type="text" id="email" value="{{ Auth::user()->email }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="new_password" class="control-label">@lang('admin.new_password')</label>
                <input type="password" name="new_password" id="new_password" class="form-control">
            </div>
            <div class="form-group">
                <label for="retry_password" class="control-label">@lang('admin.retry_password')</label>
                <input type="password" name="retry_password" id="retry_password" class="form-control">
            </div>
            <div class="form-group">
                <label for="current_password" class="control-label">@lang('admin.current_password')</label>
                <input type="password" name="current_password" id="current_password" class="form-control">
            </div>
            <div class="form-group text-right">
                <input type="submit" class="btn btn-success" value="@lang('admin.update')">
            </div>
        </form>
    </div>
@endsection
