<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Products</title>
</head>
<body>

<table>
    <thead>
    <tr>
        <th>@lang('admin.product_name')</th>
        <th>@lang('admin.articul')</th>
        <th>@lang('admin.count')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $product_id => $items)
        <tr>
            @php $count = 0; @endphp
            @foreach($items as $item)
                @php $count += $item->count; @endphp
            @endforeach
            <td>{{ $item->product->translate('title') }}</td>
            <td>{{ $item->product->articul }}</td>
            <td>{{ $count }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>
