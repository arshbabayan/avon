<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order</title>
</head>
<body>

<table>
    <thead>
    <tr>
        <th>#</th>
        <th>@lang('admin.ordered_at')</th>
        <th>@lang('admin.role')</th>
        <th>@lang('admin.address')</th>
        <th>@lang('admin.coordinator')</th>
        <th>@lang('admin.the_catalog')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $order)
        <tr>
            <td style="text-align: left">{{ $order->id }}</td>
            <td>{{ $order->created_at }}</td>
            <td>{{ trans('admin.order_status_' . $order->status) }}</td>
            <td>{{ $order->address->translate('title') }}</td>
            <td>{{ $order['user']['name'] . ' ' . $order['user']['surname'] }} ({{ $order['user']['code'] }})</td>
            <td>{{ $order->catalog->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>
