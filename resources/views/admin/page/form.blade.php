<div class="d-flex d-justify-center d-align-center">
    @include('admin.extensions.image',[
        'label' => trans('admin.image'),
        'name' => 'image',
        'path' => 'main/pages',
        'item' => $page ?? null
    ])
</div>

@include('admin.extensions.multiselect',[
    'label' => trans('admin.products'),
    'name' => 'products',
    'options' => $products,
    'selected_values' => isset($page) ? $page->products->pluck('id')->toArray() : [],
    'field' => 'title',
    'field_two' => 'articul',
    'translate' => true,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
