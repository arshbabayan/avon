@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.add'),
                'icon' => 'plus'
            ],
            [
                'route' => route('locale.admin.catalog.page.index',['locale' => $locale['prefix'], 'catalog' => $catalog->id]),
                'text' => trans('admin.pages'),
                'icon' => 'leanpub'
            ],
            [
                'route' => route('locale.admin.catalog.show',['locale' => $locale['prefix'], 'catalog' => $catalog->id]),
                'text' => $catalog->name,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.catalog.index',$locale['prefix']),
                'text' => trans('admin.catalogs'),
                'icon' => 'book'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => route('locale.admin.catalog.page.store',['locale' => $locale['prefix'], 'catalog' => $catalog->id]), 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('admin.page.form', ['formMode' => 'create'])

        {!! Form::close() !!}
    </div>
@endsection
