@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.pages'),
                'icon' => 'leanpub'
            ],
            [
                'route' => route('locale.admin.catalog.show',['locale' => $locale['prefix'], 'catalog' => $catalog->id]),
                'text' => $catalog->name,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.catalog.index',$locale['prefix']),
                'text' => trans('admin.catalogs'),
                'icon' => 'book'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header d-flex d-justify-between d-align-center">
        <div>
            <a href="{{ route('locale.admin.catalog.page.create',['locale' => $locale['prefix'], 'catalog' => $catalog->id]) }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('admin.add')
            </a>
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> @lang('admin.image') </th>
                        <th style="text-align: right">@lang('admin.actions')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($pages as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="image_part" style="height: 350px!important">
                            <img src="{{ asset('main/pages/' . $item->image) }}" alt="">
                        </td>
                        <td style="text-align: right">
                            <a href="#">
                                <button class="btn btn-warning btn-sm" style="color: white">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    @lang('admin.products') ({{ $item->products_count }})
                                </button>
                            </a>
                            <a href="{{ route('locale.admin.catalog.page.show', ['locale' => $locale['prefix'], 'catalog' => $catalog->id, 'page' => $item->id]) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> @lang('admin.show')</button></a>
                            <a href="{{ route('locale.admin.catalog.page.edit', ['locale' => $locale['prefix'], 'catalog' => $catalog->id, 'page' => $item->id]) }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('locale.admin.catalog.page.destroy', ['locale' => $locale['prefix'], 'catalog' => $catalog->id, 'page' => $item->id]),
                                'style' => 'display:inline',
                                'class' => 'delete_form'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
                                'type' => 'button',
                                'class' => 'btn btn-danger btn-sm delete',
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $pages->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
