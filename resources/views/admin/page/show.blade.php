@extends('layouts.admin')


@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $page->id,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.catalog.page.index',['locale' => $locale['prefix'], 'catalog' => $catalog->id]),
                'text' => trans('admin.pages'),
                'icon' => 'leanpub'
            ],
            [
                'route' => route('locale.admin.catalog.show',['locale' => $locale['prefix'], 'catalog' => $catalog->id]),
                'text' => $catalog->name,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.catalog.index',$locale['prefix']),
                'text' => trans('admin.catalogs'),
                'icon' => 'book'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.catalog.page.edit', ['locale' => $locale['prefix'], 'catalog' => $catalog->id, 'page' => $page->id]) }}" title="Edit Page">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>

        {!! Form::open([
            'method'=>'DELETE',
            'url' => route('locale.admin.catalog.page.destroy', ['locale' => $locale['prefix'], 'catalog' => $catalog->id, 'page' => $page->id]),
            'style' => 'display:inline',
            'class' => 'delete_form'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
            'type' => 'button',
            'class' => 'btn btn-danger btn-sm delete',
        )) !!}
        {!! Form::close() !!}
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $page->id }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.image') </th>
                        <td>
                            <img src="{{ asset('main/pages/' . $page->image) }}" alt="">
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.products') ({{ count($page->products) }}) </th>
                        <td>
                            <ul style="margin: 0; padding:0; list-style-type: none">
                                @foreach($page->products as $product)
                                    <li>
                                        <a href="{{ route('locale.admin.product.show',['locale' => $locale['prefix'], 'product' => $product->id]) }}" class="d-flex d-align-center">
                                            <img src="{{ $product->topImage() }}" alt="" style="width: 50px; height: 50px">
                                            {{ $product->translate('title') }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td>{{ $page->created_at }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td>{{ $page->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
