@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.information'),
                'icon' => 'list-alt'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="m-10">
        <div class="card-body information_body">
            @foreach($information as $information_item)
                <a href="{{ route('locale.admin.information.show',['locale' => $locale['prefix'], 'prefix' => $information_item['prefix']]) }}">
                    <button class="btn btn-success btn-sm">
                        @lang('admin.' . $information_item['prefix'])
                    </button>
                </a>
            @endforeach
        </div>
    </div>
@endsection
