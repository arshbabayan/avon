@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.' . $information['prefix']),
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.information.index',$locale['prefix']),
                'text' => trans('admin.information'),
                'icon' => 'list-alt'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::model($information, [
            'method' => 'PUT',
            'url' => route('locale.admin.information.update',['locale' => $locale['prefix'], 'prefix' => $information['prefix']]),
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('admin.information.forms.' . $information['prefix'], ['formMode' => 'edit'])

        {!! Form::close() !!}
    </div>
@endsection
