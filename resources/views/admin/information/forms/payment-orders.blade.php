@include('admin.extensions.language-json', [
    'fields' => [
        [
            'label' => trans('admin.title'),
            'name' => 'title',
            'type' => 'text'
        ],
        [
            'label' => trans('admin.content'),
            'name' => 'content',
            'type' => 'floara'
        ]
    ],
    'item' => $information,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
