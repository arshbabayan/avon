@include('admin.extensions.language-json', [
    'fields' => [
        [
            'label' => trans('admin.title'),
            'name' => 'title',
            'type' => 'text'
        ],
        [
            'label' => trans('admin.description'),
            'name' => 'description',
            'type' => 'text'
        ],
        [
            'label' => trans('admin.content'),
            'name' => 'content',
            'type' => 'floara'
        ],
        [
            'label' => trans('admin.become_a_representative'),
            'name' => 'become_a_representative',
            'type' => 'floara'
        ],
        [
            'label' => trans('admin.become_a_coordinator'),
            'name' => 'become_a_coordinator',
            'type' => 'floara'
        ],
    ],
    'item' => $information,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
