<div class="form-group {{ $errors->has('conversion_factor') ? 'has-error' : ''}}">
    {!! Form::label('conversion_factor', trans('admin.conversion_factor'), ['class' => 'control-label']) !!}
    {!! Form::text('conversion_factor', json_decode($information->info)->conversion_factor ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('phones_info_line') ? 'has-error' : ''}}">
    {!! Form::label('phones_info_line', trans('admin.phones_info_line'), ['class' => 'control-label']) !!}
    {!! Form::textarea('phones_info_line', json_decode($information->info)->phones_info_line ?? '', ['class' => 'form-control']) !!}
</div>

@include('admin.extensions.language-json', [
    'fields' => [
        [
            'label' => trans('admin.footer_content'),
            'name' => 'footer_content',
            'type' => 'floara'
        ]
    ],
    'item' => $information,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
