<div class="form-group {{ $errors->has('service_fee') ? 'has-error' : ''}}">
    {!! Form::label('service_fee', trans('admin.service_fee'), ['class' => 'control-label']) !!}
    {!! Form::number('service_fee', json_decode($information->info)->service_fee ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('packaging') ? 'has-error' : ''}}">
    {!! Form::label('packaging', trans('admin.packaging'), ['class' => 'control-label']) !!}
    {!! Form::number('packaging', json_decode($information->info)->packaging ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('journal') ? 'has-error' : ''}}">
    {!! Form::label('journal', trans('admin.journal'), ['class' => 'control-label']) !!}
    {!! Form::number('journal', json_decode($information->info)->journal ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('max_size') ? 'has-error' : ''}}">
    {!! Form::label('max_size', trans('admin.max_size'), ['class' => 'control-label']) !!}
    {!! Form::number('max_size', json_decode($information->info)->max_size ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
