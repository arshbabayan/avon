<div class="form-group">
    {!! Form::label('instagram', trans('admin.instagram'), ['class' => 'control-label']) !!}
    {!! Form::text('instagram', json_decode($information->info)->instagram ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', trans('admin.facebook'), ['class' => 'control-label']) !!}
    {!! Form::text('facebook', json_decode($information->info)->facebook ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('vk', trans('admin.vk'), ['class' => 'control-label']) !!}
    {!! Form::text('vk', json_decode($information->info)->vk ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
