@include('admin.extensions.language-json', [
    'fields' => [
        [
            'label' => trans('admin.title'),
            'name' => 'title',
            'type' => 'text'
        ],
    ],
    'item' => $information,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
