<!doctype html>
<html lang="{{ $locale['prefix'] }}">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>@lang('admin.login')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('dashboard/assets/css/turbo.css') }}" rel="stylesheet"/>
    <link href="{{ asset('dashboard/assets/css/demo.css') }}" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
    <link rel="icon" href="{{ asset('main/img/icon/logo.ico') }}" type="ico" sizes="16x16">
</head>

<body>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="blue">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="post" action="{{ route('locale.admin.sign',$locale['prefix']) }}">
                            @csrf
                            <div class="card card-login card-hidden">
                                <div class="card-header text-center">
                                    <h4 class="card-title">@lang('admin.login')</h4>
                                </div>
                                <div class="card-content">
                                    @if(session()->has('invalid_admin'))
                                        <h5 class="c-red text-center">@lang('admin.invalid_admin')</h5>
                                    @endif
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">@lang('admin.name')</label>
                                            <input name="email" type="text" class="form-control" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">@lang('admin.password')</label>
                                            <input name="password" type="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-wd btn-md btn-success">@lang('admin.sign_in')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="{{ asset('dashboard/assets/vendors/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/material.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/moment.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/chartist.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-notify.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/nouislider.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/turbo.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/demo.js') }}"></script>

<script type="text/javascript">
    $().ready(function () {
        demo.checkFullPageBackgroundImage();
        setTimeout(function () {
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>
</html>
