@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.edit'),
                'icon' => 'pencil'
            ],
            [
                'route' => route('locale.admin.slider.show',['locale' => $locale['prefix'], 'slider' => $slider->id]),
                'text' => $slider->id,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.slider.index',$locale['prefix']),
                'text' => trans('admin.sliders'),
                'icon' => 'picture-o'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

       {!! Form::model($slider, [
           'method' => 'PATCH',
           'url' => route('locale.admin.slider.update',['locale' => $locale['prefix'], 'slider' => $slider->id]),
           'class' => 'form-horizontal',
           'files' => true
       ]) !!}

       @include ('admin.slider.form', ['formMode' => 'edit'])

       {!! Form::close() !!}
    </div>
@endsection
