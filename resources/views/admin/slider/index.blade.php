@extends('layouts.admin')

@section('content')
    <div class="card-header d-flex d-justify-between d-align-center">
        <div>
            <a href="{{ route('locale.admin.slider.create',$locale['prefix']) }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('admin.add')
            </a>
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('admin.image')</th>
                        <th style="text-align: right">@lang('admin.actions')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($sliders as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="image_part">
                            <img src="{{ asset('main/sliders/' . $item->image) }}" alt="">
                        </td>
                        <td style="text-align: right">
                            <a href="{{ route('locale.admin.slider.show',['locale' => $locale['prefix'], 'slider' => $item->id]) }}" title="View Slider"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> @lang('admin.show')</button></a>
                            <a href="{{ route('locale.admin.slider.edit',['locale' => $locale['prefix'], 'slider' => $item->id]) }}" title="Edit Slider"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('locale.admin.slider.destroy',['locale' => $locale['prefix'], 'slider' => $item->id]),
                                'style' => 'display:inline',
                                'class' => 'delete_form'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
                                'type' => 'button',
                                'class' => 'btn btn-danger btn-sm delete',
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $sliders->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
