@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $slider->id,
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.slider.index',$locale['prefix']),
                'text' => trans('admin.sliders'),
                'icon' => 'picture-o'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.slider.edit',['locale' => $locale['prefix'], 'slider' => $slider->id]) }}" title="Edit Slider">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>

        {!! Form::open([
            'method'=>'DELETE',
            'url' => route('locale.admin.slider.destroy',['locale' => $locale['prefix'], 'slider' => $slider->id]),
            'style' => 'display:inline',
            'class' => 'delete_form'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
            'type' => 'button',
            'class' => 'btn btn-danger btn-sm delete',
        )) !!}
        {!! Form::close() !!}
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $slider->id }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.image') </th>
                        <td>
                            <img src="{{ asset('main/sliders/' . $slider->image) }}" alt="">
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td>{{ $slider->created_at }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td>{{ $slider->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
