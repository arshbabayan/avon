<div class="d-flex d-justify-center d-align-center">
    @include('admin.extensions.image',[
        'label' => trans('admin.image'),
        'name' => 'image',
        'path' => 'main/sliders',
        'item' => $slider ?? null
    ])
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
