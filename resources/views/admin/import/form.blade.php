<div class="d-flex d-justify-center d-align-center import-area-admin">
    @include('admin.extensions.select',[
        'name' => 'catalog',
        'label' => null,
        'placeholder' => trans('admin.choose_catalog'),
        'selected' => old('catalog', null),
        'options' => $catalogs,
        'translate' => false,
        'field' => 'name'
    ])
    <button type="button" class="btn btn-success">
        <span>@lang('admin.import_file') (xlsx)</span>
        <input name="import_file" type="file" class="custom-input-file">
    </button>
    <button type="button" class="btn btn-warning">
        <span>@lang('admin.pages_images') (zip)</span>
        <input name="pages_images" type="file" class="custom-input-file">
    </button>
    <button type="button" class="btn btn-primary">
        <span>@lang('admin.products_images') (zip)</span>
        <input name="products_images" type="file" class="custom-input-file">
    </button>
</div>

@include('admin.import.errors')

@include('admin.import.config')

<div class="form-group text-right">
    {!! Form::submit(trans('admin.import') , ['class' => 'btn btn-success']) !!}
</div>
