@extends('layouts.admin')

@section('content')
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => route('locale.admin.import.importing',$locale['prefix']), 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('admin.import.form', ['formMode' => 'create'])

        {!! Form::close() !!}
    </div>
@endsection
