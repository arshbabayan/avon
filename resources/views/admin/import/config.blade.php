<span class="color-green">
    <b>Внимание:</b>
    <span>Сделайте заголовок вашего файла в следующем порядке`</span>
</span>
<br>

@foreach($requiredHeader as $key => $headerColumn)
    <span>
        <b>{{ $loop->iteration }}.</b>
        <span>{{ $headerColumn }}</span>
    </span>
    <br>
@endforeach
