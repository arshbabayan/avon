@if (session()->get('noHeader', false))
    <span class="color-red">
        <b>Ошибка:</b>
        <span>Заголовки отсутствуют.</span>
    </span>
    <br>
@endif

@if (session()->get('noRows', false))
    <span class="color-red">
        <b>Ошибка:</b>
        <span>Данные отсутствуют.</span>
    </span>
    <br>
@endif

@if (count(session()->get('headerExcepts', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>В составе файла товаров отсутствуют следующие поля`</span>
    </span>
    <ul>
        @foreach(session()->get('headerExcepts') as $headerExcept)
            <li>{{ $headerExcept }}</li>
        @endforeach
    </ul>
@endif

@if (count(session()->get('missedPagesImages', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>В папке @lang('admin.pages_images') отсутствуют следующие файлы՝</span>
    </span>
    <p>{{ implode(', ', session()->get('missedPagesImages')) }}</p>
@endif

@if (count(session()->get('missedProductsImages', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>В папке @lang('admin.products_images') отсутствуют следующие файлы՝</span>
    </span>
    <p>{{ implode(', ', session()->get('missedProductsImages')) }}</p>
@endif

@if (count(session()->get('equalArticuls', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>@lang('admin.equal_articuls_error')</span>
    </span>
    <p>{{ implode(', ', session()->get('equalArticuls')) }}</p>
@endif

@if (count(session()->get('equalSlugs', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>@lang('admin.equal_slugs_error')</span>
    </span>
    <ul>
        @foreach(session()->get('equalSlugs') as $equalSlug)
            <li>{{ $equalSlug }}</li>
        @endforeach
    </ul>
@endif

@if (count(session()->get('invalidRows', [])) > 0)
    <span class="color-red">
        <b>Ошибка: </b>
        <span>Следующие строки имеют неправильно заполненные поля`</span>
    </span>
    <table class="errors-table">
        <thead>
            <tr>
                <th>@lang('admin.row_number')</th>
                <th>@lang('admin.errors')</th>
            </tr>
        </thead>
        <tbody>
            @foreach(session()->get('invalidRows') as $row => $messages)
                <tr>
                    <td>{{ $row }}</td>
                    <td>
                        @foreach($messages as $column => $message)
                            <b>@lang('admin.' . $column):</b> <span>{{ $message }}</span> <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
