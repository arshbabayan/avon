@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.edit'),
                'icon' => 'pencil'
            ],
            [
                'route' => route('locale.admin.product.show',['locale' => $locale['prefix'], 'product' => $product->id]),
                'text' => $product->translate('title'),
                'icon' => null
            ],
            [
                'route' => route('locale.admin.product.index',$locale['prefix']),
                'text' => trans('admin.products'),
                'icon' => 'shopping-cart'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

       {!! Form::model($product, [
           'method' => 'PATCH',
           'url' => route('locale.admin.product.update',['locale' => $locale['prefix'], 'product' => $product->id]),
           'class' => 'form-horizontal',
           'files' => true
       ]) !!}

       @include ('admin.product.form', ['formMode' => 'edit'])

       {!! Form::close() !!}
    </div>
@endsection
