@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $product->translate('title'),
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.product.index',$locale['prefix']),
                'text' => trans('admin.products'),
                'icon' => 'shopping-cart'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.product.edit',['locale' => $locale['prefix'], 'product' => $product->id]) }}" title="Edit Product">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>

        {!! Form::open([
            'method'=>'DELETE',
            'url' => route('locale.admin.product.destroy',['locale' => $locale['prefix'], 'product' => $product->id]),
            'style' => 'display:inline',
            'class' => 'delete_form'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
            'type' => 'button',
            'class' => 'btn btn-danger btn-sm delete',
        )) !!}
        {!! Form::close() !!}
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $product->id }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.product_images') </th>
                        <td>
                            <div class="d-flex d-align-center">
                                @if (count($product->images) > 0)
                                    @foreach($product->images as $image)
                                        <img src="{{ asset('main/products/' . $image->image) }}" alt="" style="width:80px;height: 150px; object-fit: contain">
                                    @endforeach
                                @else
                                    <img src="{{ $product->topImage() }}" alt="" style="width:50px;height: 50px; object-fit: contain">
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.title') </th>
                        <td> {{ $product->translate('title') }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.description') </th>
                        <td> {!! $product->translate('description') !!} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.composition') </th>
                        <td> {!! $product->translate('composition') !!} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.slug') </th>
                        <td> {{ $product->slug }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.articul') </th>
                        <td> {{ $product->articul }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.price') </th>
                        <td> {{ $product->price }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.discount') </th>
                        <td> {{ $product->discount }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.discount_for_more') </th>
                        <td> {{ $product->discount_for_more }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.easy_start') </th>
                        <td> {{ $product->easy_start ? trans('admin.active') : trans('admin.inactive') }} </td>
                    </tr>
                    @if ($product->easy_start)
                        <tr>
                            <th> @lang('admin.easy_start_price') </th>
                            <td> {{ $product->easy_start_price }} </td>
                        </tr>
                        <tr>
                            <th> @lang('admin.easy_start_step') </th>
                            <td> {{ $product->easy_start_step }} </td>
                        </tr>
                    @endif
                    <tr>
                        <th> @lang('admin.not_discount') </th>
                        <td> {{ $product->not_discount ? trans('admin.yes') : trans('admin.no') }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.statuses') ({{ count($product->statuses) }}) </th>
                        <td>
                            <ul style="margin: 0; padding:0; list-style-type: none">
                                @foreach($product->statuses as $status)
                                    <li>{{ trans('admin.' . $status->prefix) }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td> {{ $product->created_at }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td> {{ $product->updated_at }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
