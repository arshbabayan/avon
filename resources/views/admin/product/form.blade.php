<div class="card m-0" style="padding: 15px">
    <div class="card-content p-0">

        <ul class="nav nav-pills nav-pills-warning d-flex d-justify-around">
            <li class="active">
                <a class="text-lowercase" href="#product_info" data-toggle="tab" aria-expanded="true">
                    @lang("admin.product_info")
                </a>
            </li>
            <li>
                <a class="text-lowercase" href="#product_images" data-toggle="tab" aria-expanded="false">
                    @lang("admin.product_images")
                </a>
            </li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="product_info">
                <div class="form-group {{ $errors->has('articul') ? 'has-error' : ''}}">
                    {!! Form::label('articul', trans('admin.articul'), ['class' => 'control-label']) !!}
                    {!! Form::text('articul', old('articul') ?? $product->articul ?? null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                    {!! Form::label('price', trans('admin.price'), ['class' => 'control-label']) !!}
                    {!! Form::number('price', $product->price ?? null, ['class' => 'form-control', 'min' => 0]) !!}
                </div>

                <div class="form-group {{ $errors->has('discount') ? 'has-error' : ''}}">
                    {!! Form::label('discount', trans('admin.discount'), ['class' => 'control-label']) !!}
                    {!! Form::number('discount', $product->discount ?? null, ['class' => 'form-control', 'min' => 0]) !!}
                </div>

                <div class="form-group {{ $errors->has('discount_for_more') ? 'has-error' : ''}}">
                    {!! Form::label('discount_for_more', trans('admin.discount_for_more'), ['class' => 'control-label']) !!}
                    {!! Form::number('discount_for_more', $product->discount_for_more ?? null, ['class' => 'form-control', 'min' => 0]) !!}
                </div>

                <div class="not_discount form-group {{ $errors->has('not_discount') ? 'has-error' : ''}}">
                    {!! Form::label('not_discount', trans('admin.not_discount'), ['class' => 'control-label']) !!}
                    {!! Form::checkbox('not_discount', '1', $product->not_discount ?? false) !!}
                </div>

                <div class="easy-start form-group {{ $errors->has('easy_start') ? 'has-error' : ''}}">
                    {!! Form::label('easy_start', trans('admin.easy_start'), ['class' => 'control-label']) !!}
                    {!! Form::checkbox('easy_start', '1', $product->easy_start ?? false) !!}
                </div>

                <div class="easy-start-price form-group {{ $errors->has('easy_start_price') ? 'has-error' : ''}}">
                    {!! Form::label('easy_start_price', trans('admin.easy_start_price'), ['class' => 'control-label']) !!}
                    {!! Form::number('easy_start_price', isset($product->easy_start_price) && $product->easy_start_price > 0 ? $product->easy_start_price : 500, ['class' => 'form-control', 'min' => 0]) !!}
                </div>

                <div class="easy-start-step form-group {{ $errors->has('easy_start_step') ? 'has-error' : ''}}">
                    {!! Form::label('easy_start_step', trans('admin.easy_start_step'), ['class' => 'control-label']) !!}
                    {!! Form::number('easy_start_step', isset($product->easy_start_step) && $product->easy_start_step > 0 ? $product->easy_start_step : 1, ['class' => 'form-control', 'min' => 1, 'max' => 4, 'novalidate']) !!}
                </div>

                @include('admin.extensions.multiselect',[
                    'label' => trans('admin.statuses'),
                    'name' => 'statuses',
                    'options' => $statuses,
                    'selected_values' => isset($product) ? $product->statuses->pluck('id')->toArray() : [],
                    'field' => 'prefix',
                    'translate' => 'inner'
                ])

                @include('admin.extensions.language',[
                    'fields' => [
                        [
                            'label' => trans('admin.title'),
                            'name' => 'title',
                            'type' => 'text',
                        ],
                        [
                            'label' => trans('admin.description'),
                            'name' => 'description',
                            'type' => 'floara',
                        ],
                        [
                            'label' => trans('admin.composition'),
                            'name' => 'composition',
                            'type' => 'floara',
                        ],
                    ],
                    'item' => $product ?? null,
                ])
            </div>

            <div class="tab-pane" id="product_images">
                @include('admin.extensions.multiple_images',[
                    'name' => 'product_images',
                    'field' => 'image',
                    'path' => 'main/products',
                    'delete_url' => 'locale.admin.product.delete-image',
                    'item' => $product ?? null,
                    'images' => $product->images ?? [],
                    'params' => ['locale' => $locale['prefix']],
                    'item_route_param_name' => 'product',
                    'item_route_param_value' => 'id',
                ])
            </div>

        </div>
    </div>
</div>


<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
