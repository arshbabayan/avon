@extends('layouts.admin')

@section('content')
    <div class="card-header d-flex d-justify-between d-align-center">
        <div>
            <a href="{{ route('locale.admin.product.create',$locale['prefix']) }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('admin.add')
            </a>
        </div>
        <div>
            {!! Form::open(['method' => 'GET', 'url' => route('locale.admin.product.index', $locale['prefix']), 'class' => 'form-inline', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="@lang('admin.title'), @lang('admin.articul')..." value="{{ request('search') }}">
                <span class="input-group-append">
                    <button class="btn btn-warning" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                     @if(request()->has('search'))
                        <a class="btn btn-warning" href="{{ route('locale.admin.product.index',$locale['prefix']) }}">
                            <i class="fa fa-remove"></i>
                        </a>
                    @endif
                </span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('admin.top_image')</th>
                        <th>@lang('admin.title')</th>
                        <th>@lang('admin.articul')</th>
                        <th style="text-align: right">@lang('admin.actions')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="image_part">
                            <img src="{{ $item->topImage() }}" alt="">
                        </td>
                        <td>{{ $item->translate('title') }}</td>
                        <td>{{ $item->articul }}</td>
                        <td style="text-align: right">
                            <a href="{{ route('locale.admin.product.show',['locale' => $locale['prefix'], 'product' => $item->id]) }}" title="View Product"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> @lang('admin.show')</button></a>
                            <a href="{{ route('locale.admin.product.edit',['locale' => $locale['prefix'], 'product' => $item->id]) }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('locale.admin.product.destroy',['locale' => $locale['prefix'], 'product' => $item->id]),
                                'style' => 'display:inline',
                                'class' => 'delete_form'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
                                'type' => 'button',
                                'class' => 'btn btn-danger btn-sm delete',
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $products->appends(request()->all())->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
