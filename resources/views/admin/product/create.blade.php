@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.add'),
                'icon' => 'plus'
            ],
            [
                'route' => route('locale.admin.product.index',$locale['prefix']),
                'text' => trans('admin.products'),
                'icon' => 'shopping-cart'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => route('locale.admin.product.store',$locale['prefix']), 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('admin.product.form', ['formMode' => 'create'])

        {!! Form::close() !!}
    </div>
@endsection
