@extends('layouts.admin')

@section('content')
    <div class="card-header">
        <form class="filter-form form-inline">
            @csrf
            <div>
                <div>
                    <p class="text-center m-0">@lang('admin.filter_date')</p>
                    <div>
                        <div class="col-md-6">
                            <div class="form-group m-0">
                                <label for="from-date"></label>
                                <input value="{{ request()->get('from_date') }}" name="from_date" type="text"
                                       id="from-date" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-0">
                                <label for="to-date"></label>
                                <input value="{{ request()->get('to_date') }}" name="to_date" type="text" id="to-date"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="text-center m-0">@lang('admin.address')</p>
                    <div class="form-group m-0 small_select">
                        <select name="address" class="selectpicker" data-live-search="true">
                            <option value="">@lang('admin.non_selected')</option>
                            @foreach($addresses as $address)
                                <option @if($address->id == request()->get('address')) selected
                                        @endif value="{{ $address->id }}">
                                    {{ $address->translate('title') }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <p class="text-center m-0">@lang('admin.coordinator')</p>
                    <div class="form-group m-0 small_select">
                        <select name="coordinator" class="selectpicker" data-live-search="true">
                            <option value="">@lang('admin.non_selected')</option>
                            @foreach($coordinators as $coordinator)
                                <option @if($coordinator->id == request()->get('coordinator')) selected
                                        @endif value="{{ $coordinator->id }}">
                                    {{ $coordinator->name . ' ' .$coordinator->surname }} ({{ $coordinator->code }})
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <p class="text-center m-0">@lang('admin.filter_articul')</p>
                    <div>
                        <div class="col-md-6">
                            <div class="form-group m-0">
                                <label for="from-articul"></label>
                                <input value="{{ request()->get('from_articul') }}" name="from_articul" type="number"
                                       id="from-articul" min="0" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group m-0">
                                <label for="to-articul"></label>
                                <input value="{{ request()->get('to_articul') }}" name="to_articul" type="number"
                                       id="to-articul" min="0" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="d-flex">
                    <p class="text-center m-0 return_checkbox">
                        <span>@lang('admin.canceled_orders')</span>
                        {!! Form::checkbox('returned', '1', request()->get('returned')) !!}
                    </p>
                </div>
            </div>
        </form>

        <div class="filter-buttons d-flex d-justify-center d-align-center">
            <button data-method="GET" data-action="{{ route('locale.admin.report.products', $locale['prefix']) }}"
                    class="btn btn-warning m-5" type="button">
                <i class="fa fa-search"></i>
            </button>
            <button data-method="POST"
                    data-action="{{ route('locale.admin.report.products.download', $locale['prefix']) }}"
                    class="btn btn-success m-5" type="button">
                <i class="fa fa-download"></i>
            </button>
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless" style="table-layout: fixed">
                <thead>
                <tr>
                    <th class="text-center">@lang('admin.product_name')</th>
                    <th class="text-center">@lang('admin.articul')</th>
                    <th class="text-center">@lang('admin.count')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $product_id => $items)
                    <tr>
                        @php $count = 0; @endphp
                        @foreach($items as $item)
                            @php $count += $item->count; @endphp
                        @endforeach
                        <td class="text-center">{{ $item->product->translate('title') }}</td>
                        <td class="text-center">{{ $item->product->articul }}</td>
                        <td class="text-center">{{ $count }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
{{--            <div class="pagination-wrapper"> {!! $data->appends(request()->all())->render() !!} </div>--}}
        </div>
    </div>
@endsection

@section('script')
    <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/Lightpick-master/css/lightpick.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
    <script src="{{ asset('dashboard/Lightpick-master/lightpick.js') }}"></script>

    <script>
        $(function () {
            //Range Picker for step two
            let fromDate = '{{ request()->get('from', null) }}';
            let toDate = '{{ request()->get('to', null) }}';

            new Lightpick({
                field: document.getElementById('from-date'),
                secondField: document.getElementById('to-date'),
                startDate: fromDate,
                endDate: toDate,
                lang: 'en',
                format: 'YYYY-MM-DD',
            });

            $('.filter-buttons button').click(function () {
                let form = $('.filter-form');
                form.attr('method', $(this).data('method'));
                form.attr('action', $(this).data('action'));
                if ($(this).data('method') === 'GET') {
                    $('.filter-form input[name="_token"]').remove();
                }
                form.submit();
            });
        })
    </script>
@endsection
