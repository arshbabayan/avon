<div>
    @if(isset($label))
        <div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
            {!! Form::label($name, $label, ['class' => 'control-label']) !!}
        </div>
    @endif

    <div class="form-group">
        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
                @if($formMode === 'edit' && !is_null($item[$field ?? $name]))
                    <img src="{{ asset($path.'/'.$item[$field ?? $name]) }}">
                @else
                    <img src="{{ asset('dashboard/assets/img/image_placeholder.jpg') }}">
                @endif
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail"></div>
            <div>
                <span class="btn btn-warning btn-file">
                    <span class="fileinput-new">@lang('admin.select')</span>
                    <span class="fileinput-exists">@lang('admin.change')</span>
                    <input type="file" name="{{$name}}">
                </span>
                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">@lang('admin.delete')</a>
            </div>
        </div>
    </div>
</div>
