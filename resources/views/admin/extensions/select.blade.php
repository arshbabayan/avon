<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    {!! Form::label($name, $label, ['class' => 'control-label']) !!}
    <select name="{{ $name }}" class="form-control">
        <option value="">{{ $placeholder ?? trans('admin.non_selected') }}</option>
        @foreach($options as $option)
            <option
                @if($selected == $option->id) selected @endif
                value="{{ $option->id }}"
                >
                @if($translate === true)
                    {{ $option->translate($field) }}
                @elseif($translate === false)
                    {{ $option[$field] }}
                @else
                    {{ trans('admin.' . $option[$field]) }}
                @endif
            </option>
        @endforeach
    </select>
</div>
