<div class="custom_multiple_area">
    <div class="multiple_item">
        <button type="button" class="btn btn-danger delete_multiple_item_{{ $formMode }}">
            <i class="fa fa-trash"></i>
        </button>
        @include('admin.extensions.image',[
            'name' => $name,
            'field' => $field,
            'path' => $path,
            'item' => null
        ])
    </div>
</div>

<div class="multiple_items" data-name="{{ $name }}">
    @if($formMode === 'create')
        <div class="multiple_item" data-item="1">
            @include('admin.extensions.image',[
                'name' => $name.'[1]',
                'field' => $field,
                'path' => $path,
                'item' => null
            ])
        </div>
    @else
        @foreach($images as $image)
            <div class="multiple_item" data-item="{{ $image->id }}">
                @php
                    $params['image'] = $image->id;
                    $params[$item_route_param_name] = $item[$item_route_param_value];
                @endphp

                <button type="button" class="btn btn-danger delete_multiple_item_{{ $formMode }}"
                    data-url="{{ route($delete_url,$params) }}">
                    <i class="fa fa-trash"></i>
                </button>

                @include('admin.extensions.image',[
                    'name' => $name . '[' . $image->id . ']',
                    'field' => $field,
                    'path' => $path,
                    'item' => $image
                ])
            </div>
        @endforeach
    @endif

    <div class="add_multiple_item">
        <i class="fa fa-plus"></i>
    </div>
</div>
