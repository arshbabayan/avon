<div class="form-group {{ $errors->has($name) ? 'has-error' : ''}}">
    {!! Form::label($name, $label, ['class' => 'control-label']) !!}
    <select name="{{ $name }}[]" class="selectpicker" multiple data-live-search="true">
        @foreach($options as $option)
            <option
                @if(in_array($option->id,$selected_values)) selected @endif
                value="{{ $option['id'] }}"
            >
                @if($translate === true)
                    {{ $option->translate($field) }}
                    @if(isset($field_two))
                        ({{ $option->{$field_two} }})
                    @endif
                @elseif($translate === false)
                    {{ $option[$field] }}
                @else
                    {{ trans('admin.' . $option[$field]) }}
                @endif
            </option>
        @endforeach
    </select>
</div>
