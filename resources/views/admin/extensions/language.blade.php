<div class="card m-0" style="padding: 15px">
    @foreach ($fields as $field)
        @if($field['type'] == 'floara')
            <div class="card-header">
                <div class="d-flex d-justify-center d-align-center">
                    <button type="button" class="btn btn-info btn-sm refresh-floara">@lang('admin.refresh_floara')</button>
                </div>
            </div>
            @break
        @endif
    @endforeach

    <div class="card-content p-0">

        <ul class="nav nav-pills nav-pills-warning d-flex d-justify-center d-align-center">
            @for($i = 0; $i < count($languages); $i++)
                <li class="@if($languages[$i]->prefix == $default['prefix']) active @endif">
                    <a class="text-lowercase" href="#store_{{ $languages[$i]->prefix }}" data-toggle="tab" aria-expanded="{{ ($languages[$i]->prefix == $default['prefix']) ? 'true' : 'false' }}">
                        <img style="width: 30px!important;" src="{{ asset('dashboard/images/flags/' . $languages[$i]->prefix . '.png') }}" alt="">
                    </a>
                </li>
            @endfor
        </ul>

        <div class="tab-content">
            @for($i = 0;$i < count($languages); $i++)
                <div class="tab-pane @if($languages[$i]->prefix == $default['prefix']) active @endif" id="store_{{$languages[$i]->prefix}}">

                    @foreach ($fields as $field)
                        <div class="form-group @if($field['type'] === 'floara') floara @endif {{ $errors->has($field['name']) ? 'has-error' : ''}}">
                            {!! Form::label($field['name'], $field['label'], ['class' => 'control-label']) !!}

                            @if($field['type'] === 'text')
                                {!! Form::text($languages[$i]->prefix.'['.$field['name'].']', $item ? $item->translations($languages[$i]->id)->{$field['name']} ?? '' : '', ['class' => 'form-control']) !!}
                            @elseif($field['type'] === 'textarea')
                                {!! Form::textarea($languages[$i]->prefix.'['.$field['name'].']', $item ? $item->translations($languages[$i]->id)->{$field['name']} ?? '' : '', ['class' => 'form-control']) !!}
                            @else
                                {!! Form::textarea($languages[$i]->prefix.'['.$field['name'].']', $item ? $item->translations($languages[$i]->id)[$field['name']] ?? '' : '', ['class' => 'floara-textarea']) !!}
                            @endif
                        </div>
                    @endforeach

                </div>
            @endfor
        </div>

    </div>
</div>
