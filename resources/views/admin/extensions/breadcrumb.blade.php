<div class="card breadcrumb_card">
    <ul class="custom-breadcrumb">
        @if(isset($breadcrumbs))
            @foreach($breadcrumbs as $breadcrumb)
                <li @if (is_null($breadcrumb['route'])) class="active" @endif>
                    <a href="{{ $breadcrumb['route'] ? $breadcrumb['route'] : '#' }}">
                        @if($breadcrumb['icon'])
                            <i class="fa fa-{{ $breadcrumb['icon'] }}"></i>
                        @endif
                        <span class="text">{{ $breadcrumb['text'] }}</span>
                    </a>
                </li>
            @endforeach
        @endif
    </ul>
</div>
