@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.orders.breadcrumb')
@endsection

@section('content')
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">@lang('admin.role')</th>
                    <th class="text-center">@lang('admin.address')</th>
                    <th class="text-center">@lang('admin.products')</th>
                    <th class="text-center">@lang('admin.' . $type . '_at')</th>
                    <th class="text-center">@lang('admin.ordered_at')</th>
                    <th class="text-center">@lang('admin.buyer')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td class="text-center">{{ $order->id }}</td>
                        <td class="text-center">
                            @switch($type)
                                @case('finished')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.pay',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id]),
                                        'style' => 'display:inline',
                                        'class' => 'pay_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-info btn-sm w-100 pay">
                                            @lang('admin.pay_order')
                                        </button>
                                    {!! Form::close() !!}

                                    <br>

                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => 'all', 'action' => 'cancel']),
                                        'style' => 'display:inline',
                                        'class' => 'change_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-danger btn-sm w-100 change">
                                            @lang('admin.cancel_order')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('payed')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.finish',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id]),
                                        'style' => 'display:inline',
                                        'class' => 'finish_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-success btn-sm w-100 finish">
                                            @lang('admin.finish_order')
                                        </button>
                                    {!! Form::close() !!}

                                    <br>

                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => 'all', 'action' => 'cancel']),
                                        'style' => 'display:inline',
                                        'class' => 'change_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-danger btn-sm w-100 change">
                                            @lang('admin.cancel_order')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('in_processed')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => 'all', 'action' => 'cancel']),
                                        'style' => 'display:inline',
                                        'class' => 'change_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-danger btn-sm w-100 change">
                                            @lang('admin.cancel_order')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('canceled')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => 'all', 'action' => 'process']),
                                        'style' => 'display:inline',
                                        'class' => 'change_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-warning btn-sm w-100 change">
                                            @lang('admin.process_order')
                                        </button>
                                    {!! Form::close() !!}
                                @break
                            @endswitch
                        </td>
                        <td class="text-center">{{ $order->address->translate('title') }}</td>
                        <td class="text-center">
                            <a href="{{ route('locale.admin.orders.products',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id]) }}" title="View Products">
                                <button class="btn btn-success btn-sm">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    @lang('admin.show')
                                </button>
                            </a>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-primary btn-sm">
                                <i class="fa fa-clock" aria-hidden="true"></i>
                                {{ $order[$type . '_at'] }}
                            </button>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-primary btn-sm">
                                <i class="fa fa-clock" aria-hidden="true"></i>
                                {{ $order->created_at }}
                            </button>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-info btn-sm">
                                {{ $order['user']['name'] . ' ' . $order['user']['surname'] }}
                                <br>
                                {{ '(' . $order['user']['code'] . ')' }}
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $orders->appends(request()->all())->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.finish', function () {
                let form = $(this).parents('form.finish_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });

            $('body').on('click','.pay', function () {
                let form = $(this).parents('form.pay_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });

            $('body').on('click','.change', function () {
                let form = $(this).parents('form.change_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });
        })
    </script>
@endsection
