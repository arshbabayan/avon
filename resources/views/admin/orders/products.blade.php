@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.orders.breadcrumb')
@endsection

@section('content')
    @if(count($order->withDiscountProducts) > 0)
        <div class="card-body order-products">
            <h4 class="order-top-title">@lang('main.products_with_discount')</h4>
            <div class="container-fluid">
                <div class="row order-header">
                    <div class="col-md-2 br-none">@lang('admin.image')</div>
                    <div class="col-md-2 br-none">@lang('admin.articul')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_price')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_count')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_total')</div>
                    <div class="col-md-2">@lang('admin.actions')</div>
                </div>
                @foreach($order->withDiscountProducts as $item)
                    <div class="row order-item">
                        <div class="col-md-2 image-and-title">
                            <img src="{{ $item->product->topImage() }}" title="{{ $item->product->translate('title') }}" alt="">
                        </div>
                        <div class="col-md-2">{{ $item->product->articul }}</div>
                        <div class="col-md-2">{{ $item->price . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2">{{ $item->count }}</div>
                        <div class="col-md-2">{{ $item->price * $item->count . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2 br-2-blue">
                            {!! Form::open([
                                'method'=>'POST',
                                'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => $item->id, 'action' => 'cancel']),
                                'style' => 'display:inline',
                                'class' => 'change_form'
                            ]) !!}
                            <button type="button" class="btn btn-danger btn-sm w-100 change">
                                @lang('admin.cancel_order')
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
                <div class="row order-item">
                    <div class="col-md-4 height-70">
                        <span class="color-blue mr-5-custom">@lang('admin.products_with_discount_total')</span>
                        <span class="font-weight-bold">
                        {{ $order->products_with_discount_total }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-4 height-70">
                        <span class="color-blue mr-5-custom">@lang('admin.affordable_discount')</span>
                        <span class="font-weight-bold">
                        {{ $order->affordable_discount }} %
                    </span>
                    </div>
                    <div class="col-md-4 height-70 br-2-blue">
                        <span class="color-blue mr-5-custom">@lang('admin.products_with_discount_finally_total')</span>
                        <span class="font-weight-bold">
                        {{ $order->products_with_discount_finally_total }} @lang('admin.dram')
                    </span>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(count($order->notDiscountProducts) > 0)
        <div class="card-body order-products">
            <h4 class="order-top-title">@lang('main.products_not_discount')</h4>
            <div class="container-fluid">
                <div class="row order-header">
                    <div class="col-md-2 br-none">@lang('admin.image')</div>
                    <div class="col-md-2 br-none">@lang('admin.articul')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_price')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_count')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_total')</div>
                    <div class="col-md-2">@lang('admin.actions')</div>
                </div>
                @foreach($order->notDiscountProducts as $item)
                    <div class="row order-item">
                        <div class="col-md-2 image-and-title">
                            <img src="{{ $item->product->topImage() }}" title="{{ $item->product->translate('title') }}" alt="">
                        </div>
                        <div class="col-md-2">{{ $item->product->articul }}</div>
                        <div class="col-md-2">{{ $item->price . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2">{{ $item->count }}</div>
                        <div class="col-md-2">{{ $item->price * $item->count . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2 br-2-blue">
                            {!! Form::open([
                                'method'=>'POST',
                                'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => $item->id, 'action' => 'cancel']),
                                'style' => 'display:inline',
                                'class' => 'change_form'
                            ]) !!}
                            <button type="button" class="btn btn-danger btn-sm w-100 change">
                                @lang('admin.cancel_order')
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
                <div class="row order-item">
                    <div class="col-md-12 height-70 br-2-blue">
                        <span class="color-blue mr-5-custom">@lang('admin.products_not_discount_finally_total')</span>
                        <span class="font-weight-bold">
                            {{ $order->products_not_discount_finally_total }} @lang('admin.dram')
                        </span>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(count($order->returnedProducts) > 0)
        <div class="card-body">
            <h4 class="order-top-title">@lang('admin.canceled_orders')</h4>
            <div class="container-fluid">
                <div class="row order-header">
                    <div class="col-md-2 br-none">@lang('admin.image')</div>
                    <div class="col-md-2 br-none">@lang('admin.articul')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_price')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_count')</div>
                    <div class="col-md-2 br-none">@lang('admin.product_total')</div>
                    <div class="col-md-2">@lang('admin.actions')</div>
                </div>
                @foreach($order->returnedProducts as $item)
                    <div class="row order-item">
                        <div class="col-md-2 image-and-title">
                            <img src="{{ $item->product->topImage() }}" title="{{ $item->product->translate('title') }}" alt="">
                        </div>
                        <div class="col-md-2">{{ $item->product->articul }}</div>
                        <div class="col-md-2">{{ $item->price . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2">{{ $item->count }}</div>
                        <div class="col-md-2">{{ $item->price * $item->count . ' ' . trans('admin.dram') }}</div>
                        <div class="col-md-2 br-2-blue">
                            {!! Form::open([
                                'method'=>'POST',
                                'url' => route('locale.admin.orders.change',['locale' => $locale['prefix'], 'type' => $type, 'order_id' => $order->id, 'which' => $item->id, 'action' => 'process']),
                                'style' => 'display:inline; flex-direction:column',
                                'class' => 'change_form d-flex text-center justify-content-between align-items-center'
                            ]) !!}
                            <p class="m-0" style="font-size: 11px">@lang('admin.canceled_at')</p>
                            <p class="m-0" style="font-size: 11px">{{ $item->returned_at }}</p>
                            <button type="button" class="btn btn-warning btn-sm w-100 change m-0">
                                @lang('admin.process_order')
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @if($order->status !== 3)
        <div class="card-body">
            <h4 class="order-top-title">@lang('main.final_calculation')</h4>
            <div class="container-fluid">
                <div class="row final-calculation">
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.products_finally_total')</span>
                        <span class="font-weight-bold">
                        + {{ $order->products_finally_total }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.bonus')</span>
                        <span class="font-weight-bold">
                        - {{ $order->bonus }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.transportation_costs')</span>
                        <span class="font-weight-bold">
                        + {{ $order->transportation_costs }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.service_fee')</span>
                        <span class="font-weight-bold">
                        + {{ $order->service_fee }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.packaging')</span>
                        <span class="font-weight-bold">
                        + {{ $order->packaging }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.journal')</span>
                        <span class="font-weight-bold">
                        + {{ $order->journal }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.finally_total')</span>
                        <span class="font-weight-bold">
                        {{ $order->finally_total }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12 blue-hr"></div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.payed')</span>
                        <span class="font-weight-bold">
                        {{ $order->payed }} @lang('admin.dram')
                    </span>
                    </div>
                    <div class="col-md-12">
                        <span class="color-blue">@lang('admin.will_be_pay')</span>
                        <span class="font-weight-bold">
                        {{ $order->will_be_pay }} @lang('admin.dram')
                    </span>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.change', function () {
                let form = $(this).parents('form.change_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });
        })
    </script>
@endsection
