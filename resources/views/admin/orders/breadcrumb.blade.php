<div class="card order-types d-flex d-justify-between d-align-center" style="padding: 20px">
    <div class="@if($type === 'finished') active_type @endif">
        <a href="{{ route('locale.admin.orders.index',['locale' => $locale['prefix'], 'type' => 'finished']) }}">
            <button class="btn btn-outline-success" type="submit">
                <i class="fa fa-check"></i>
                @lang('admin.finished_orders') ({{ $types['finished'] }})
            </button>
        </a>
    </div>
    <div class="@if($type === 'payed') active_type @endif">
        <a href="{{ route('locale.admin.orders.index',['locale' => $locale['prefix'], 'type' => 'payed']) }}">
            <button class="btn btn-outline-info" type="submit">
                <i class="fa fa-money"></i>
                @lang('admin.payed_orders') ({{ $types['payed'] }})
            </button>
        </a>
    </div>
    <div class="@if($type === 'in_processed') active_type @endif">
        <a href="{{ route('locale.admin.orders.index',['locale' => $locale['prefix'], 'type' => 'in_processed']) }}">
            <button class="btn btn-outline-warning" type="submit">
                <i class="fa fa-refresh"></i>
                @lang('admin.in_processed_orders') ({{ $types['in_processed'] }})
            </button>
        </a>
    </div>
    <div class="@if($type === 'canceled') active_type @endif">
        <a href="{{ route('locale.admin.orders.index',['locale' => $locale['prefix'], 'type' => 'canceled']) }}">
            <button class="btn btn-outline-danger" type="submit">
                <i class="fa fa-ban"></i>
                @lang('admin.canceled_orders') ({{ $types['canceled'] }})
            </button>
        </a>
    </div>
</div>
