<div class="form-group {{ $errors->has('percent') ? 'has-error' : ''}}">
    {!! Form::label('percent', trans('admin.percent'), ['class' => 'control-label']) !!}
    {!! Form::text('percent', old('percent') ?? $address->percent ?? null, ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('embed') ? 'has-error' : ''}}">
    {!! Form::label('embed', 'Embed', ['class' => 'control-label']) !!}
    {!! Form::text('embed', old('embed') ?? $address->embed ?? null, ['class' => 'form-control']) !!}
</div>

@include('admin.extensions.language',[
    'fields' => [
        [
            'label' => trans('admin.title'),
            'name' => 'title',
            'type' => 'text',
        ],
    ],
    'item' => $address ?? null,
])

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
