@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.edit'),
                'icon' => 'pencil'
            ],
            [
                'route' => route('locale.admin.address.show',['locale' => $locale['prefix'], 'address' => $address->id]),
                'text' => $address->translate('title'),
                'icon' => null
            ],
            [
                'route' => route('locale.admin.address.index',$locale['prefix']),
                'text' => trans('admin.address'),
                'icon' => 'map-pin'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

       {!! Form::model($address, [
           'method' => 'PATCH',
           'url' => route('locale.admin.address.update',['locale' => $locale['prefix'], 'address' => $address->id]),
           'class' => 'form-horizontal',
           'files' => true
       ]) !!}

       @include ('admin.address.form', ['formMode' => 'edit'])

       {!! Form::close() !!}
    </div>
@endsection
