@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $address->translate('title'),
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.address.index',$locale['prefix']),
                'text' => trans('admin.address'),
                'icon' => 'map-pin'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.address.edit',['locale' => $locale['prefix'], 'address' => $address->id]) }}" title="Edit Address">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $address->id }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.title') </th>
                        <td>{{ $address->translate('title') }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.percent') </th>
                        <td>{{ $address->percent }}</td>
                    </tr>
                    <tr>
                        <th> Embed </th>
                        <td>{{ $address->embed }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td>{{ $address->created_at }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td>{{ $address->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
