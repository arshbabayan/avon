@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $catalog->name,
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.catalog.index',$locale['prefix']),
                'text' => trans('admin.catalogs'),
                'icon' => 'book'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.catalog.edit',['locale' => $locale['prefix'], 'catalog' => $catalog->id]) }}" title="Edit Catalog">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $catalog->id }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.thumbnail') </th>
                        <td>
                            <img src="{{ asset('main/catalogs/' . $catalog->thumbnail) }}" alt="">
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.catalog_name') </th>
                        <td> {{ $catalog->name }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.catalog_deadline') </th>
                        <td> {{ $catalog->deadline }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.pages') </th>
                        <td> {{ $catalog->pages_count }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td>{{ $catalog->created_at }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td>{{ $catalog->updated_at }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
