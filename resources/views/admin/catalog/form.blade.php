<div class="d-flex d-justify-center d-align-center">
    @include('admin.extensions.image',[
        'label' => trans('admin.thumbnail'),
        'name' => 'thumbnail',
        'path' => 'main/catalogs',
        'item' => $catalog ?? null
    ])
</div>

<div class="form-group {{ $errors->has('catalog_name') ? 'has-error' : ''}}">
    {!! Form::label('catalog_name', trans('admin.catalog_name'), ['class' => 'control-label']) !!}
    {!! Form::text('catalog_name', old('catalog_name') ?? $catalog->name ?? null, ['class' => 'form-control']) !!}
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
