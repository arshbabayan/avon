@extends('layouts.admin')

@section('content')
    <div class="card-header d-flex d-justify-between d-align-center">
        <div>
            <a href="{{ route('locale.admin.catalog.create',$locale['prefix']) }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('admin.add')
            </a>
        </div>
        <div>
            {!! Form::open(['method' => 'GET', 'url' => route('locale.admin.catalog.index', $locale['prefix']), 'class' => 'form-inline', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="@lang('admin.name')..." value="{{ request('search') }}">
                <span class="input-group-append">
                    <button class="btn btn-warning" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                     @if(request()->has('search'))
                        <a class="btn btn-warning" href="{{ route('locale.admin.catalog.index',$locale['prefix']) }}">
                            <i class="fa fa-remove"></i>
                        </a>
                    @endif
                </span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('admin.thumbnail')</th>
                        <th>@lang('admin.name')</th>
                        <th style="text-align: right">@lang('admin.actions')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($catalogs as $item)
                    <tr @if($item->active) class="active_catalog_style" @endif>
                        <td>{{ $loop->iteration }}</td>
                        <td class="image_part">
                            <img src="{{ asset('main/catalogs/' . $item->thumbnail) }}" alt="">
                        </td>
                        <td>{{ $item->name }}</td>
                        <td style="text-align: right">
                            <a href="{{ route('locale.admin.catalog.page.index', ['locale' => $locale['prefix'], 'catalog' => $item->id]) }}">
                                <button class="btn btn-success btn-sm" style="color: white">
                                    <i class="fa fa-leanpub" aria-hidden="true"></i>
                                    @lang('admin.pages') ({{ $item->pages_count }})
                                </button>
                            </a>

                            <a href="{{ route('locale.admin.catalog.show',['locale' => $locale['prefix'], 'catalog' => $item->id]) }}" title="View Catalog"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> @lang('admin.show')</button></a>
                            <a href="{{ route('locale.admin.catalog.edit',['locale' => $locale['prefix'], 'catalog' => $item->id]) }}" title="Edit Catalog"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')</button></a>

                            @if(!$item->active && $loop->iteration === 1)
                                <form action="{{ route('locale.admin.catalog.activate', ['locale' => $locale['prefix'], 'catalog' => $item->id]) }}" method="post">
                                    @csrf
                                    <button type="button" data-name="{{ $item->name }}" class="btn btn-warning btn-sm set_catalog_active" style="color: white">
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                        @lang('admin.activate')
                                    </button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $catalogs->appends(request()->all())->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.set_catalog_active', function () {
                let form = $(this).parents('form');
                let text = trans('activate_catalog') + '(' + $(this).data('name') + ')';
                swal({
                    title: trans('are_you_sure'),
                    text: text,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });
        })
    </script>
@endsection
