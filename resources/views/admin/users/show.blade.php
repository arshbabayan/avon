@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => $user->name . ' ' . $user->surname,
                'icon' => 'eye'
            ],
            [
                'route' => route('locale.admin.users.index',$locale['prefix']),
                'text' => trans('admin.users'),
                'icon' => 'users'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-header">
        <a href="{{ route('locale.admin.users.edit',['locale' => $locale['prefix'], 'user' => $user->id]) }}" title="Edit User">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
            </button>
        </a>

        {!! Form::open([
            'method'=>'DELETE',
            'url' => route('locale.admin.users.destroy',['locale' => $locale['prefix'], 'user' => $user->id]),
            'style' => 'display:inline',
            'class' => 'delete_form'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
            'type' => 'button',
            'class' => 'btn btn-danger btn-sm delete',
        )) !!}
        {!! Form::close() !!}
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th> @lang('admin.name') </th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.surname') </th>
                        <td>{{ $user->surname }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.address') </th>
                        <td>{{ $user->address }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.passport') </th>
                        <td>{{ $user->passport }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.phone') </th>
                        <td>{{ $user->phone }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.balance') </th>
                        <td>{{ $user->balance }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.blocked') </th>
                        <td>{{ $user->blocked ? trans('admin.yes') : trans('admin.no') }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.verified') </th>
                        <td>{{ $user->verified ? trans('admin.yes') : trans('admin.no') }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.easy_start') </th>
                        <td>
                            @switch($user->easy_start)
                                @case('start')
                                    @lang('admin.start')
                                @break
                                @case('active')
                                    @lang('admin.active')
                                @break
                                @case('inactive')
                                    @lang('admin.inactive')
                                @break
                            @endswitch
                        </td>
                    </tr>
                    @if ($user->easy_start !== 'start')
                        <tr>
                            <th> @lang('admin.easy_start_step') </th>
                            <td> {{ $user->easy_start_step }} </td>
                        </tr>
                    @endif
                    <tr>
                        <th> @lang('admin.bonus') </th>
                        <td>{{ $user->bonus }}</td>
                    </tr>
                    <tr>
                        <th>MAX @lang('admin.bonus') % </th>
                        <td>{{ $user->max_percent }}%</td>
                    </tr>
                    <tr>
                        <th>@lang('admin.inactive') </th>
                        <td>{{ $user->inactive_catalogs_count . ' ' . trans('admin.catalog') }} </td>
                    </tr>
                    <tr>
                        <th>@lang('admin.register_catalog') </th>
                        <td>{{ $user->registerCatalog()->name ?? trans('admin.exist') }} </td>
                    </tr>
                    <tr>
                        <th> @lang('admin.email') </th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.code') </th>
                        <td>{{ $user->code }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.coordinator') </th>
                        @if(is_null($user->coordinator))
                            <td>{{ trans('admin.exist') }}</td>
                        @else
                            <td>
                                <span>
                                    <b>@lang('admin.name')</b>: {{ $user->coordinator->name }}
                                </span>
                                <br>
                                <span>
                                    <b>@lang('admin.surname')</b>: {{ $user->coordinator->surname }}
                                </span>
                                <br>
                                <span>
                                    <b>@lang('admin.code')</b>: {{ $user->coordinator->code }}
                                </span>
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <th> @lang('admin.role') </th>
                        <td>{{ trans('admin.user_role_' . $user->role_id) }}</td>
                    </tr>
                    @if($user->email_verified_at)
                        <tr>
                            <th> @lang('admin.email_verified_at') </th>
                            <td>{{ $user->email_verified_at }}</td>
                        </tr>
                    @endif
                    @if($user->verified_at)
                        <tr>
                            <th> @lang('admin.verified_at') </th>
                            <td>{{ $user->verified_at }}</td>
                        </tr>
                    @endif
                    @if($user->blocked_at)
                        <tr>
                            <th> @lang('admin.blocked_at') </th>
                            <td>{{ $user->blocked_at }}</td>
                        </tr>
                    @endif
                    <tr>
                        <th> @lang('admin.created_at') </th>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                    <tr>
                        <th> @lang('admin.updated_at') </th>
                        <td>{{ $user->updated_at }}</td>
                    </tr>
                    @if($user->deleted_at)
                        <tr>
                            <th> @lang('admin.deleted_at') </th>
                            <td>{{ $user->deleted_at }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection
