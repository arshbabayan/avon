@extends('layouts.admin')

@section('breadcrumb')
    @include('admin.extensions.breadcrumb', [
        'breadcrumbs' => [
            [
                'route' => null,
                'text' => trans('admin.edit'),
                'icon' => 'pencil'
            ],
            [
                'route' => route('locale.admin.users.show',['locale' => $locale['prefix'], 'user' => $user->id]),
                'text' => $user->name . ' ' . $user->surname,
                'icon' => null
            ],
            [
                'route' => route('locale.admin.users.index',$locale['prefix']),
                'text' => trans('admin.users'),
                'icon' => 'users'
            ],
        ],
    ])
@endsection

@section('content')
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => route('locale.admin.users.update',['locale' => $locale['prefix'], 'user' => $user->id]),
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('admin.users.form', ['formMode' => 'edit'])

        {!! Form::close() !!}
    </div>
@endsection
