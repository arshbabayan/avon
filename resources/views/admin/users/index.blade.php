@extends('layouts.admin')

@section('breadcrumb')
    <div class="card users-types d-flex d-justify-between d-align-center" style="padding: 20px">
        <div class="@if(request()->get('type') === 'verified') active_type @endif">
            <a href="{{ route('locale.admin.users.index',['locale' => $locale['prefix'], 'type' => 'verified']) }}">
                <button class="btn btn-outline-success" type="submit">
                    <i class="fa fa-check"></i>
                    @lang('admin.verified_users') ({{ $types['verified'] }})
                </button>
            </a>
        </div>
        <div class="@if(request()->get('type') === 'unverified') active_type @endif">
            <a href="{{ route('locale.admin.users.index',['locale' => $locale['prefix'], 'type' => 'unverified']) }}">
                <button class="btn btn-outline-warning" type="submit">
                    <i class="fa fa-close"></i>
                    @lang('admin.unverified_users') ({{ $types['unverified'] }})
                </button>
            </a>
        </div>
        <div class="@if(request()->get('type') === 'blocked') active_type @endif">
            <a href="{{ route('locale.admin.users.index',['locale' => $locale['prefix'], 'type' => 'blocked']) }}">
                <button class="btn btn-outline-info" type="submit">
                    <i class="fa fa-ban"></i>
                    @lang('admin.blocked_users') ({{ $types['blocked'] }})
                </button>
            </a>
        </div>
        <div class="@if(request()->get('type') === 'deleted') active_type @endif">
            <a href="{{ route('locale.admin.users.index',['locale' => $locale['prefix'], 'type' => 'deleted']) }}">
                <button class="btn btn-outline-danger" type="submit">
                    <i class="fa fa-trash"></i>
                    @lang('admin.deleted_users') ({{ $types['deleted'] }})
                </button>
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-header d-flex d-justify-center d-align-center">
        <div>
            <a href="{{ route('locale.admin.users.create',$locale['prefix']) }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> @lang('admin.add_user')
            </a>
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>@lang('admin.actions')</th>
                    <th>@lang('admin.code')</th>
                    <th>@lang('admin.full_name')</th>
                    <th>@lang('admin.role')</th>
                    <th>@lang('admin.coordinator')</th>
                    <th style="text-align: right">@lang('admin.actions')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $item)
                    <tr>
                        <td>
                            @switch(request()->get('type'))
                                @case('verified')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.users.verify',['locale' => $locale['prefix'], 'user' => $item->id]),
                                        'style' => 'display:inline',
                                        'class' => 'verify_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-info btn-sm w-100 verify">
                                            @lang('admin.unverify')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('unverified')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.users.verify',['locale' => $locale['prefix'], 'user' => $item->id]),
                                        'style' => 'display:inline',
                                        'class' => 'verify_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-success btn-sm w-100 verify">
                                            @lang('admin.verify')
                                        </button>
                                    {!! Form::close() !!}
                                    <br>
                                    {!! Form::open([
                                         'method'=>'POST',
                                         'url' => route('locale.admin.users.block',['locale' => $locale['prefix'], 'user' => $item->id]),
                                         'style' => 'display:inline',
                                         'class' => 'block_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-danger btn-sm w-100 block">
                                            @lang('admin.block')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('blocked')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.users.block',['locale' => $locale['prefix'], 'user' => $item->id]),
                                        'style' => 'display:inline',
                                        'class' => 'block_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-success btn-sm w-100 block">
                                            @lang('admin.unblock')
                                        </button>
                                    {!! Form::close() !!}
                                @break

                                @case('deleted')
                                    {!! Form::open([
                                        'method'=>'POST',
                                        'url' => route('locale.admin.users.restore',['locale' => $locale['prefix'], 'user' => $item->id]),
                                        'style' => 'display:inline',
                                        'class' => 'restore_form'
                                    ]) !!}
                                        <button type="button" class="btn btn-success btn-sm w-100 restore">
                                            @lang('admin.restore')
                                        </button>
                                    {!! Form::close() !!}
                                @break
                            @endswitch
                        </td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->name . ' ' . $item->surname }}</td>
                        <td>{{ trans('admin.user_role_' . $item->role_id) }}</td>
                        <td>
                            @if($item->coordinator)
                                <a href="{{ route('locale.admin.users.show',['locale' => $locale['prefix'], 'user' => $item->id]) }}" title="View Coordinator">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                    {{ $item->coordinator->name . ' ' . $item->coordinator->surname }}
                                </a>
                            @else
                                Avon Cosmetics
                            @endif
                        </td>
                        <td style="text-align: right">
                            @if(request()->get('type') !== 'deleted')
                                <a href="{{ route('locale.admin.users.show',['locale' => $locale['prefix'], 'user' => $item->id]) }}" >
                                    <button class="btn btn-info btn-sm">
                                        <i class="fa fa-eye" aria-hidden="true"></i> @lang('admin.show')
                                    </button>
                                </a>
                                <br>
                                <a href="{{ route('locale.admin.users.edit',['locale' => $locale['prefix'], 'user' => $item->id]) }}" title="Edit User">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('admin.edit')
                                    </button>
                                </a>
                                <br>
                                {!! Form::open([
                                   'method'=>'DELETE',
                                   'url' => route('locale.admin.users.destroy',['locale' => $locale['prefix'], 'user' => $item->id]),
                                   'style' => 'display:inline',
                                   'class' => 'delete_form'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ' . trans('admin.delete'), array(
                                    'type' => 'button',
                                    'class' => 'btn btn-danger btn-sm delete',
                                )) !!}
                                {!! Form::close() !!}
                            @else
                                <span class="btn btn-info">
                                    @lang('admin.actions_after_restore')
                                </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $users->appends(request()->all())->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: trans('are_you_sure'),
                    text: trans('return_is_failed'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes_delete'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });

            $('body').on('click','.verify', function () {
                let form = $(this).parents('form.verify_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });

            $('body').on('click','.block', function () {
                let form = $(this).parents('form.block_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });

            $('body').on('click','.restore', function () {
                let form = $(this).parents('form.restore_form');
                swal({
                    title: trans('are_you_sure'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: trans('yes'),
                    cancelButtonText: trans('cancel'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: trans('canceled'),
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            });
        })
    </script>
@endsection
