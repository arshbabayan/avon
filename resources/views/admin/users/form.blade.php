<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('admin.name'), ['class' => 'control-label']) !!}
    {!! Form::text('name', old('name') ?? $user->name ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('surname') ? 'has-error' : ''}}">
    {!! Form::label('surname', trans('admin.surname'), ['class' => 'control-label']) !!}
    {!! Form::text('surname', old('surname') ?? $user->surname ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', trans('admin.address'), ['class' => 'control-label']) !!}
    {!! Form::text('address', old('address') ?? $user->address ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
    {!! Form::label('passport', trans('admin.passport'), ['class' => 'control-label']) !!}
    {!! Form::text('passport', old('passport') ?? $user->passport ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', trans('admin.phone'), ['class' => 'control-label']) !!}
    {!! Form::text('phone', old('phone') ?? $user->phone ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('balance') ? 'has-error' : ''}}">
    {!! Form::label('balance', trans('admin.balance'), ['class' => 'control-label']) !!}
    {!! Form::number('balance', old('balance') ?? $user->balance ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('bonus') ? 'has-error' : ''}}">
    {!! Form::label('bonus', trans('admin.bonus'), ['class' => 'control-label']) !!}
    {!! Form::number('bonus', old('bonus') ?? $user->bonus ?? '', ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', trans('admin.email'), ['class' => 'control-label']) !!}
    {!! Form::text('email', old('email') ?? $user->email ?? '', ['class' => 'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('coordinator') ? 'has-error' : ''}}">
    {!! Form::label('coordinator', trans('admin.coordinator'), ['class' => 'control-label']) !!}
    <br>
    <select name="coordinator" class="selectpicker" data-live-search="true">
        <option value="">@lang('admin.non_selected')</option>
        @foreach($coordinators as $coordinator)
            <option @if(isset($user) && $user->coordinator_id == $coordinator->id) selected @endif value="{{ $coordinator->id }}">
                {{ $coordinator->name . ' ' . $coordinator->surname }} ({{ $coordinator->code }})
            </option>
        @endforeach
    </select>
</div>

<div class="form-group {{ $errors->has('role') ? 'has-error' : ''}}">
    {!! Form::label('role', trans('admin.role'), ['class' => 'control-label']) !!}
    <br>
    <select name="role" class="selectpicker" data-live-search="true">
        <option value="">@lang('admin.non_selected')</option>
        @foreach($roles as $role)
            <option @if(isset($user) && $user->role_id == $role->id) selected @endif value="{{ $role->id }}">
                {{ trans('admin.user_role_' . $role->id) }}
            </option>
        @endforeach
    </select>
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? trans('admin.update') : trans('admin.add'), ['class' => 'btn btn-success']) !!}
</div>
