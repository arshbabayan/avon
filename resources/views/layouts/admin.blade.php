<!doctype html>
<html lang="{{ $locale['prefix'] }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Avon</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/" />
    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/turbo.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/fileinput.css') }}">
    <link href="{{ asset('dashboard/assets/css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/editor/codemirror.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/froala_editor.pkgd.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/froala_style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/codemirror5.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/multiselect/select.css') }}" rel="stylesheet"/>
    <link href="{{ asset('dashboard/breadcrumb/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/main.css') }}" rel="stylesheet">
    <script> window.translations = {!! json_encode(trans('admin')) !!} </script>
    <script src="{{ asset('dashboard/js/transition.js') }}"></script>
    @yield('style')
</head>

<body>

<div class="wrapper">
    <div class="sidebar">
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="@if($url_segments[3] == 'orders') active_page @endif">
                    <a href="{{ route('locale.admin.orders.index',['locale' => $locale['prefix'], 'type' => 'finished']) }}">
                        <i class="fa fa-bell-o"></i>
                        <p>@lang('admin.orders')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'report-orders') active_page @endif">
                    <a href="{{ route('locale.admin.report.orders',['locale' => $locale['prefix']]) }}">
                        <i class="fa fa-file-excel-o"></i>
                        <p>@lang('admin.report_orders')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'report-products') active_page @endif">
                    <a href="{{ route('locale.admin.report.products',['locale' => $locale['prefix']]) }}">
                        <i class="fa fa-file-excel-o"></i>
                        <p>@lang('admin.report_products')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'users') active_page @endif">
                    <a href="{{ route('locale.admin.users.index', ['locale' => $locale['prefix'], 'type' => 'verified']) }}">
                        <i class="fa fa-users"></i>
                        <p>@lang('admin.users')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'import') active_page @endif">
                    <a href="{{ route('locale.admin.import.index',$locale['prefix']) }}">
                        <i class="fa fa-upload"></i>
                        <p>@lang('admin.import')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'catalog') active_page @endif">
                    <a href="{{ route('locale.admin.catalog.index',$locale['prefix']) }}">
                        <i class="fa fa-book"></i>
                        <p>@lang('admin.catalogs')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'product') active_page @endif">
                    <a href="{{ route('locale.admin.product.index',$locale['prefix']) }}">
                        <i class="fa fa-shopping-cart"></i>
                        <p>@lang('admin.products')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'address') active_page @endif">
                    <a href="{{ route('locale.admin.address.index',$locale['prefix']) }}">
                        <i class="fa fa-map-pin"></i>
                        <p>@lang('admin.address')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'slider') active_page @endif">
                    <a href="{{ route('locale.admin.slider.index',$locale['prefix']) }}">
                        <i class="fa fa-picture-o"></i>
                        <p>@lang('admin.sliders')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'information') active_page @endif">
                    <a href="{{ route('locale.admin.information.index',$locale['prefix']) }}">
                        <i class="fa fa-list-alt"></i>
                        <p>@lang('admin.information')</p>
                    </a>
                </li>
                <li class="@if($url_segments[3] == 'personal-details') active_page @endif">
                    <a href="{{ route('locale.admin.personal-details',$locale['prefix']) }}">
                        <i class="fa fa-key"></i>
                        <p>@lang('admin.personal_details')</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-absolute" data-topbar-color="blue">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                        <i class="material-icons visible-on-sidebar-regular f-26">keyboard_arrow_left</i>
                        <i class="material-icons visible-on-sidebar-mini f-26">keyboard_arrow_right</i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse languages">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            @foreach($languages as $language)
                                @if($locale['prefix'] == $language->prefix)
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img width="30px" src="{{ asset('dashboard/images/flags/'.$language->icon) }}" alt="">
                                    </a>
                                    @break
                                @endif
                            @endforeach
                            <ul class="dropdown-menu">
                                @foreach($languages as $language)
                                    @if($locale['prefix'] !== $language->prefix)
                                        <li>
                                            <a href="{{ str_replace('/'.$locale['prefix'].'/','/'.$language->prefix.'/',url()->current()) }}" >
                                                <img width="30px" src="{{ asset('dashboard/images/flags/'.$language->icon) }}" alt="">
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a style="cursor:pointer" data-href="{{ route('locale.admin.logout',$locale['prefix']) }}" class="logout">
                                <i class="fa fa-sign-out"></i>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @yield('breadcrumb')
                        <div class="card">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="{{ asset('dashboard/assets/vendors/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/material.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/moment.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/chartist.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.bootstrap-wizard.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-notify.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/nouislider.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.select-bootstrap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.datatables.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/sweetalert2.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/fullcalendar.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/turbo.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/fileinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/demo.js') }}"></script>
<script src="{{ asset('dashboard/editor/codemirror.min.js') }}"></script>
<script src="{{ asset('dashboard/editor/codemirror5.js') }}"></script>
<script src="{{ asset('dashboard/editor/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('dashboard/js/main.js') }}"></script>
<script>
    $(function () {
        @if(session()->has('sweet_alert'))
            swal({
                type: '{{ session()->get('sweet_type', 'success') }}',
                title: trans('{{ session()->get('sweet_alert') }}'),
                timer: '{{ is_null(session()->get('sweet_type', null)) ? 2000 : false }}',
                showConfirmButton:false,
            }).catch(swal.loop);
        @endif
    });
</script>
@yield('script')
</html>
