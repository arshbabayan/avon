<!doctype html>
<html lang="{{ $locale['prefix'] }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="shortcut icon" href="{{ asset('main/assets/img/icons/favicon.ico') }}" type="image/x-icon">

    <!-- font-awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Bootstrap v4.4.1 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('main/assets/bootstrap/bootstrap.css') }}">

    @yield('slick')

    <link href="{{ asset('main/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('main/assets/css/font-size.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('main/custom/css/main.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('main/custom/css/main.responsive') }}" rel="stylesheet" type="text/css">

    <script> window.translations2 = {!! json_encode(trans('main')) !!} </script>
    <script src="{{ asset('main/custom/js/Others/transition.js') }}"></script>
    @yield('style')
</head>

<body>

<div class="preloader">
    <img src="{{ asset('main/pictures/preloader.gif') }}" alt="">
</div>

<div class="container">
    <header class="header-section mt-3 mb-2">
        <div class="row justify-content-center justify-content-md-between align-items-center py-3">
            <div class="col-md-auto text-center text-md-left my-2 my-lg-0">
                <a href="{{ route('locale.home', $locale['prefix']) }}">
                    <img src="{{ asset('main/pictures/logo.png') }}" class="navbar-logo img-fluid" alt="avon" width="150">
                </a>
                {{ \App\Helpers\Actions::getInfo($information['home'],'phones_info_line') }}
            </div>

            @if(!is_null($active_catalog))
                <div class="col-md-auto d-flex justify-content-center d-lg-none mb-2">
                    <div class="media my-2">
                        <a href="{{ route('locale.catalog.book', ['locale' => $locale['prefix'], 'name' => $active_catalog['name']]) }}">
                            <img class="mr-3" src="{{ asset('main/catalogs/' . $active_catalog->thumbnail) }}" alt="{{ $active_catalog->name }}" title="{{ $active_catalog->name }}" width="85" height="85" style="object-fit: cover;">
                        </a>
                        <div class="media-body">
                            <div class="d-flex align-items-center">
                                <h3 class="font-weight-normal mb-0">{{ $active_catalog->name }}</h3>
                                <span class="vl-h-20 mx-1"></span>
                                <span class="font-size-11 line-height">
                                    @lang('main.catalog_deadline')
                                    {{ \App\Helpers\Actions::datesDiff(now(), $active_catalog->deadline) }}
                                </span>
                            </div>
                            <p class="font-weight-bold font-size-12 text-pink m-0">@lang('main.conversion_factor') {{ \App\Helpers\Actions::getInfo($information['home'],'conversion_factor') }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-auto d-none d-lg-block my-2 my-lg-0">
                    <a href="{{ route('locale.catalog.book', ['locale' => $locale['prefix'], 'name' => $active_catalog['name']]) }}">
                        <img class="mr-3" src="{{ asset('main/catalogs/' . $active_catalog->thumbnail) }}" alt="{{ $active_catalog->name }}" title="{{ $active_catalog->name }}" width="120" height="120" style="object-fit: cover;">
                    </a>
                </div>

                <div class="col-md-auto d-none d-lg-block my-2 my-lg-0">
                    <div class="d-flex align-items-center">
                        <h3 class="font-weight-normal mb-0">{{ $active_catalog->name }}</h3>
                        <span class="vl-h-20 mx-1"></span>
                        <span class="font-size-11 line-height">
                            @lang('main.catalog_deadline')
                            {{ \App\Helpers\Actions::datesDiff(now(), $active_catalog->deadline) }}
                        </span>
                    </div>
                </div>
            @endif

            <div class="col-md-auto d-none d-lg-block my-2 my-lg-0">
                <p class="font-weight-bold font-size-14 text-pink m-0">@lang('main.conversion_factor') {{ \App\Helpers\Actions::getInfo($information['home'],'conversion_factor') }}</p>
            </div>

            <div class="col-auto my-2 my-lg-0">
                @php $user = \Illuminate\Support\Facades\Auth::user(); @endphp
                @if($user && $user->role_id !== 9)
                    <a href="{{ route('locale.profile.index') }}" class="btn text-white bg-pink outline-none">
                        {{ $user->name . ' ' . $user->surname }}
                    </a>
                @else
                    <button type="button" class="btn text-white bg-pink outline-none font-size-13 font-size-md-16" data-toggle="modal" data-target="#entrance_for_representatives">
                        @lang('main.entrance_for_representatives')
                    </button>
                @endif
            </div>

            @include('main.extensions.basket-menu')

            <div class="col-auto pr-2 d-lg-none">
                <button class="navbar-toggler border-0 p-0" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="outline: none;">
                    <i class="fas fa-bars text-pink"></i>
                </button>
            </div>
        </div>

        @include('main.extensions.navbar')

        @if($route_name === 'locale.home')
            <div class="row header-faq justify-content-center justify-content-md-between">
                <div class="col-auto my-2">
                    <button type="button" class="btn text-white bg-pink outline-none" data-toggle="modal" data-target="#faq-1">
                        @lang('main.how-to-join-our-team')
                    </button>
                </div>
                <div class="col-auto my-2">
                    <button type="button" class="btn text-white bg-pink outline-none" data-toggle="modal" data-target="#faq-2">
                        @lang('main.how-to-become-a-coordinator')
                    </button>
                </div>
                <div class="col-auto my-2">
                    <button type="button" class="btn text-white bg-pink outline-none" data-toggle="modal" data-target="#faq-3">
                        @lang('main.how-to-become-a-regional-manager')
                    </button>
                </div>
            </div>
        @endif
    </header>

    @yield('content')

    <hr class="bg-pink mb-0">

    <footer class="footer row justify-content-center font-size-15 mt-2 text-center">
        <div class="col-md-auto">
            <a href="{{ route('locale.terms-of-use', $locale['prefix']) }}">@lang('main.terms-of-use')</a>
        </div>
        <div class="col-md-auto">
            <a href="{{ route('locale.confidentiality', $locale['prefix']) }}">@lang('main.confidentiality')</a>
        </div>
        <div class="col-md-auto">
            <a href="{{ route('locale.home', $locale['prefix']) }}">@lang('main.home')</a>
        </div>
        <div class="w-100"></div>
        <div class="col-md-6 mx-auto">
            {!! \App\Helpers\Actions::getInfoTranslate($information['home'], 'footer_content') !!}
        </div>
        <div class="w-100"></div>

        <div class="col-auto">
            <a href="{{ \App\Helpers\Actions::getInfo($information['socials'], 'instagram') }}" target="_blank">
                <i class="fab fa-instagram fa-3x m-2"></i>
            </a>
        </div>
        <div class="col-auto">
            <a href="{{ \App\Helpers\Actions::getInfo($information['socials'], 'facebook') }}" target="_blank">
                <i class="fab fa-facebook-square fa-3x m-2"></i>
            </a>
        </div>
        <div class="col-auto">
            <a href="{{ \App\Helpers\Actions::getInfo($information['socials'], 'vk') }}" target="_blank">
                <i class="fab fa-vk fa-3x m-2"></i>
            </a>
        </div>
        <div class="w-100 mb-2"></div>
    </footer>
</div>

<!-- Modal Вход Для Представителей  -->
@include('main.popups.entrance_for_representatives')
<!-- Modal for messages  -->
@include('main.popups.message')

<a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up d-none d-lg-inline"></i></a>

@yield('script_start')

<!-- jQuery -->
<script src="{{ asset('main/assets/js/jquery/jquery.min.js') }}"></script>

<!-- bootstrap JS -->
<script src="{{ asset('main/assets/js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('main/assets/js/bootstrap/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('main/assets/slick-carousel/slick.min.js') }}"></script>
<script src="{{ asset('main/assets/js/index.js') }}"></script>
<script src="{{ asset('main/custom/js/main.js') }}"></script>
<script src="{{ asset('main/custom/js/mails.js') }}"></script>

@yield('script_end')

</body>
</html>
