<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('{locale?}')->name('locale.')->group(function (){
    //Main routes
    Route::get('','Main\IndexController@index')->name('home');

    //Global setting routes
    Route::post('/set-user', 'Main\GlobalController@setUser')->name('set-user');
    Route::post('/set-address', 'Main\GlobalController@getAddress')->name('get-address');
    Route::post('/update-basket-page', 'Main\GlobalController@updateBasketPage')->name('update-basket-page');

    //Order checkout
    Route::post('checkout', 'Main\OrderController@checkout')->name('checkout');

    //Mails
    Route::get('contact-us','Main\ContactUsController@index')->name('contact-us');
    Route::post('contact-us-send','Main\ContactUsController@send')->name('contact-us-send');

    //Pages
    Route::get('business-model','Main\SinglePagesController@businessModel')->name('business-model');
    Route::get('about-company','Main\SinglePagesController@aboutCompany')->name('about-company');
    Route::get('payment-orders','Main\SinglePagesController@paymentOrders')->name('payment-orders');
    Route::get('delivery-points','Main\SinglePagesController@deliveryPoints')->name('delivery-points');
    Route::get('terms-of-use','Main\SinglePagesController@termsOfUse')->name('terms-of-use');
    Route::get('confidentiality','Main\SinglePagesController@confidentiality')->name('confidentiality');

    //Catalog
    Route::prefix('/catalog/{name}')->name('catalog.')->group(function (){
        Route::get('','Main\CatalogController@index')->name('book');
        Route::post('get-page/{page}', 'Main\CatalogController@getPage')->name('get-page');
    });

    //Products
    Route::get('statuses/{prefix}','Main\ProductController@statuses')->name('statuses');
    Route::get('product/{articul__id}','Main\ProductController@product')->name('product');

    //Basket
    Route::prefix('/basket')->name('basket.')->group(function (){
        Route::get('','Main\BasketController@index')->name('index');
        Route::post('update', 'Main\BasketController@update')->name('update');
    });

    //Required user
    Route::middleware('user_middle')->group(function (){
        Route::prefix('/profile')->name('profile.')->group(function (){

            Route::post('/converse-post', 'Main\PaymentController@converse_post')->name('converse_post');
            Route::any('/converse-success', ['as'=>'converse_back_url', 
                'uses'=>'Main\PaymentController@back_url_conerse']);

            Route::any('','Main\ProfileController@index')->name('index');
            Route::get('orders','Main\ProfileController@orders')->name('orders');
            Route::get('edit','Main\ProfileController@edit')->name('edit');
            Route::post('update','Main\ProfileController@update')->name('update');
            Route::get('reports','Main\ProfileController@reports')->name('reports');
            Route::post('reports/export','Main\ProfileController@exportReport')->name('export-report');
            Route::get('logout','Main\ProfileController@logout')->name('logout');
            Route::post('get-product/{articul}', 'Main\ProfileController@getProduct')->name('get-product');
            Route::post('store-to-basket', 'Main\ProfileController@storeToBasket')->name('store-to-basket');
        });
    });

    //Required guest or admin
    Route::middleware('guest_middle')->group(function (){
        Route::get('registration','Main\RegistrationController@index')->name('registration');
        Route::post('registration-send','Main\RegistrationController@send')->name('registration-send');
        Route::get('registration-verify','Main\RegistrationController@verify')->name('registration-verify');
        Route::post('sign-in', 'Main\ProfileController@signIn')->name('sign-in');
    });

    //Admin routes
    Route::prefix('/admin')->name('admin.')->group(function (){
        Route::get('login','Admin\AuthController@login')->name('login');
        Route::post('sign','Admin\AuthController@sign')->name('sign');

        Route::middleware('auth_middle')->group(function (){
            Route::get('personal-details', 'Admin\IndexController@personalDetails')->name('personal-details');
            Route::post('update-admin','Admin\IndexController@updateAdmin')->name('update-admin');
            Route::post('redactor-image-upload','Admin\IndexController@redactorImageUpload')->name('redactor-image-upload');
            Route::get('logout','Admin\AuthController@logout')->name('logout');

            //Catalog
            Route::resource('catalog', 'Admin\CatalogController');
            Route::post('/catalog/{catalog}/activate','Admin\CatalogController@activate')->name('catalog.activate');

            //Pages
            Route::prefix('/catalog/{catalog}')->name('catalog.')->group(function (){
                Route::resource('page', 'Admin\PageController');
            });

            //Products
            Route::resource('product', 'Admin\ProductController');
            Route::post('/product/{product}/{image}/delete-image','Admin\ProductController@deleteImage')->name('product.delete-image');

            //Slider
            Route::resource('slider', 'Admin\SliderController');

            //Information
            Route::get('information', 'Admin\InformationController@index')->name('information.index');
            Route::get('information/{prefix}', 'Admin\InformationController@show')->name('information.show');
            Route::put('information/{prefix}/update', 'Admin\InformationController@update')->name('information.update');

            //Users
            Route::resource('users', 'Admin\UsersController');
            Route::post('users/{user}/verify', 'Admin\UsersController@verify')->name('users.verify');
            Route::post('users/{user}/block', 'Admin\UsersController@block')->name('users.block');
            Route::post('users/{user}/restore', 'Admin\UsersController@restore')->name('users.restore');

            //Orders
            Route::prefix('orders/{type}')->name('orders.')->group(function (){
                Route::get('', 'Admin\OrderController@index')->name('index');

                Route::prefix('{order_id}')->group(function (){
                    Route::get('products', 'Admin\OrderController@products')->name('products');

                    Route::post('finish', 'Admin\OrderController@finish')->name('finish');
                    Route::post('pay', 'Admin\OrderController@pay')->name('pay');
                    Route::post('{which}/{action}', 'Admin\OrderController@change')->name('change');
                });
            });

            //Report orders
            Route::get('report-orders', 'Admin\ReportController@orders')->name('report.orders');
            Route::post('report-orders-download', 'Admin\ReportController@ordersDownload')->name('report.orders.download');

            //Report products
            Route::get('report-products', 'Admin\ReportController@products')->name('report.products');
            Route::post('report-products-download', 'Admin\ReportController@productsDownload')->name('report.products.download');

            //Address
            Route::resource('address', 'Admin\AddressController');

            //Import
            Route::get('import', 'Admin\ImportController@index')->name('import.index');
            Route::post('importing', 'Admin\ImportController@importing')->name('import.importing');
        });
    });
});

Auth::routes();
