<?php

namespace App\Traits;

use App\Helpers\Actions;
use App\Models\Address;
use App\Models\Information;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

trait Basket{

    public static function basket($address = null, $journal = false){
        $cache = session()->get('basket',[]);
        $products = Product::query()->whereIn('id', array_keys($cache))->with('images')->get();
        $products->each(function($row){
            $row->append('info');
        });

        //Remove existing ids in basket
        foreach ($cache as $product_id => $product_count){
            if (!in_array($product_id,$products->pluck('id')->toArray())){
                unset($cache[$product_id]);
            }
        }

        //Checking products with discount
        $products_with_discount = [];
        $products_with_discount_total = 0;
        $products_not_discount = [];
        $products_not_discount_total = 0;

        foreach ($products as $product){
            $count = (int)$cache[$product->id];
            if ($product->not_discount === 0){
                array_push($products_with_discount, $product);
                $products_with_discount_total += $count * $product->discount($count);
            } else{
                array_push($products_not_discount, $product);
                $products_not_discount_total += $count * $product->discount($count);
            }
        }

        //Discount
        $total_for_get_discount = $products_with_discount_total + $products_not_discount_total;
        $affordable_discount = 0;
        if ($total_for_get_discount > 49999){
            $affordable_discount = 30;
        } elseif ($total_for_get_discount > 29999){
            $affordable_discount = 25;
        } elseif ($total_for_get_discount > 14999){
            $affordable_discount = 20;
        } elseif ($total_for_get_discount > 4999){
            $affordable_discount = 15;
        }

        $products_with_discount_finally_total = ((100 - ($affordable_discount)) * $products_with_discount_total) / 100;
        $products_not_discount_finally_total = $products_not_discount_total;

        //Products finally total
        $products_finally_total = $products_with_discount_finally_total + $products_not_discount_finally_total;

        //Bonus
        $basket_bonus = Auth::user()->bonus ?? 0;
        if ($basket_bonus > $products_finally_total / 2){
            $basket_bonus = $products_finally_total / 2;
        }

        //Total
        $order_total = (int)($products_finally_total - $basket_bonus);

        return [
            'cache' => $cache,
            'products' => $products->keyBy('id'),

            'products_with_discount' => $products_with_discount,
            'products_with_discount_total' => (int)$products_with_discount_total,
            'affordable_discount' => (int)$affordable_discount,
            'products_with_discount_finally_total' => (int)$products_with_discount_finally_total,

            'products_not_discount' => $products_not_discount,
            'products_not_discount_total' => (int)$products_not_discount_total,
            'products_not_discount_finally_total' => (int)$products_not_discount_finally_total,

            'products_finally_total' => (int)$products_finally_total,
            'basket_bonus' => (int)$basket_bonus,
            'order_total' => (int)$order_total,

            'transportation_costs' => (int)self::transportationCosts($order_total, $address),
            'service_fee' => (int)self::serviceFee(),
            'packaging' => (int)self::packaging(),
            'journal' => (int)self::journal($journal),

            'finally_total' => (int)self::finallyTotal($order_total, $address, $journal)
        ];
    }

    public static function transportationCosts($order_total, $address){
        if ($address){
            return (int)(($order_total * (Address::query()->find($address)['percent'] ?? 0)) / 100);
        }
        return (int)(($order_total * (Address::query()->first()['percent'] ?? 0)) / 100);
    }

    public static function serviceFee(){
        return (int)(json_decode(Information::query()->where('prefix', 'order')->first()->info)->service_fee ?? 0);
    }

    public static function packaging(){
        return (int)(json_decode(Information::query()->where('prefix', 'order')->first()->info)->packaging ?? 0);
    }

    public static function journal($journal){
        return $journal ? (int)(json_decode(Information::query()->where('prefix', 'order')->first()->info)->journal ?? 0) : 0;
    }

    public static function maxSize(){
        return (int)(json_decode(Information::query()->where('prefix', 'order')->first()->info)->max_size ?? 0);
    }

    public static function finallyTotal($order_total, $address, $journal){
        return array_sum([
            self::transportationCosts($order_total, $address),
            self::serviceFee(),
            self::packaging(),
            self::journal($journal),
            $order_total
        ]);
    }

}
