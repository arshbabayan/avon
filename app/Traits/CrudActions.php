<?php

namespace App\Traits;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Zip;

trait CrudActions{

    public function getOnlyFullTranslations($data,$field,$value,$not_required = []){
        $insertData = [];
        foreach ((new Controller())->languages as $lang){
            $translate = $validate = $data[$lang['prefix']];
            if (count($not_required) > 0){
                foreach ($not_required as $not){
                    unset($validate[$not]);
                }
            }
            if (!in_array(null, $validate, true)){
                $translate['language_id'] = $lang->id;
                $translate[$field] = $value;
                array_push($insertData,$translate);
            }
        }
        return $insertData;
    }

    public function cleanFloara($data, $translates = [], $others = []){
        foreach ((new Controller())->languages as $lang){
            foreach ($translates as $field){
                $data[$lang['prefix']][$field] = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>','',$data[$lang['prefix']][$field]);
                if (trim($data[$lang['prefix']][$field]) == ''){
                    $data[$lang['prefix']][$field] = null;
                }
            }
        }
        foreach ($others as $other){
            $data[$other] = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>','',$data[$other]);
            if (trim($data[$other]) == ''){
                $data[$other] = null;
            }
        }
        return $data;
    }

    public function floaraImages($html){
        $images = [];
        preg_match_all('/<img.*?src\s*=.*?>/', $html, $matches, PREG_SET_ORDER);
        foreach ($matches as $image) {
            preg_match('/src="([^"]*)"/i', $image[0], $array);
            array_push($images, $array[1]);
        }
        return $images;
    }

    public function getOnlyValidImages($data){
        foreach ($data as $key => $image){
            $image_value = ['image' => $image];
            $image_rule = ['image' => 'image|mimes:jpeg,png,jpg,gif|max:10000'];
            $image_validate = Validator::make($image_value, $image_rule);
            if ($image_validate->fails()){
                unset($data[$key]);
            }
        }
        return $data;
    }

    public function missedAndExistsFiles($column, $file){
        $missing = [];
        $exists = [];
        $names = [];

        $data = explode(', ', $column);
        foreach ($data as $key => $value){
            if (trim($value) !== ''){
                array_push($names, $value);
            }
        }

        if (is_null($file)){
            $missing = $names;
        } else{
            $zip = Zip::open($file);
            foreach ($names as $image){
                if (!in_array($image, $zip->listFiles())){
                    array_push($missing, $image);
                } else{
                    array_push($exists, $image);
                }
            }
        }

        return [
            'missing' => $missing,
            'exists' => $exists,
        ];
    }

    public function generateCode($length, $user_id){
        $zero = '';
        for ($i = 0; $i < $length - strlen((string)$user_id); $i++){
            $zero .= '0';
        }
        return 'A' . $zero . $user_id;
    }

    public function generatePassword($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generateEmailToken($length){
        do {
            $email_token = "";
            $codeAlphabet= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for($i = 0; $i < $length; $i++){
                $email_token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
            }
            $unique = User::query()->where('email_token', $email_token)->first();
        }
        while(!is_null($unique));

        return $email_token;
    }
}
