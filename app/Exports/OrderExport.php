<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OrderExport implements FromView
{
    protected $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function view(): View
    {
        return view('main.exports.order', [
            'order' => $this->order
        ]);
    }
}
