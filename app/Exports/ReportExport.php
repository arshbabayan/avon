<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportExport implements FromView
{
    protected $catalog;
    protected $user;

    public function __construct($catalog, $user)
    {
        $this->catalog = $catalog;
        $this->user = $user;
    }

    public function view(): View
    {
        return view('main.exports.report', [
            'catalog' => $this->catalog,
            'user' => $this->user
        ]);
    }
}
