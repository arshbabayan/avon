<?php
namespace App\Helpers;

class Actions{

    public static function getInfo($from,$column){
        return json_decode($from->info)->{$column} ?? '';
    }

    public static function getInfoTranslate($from,$column){
        return json_decode($from->translate('info'))->{$column} ?? '';
    }

    public static function datesDiff($now,$end){
        $date1 = date_create($now);
        $date2 = date_create($end);
        $diff = date_diff($date1,$date2);
        return (int)$diff->format('%R%a') < 1 ? 0 : (int)$diff->format('%R%a');
    }

    public static function getStatusName($status){
        $type = '';
        switch($status){
            case 0:
                $type = 'in_processed';
                break;
            case 1:
                $type = 'payed';
                break;
            case 2:
                $type = 'finished';
                break;
            case 3:
                $type = 'canceled';
                break;
        }
        return $type;
    }
}
