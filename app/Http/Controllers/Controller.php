<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Information;
use App\Models\Language;
use App\Models\Status;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use App\Traits\Basket;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Basket;

    public $locale;
    public $default;
    public $languages;
    public $url_segments;
    public $route_name;
    public $information;
    public $active_catalog;
    public $statuses;

    public function __construct(){
        date_default_timezone_set("Asia/Yerevan");
        //Current language
        if (!session()->has('current_locale')){
            session()->put('current_locale',Language::query()->where('prefix',app()->getLocale())->first());
        }
        //Default language
        if (!session()->has('default_locale')){
            session()->put('default_locale',Language::query()->where('prefix',config('app.fallback_locale'))->first());
        }
        //Languages
        if (!session()->has('languages')){
            session()->put('languages',Language::all());
        }

        $this->locale = session()->get('current_locale');
        $this->default = session()->get('default_locale');
        $this->languages = session()->get('languages');
        $this->url_segments = explode('/', parse_url(url()->current())['path'] ?? '');
        $this->route_name = Route::currentRouteName();
        $this->information = Information::query()->whereIn('prefix',[
            'home', 'socials', 'order'
        ])->get()->keyBy('prefix');
        $this->active_catalog = Catalog::query()->where('active',true)->with('pages')->first();
        $this->statuses = Status::all();

        $this->middleware(function ($request, $next) {
            View::share('basket',$this->basket());
            return $next($request);
        });

        $this->share();
    }

    public function share(){
        View::share([
            'locale' => $this->locale,
            'default' => $this->default,
            'languages' => $this->languages,
            'url_segments' => $this->url_segments,
            'route_name' => $this->route_name,
            'information' => $this->information,
            'active_catalog' => $this->active_catalog,
            'statuses' => $this->statuses,
        ]);
    }
}
