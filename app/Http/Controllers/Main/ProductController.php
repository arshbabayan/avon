<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Status;

class ProductController extends Controller
{
    public function statuses($locale, $prefix){
        $status = Status::query()->where('prefix',$prefix)->firstOrFail();
        $products = Product::query()->whereHas('statuses', function($q) use($status){
            return $q->where('status_id', $status->id);
        })->with('images')->latest()->paginate(16);
        return view('main.products.statuses', compact('products', 'status'));
    }

    public function product($locale, $articul__id){
        $where = [
            'articul' => explode('__',$articul__id)[0] ?? 0,
            'id' => explode('__',$articul__id)[1] ?? 0,
        ];
        $product = Product::query()->where($where)->with('images')->firstOrFail();
        return view('main.products.product', compact('product'));
    }
}
