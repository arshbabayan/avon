<?php

namespace App\Http\Controllers\Main;

use App\Exports\ReportExport;
use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\Order;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Excel;

class ProfileController extends Controller{

    public function index(){
        return view('main.profile.index');
    }

    public function edit(){
        return view('main.profile.edit');
    }

    public function update(Request $request, $locale){
        try{
            $validate = Validator::make($request->all(),[
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'address' => 'required|max:255',
                'passport' => 'required|max:255',
                'phone' => 'required|max:255',
            ]);

            if ($validate->fails()){
                return response()->json([
                    'success' => false,
                    'errors' => $validate->errors()->getMessages()
                ]);
            }

            //Update user
            $user = User::query()->where('id', Auth::id())->first();
            $user->name = $request->get('name');
            $user->surname = $request->get('surname');
            $user->address = $request->get('address');
            $user->passport = $request->get('passport');
            $user->phone = $request->get('phone');
            $user->save();

            return response()->json(['success' => true]);
        } catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function signIn(Request $request, $locale){
        $validator = Validator::make($request->all(),[
            'code' => 'required|max:255',
            'password' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => trans('main.invalid_user')
            ]);
        }

        $data = array(
            'code' => $request->get('code'),
            'password' => $request->get('password')
        );

        if (Auth::attempt($data) && Auth::user()->role_id !== 9) {
            if (!is_null(Auth::user()->email_token)){
                Auth::logout();
                return response()->json([
                    'success' => false,
                    'message' => trans('main.email_not_verified')
                ]);
            }

            if (Auth::user()->blocked){
                Auth::logout();
                return response()->json([
                    'success' => false,
                    'message' => trans('main.user_blocked')
                ]);
            }
            return response()->json(['success' => true]);
        }

        Auth::logout();
        return response()->json([
            'success' => false,
            'message' => trans('main.invalid_user')
        ]);
    }

    public function logout($locale){
        Auth::logout();
        return redirect(route('locale.home', $locale));
    }

    public function getProduct($locale, $articul){
        try {
            $product = Product::query()->where('articul', $articul)->orderBy('id','desc')->first();

            if (is_null($product)){
                return response()->json([
                    'success' => false,
                ]);
            }

            return response()->json([
                'product_id' => $product->id,
                'title' => $product->translate('title'),
                'success' => true
            ]);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
            ]);
        }
    }

    public function storeToBasket(Request $request){
        try{
            $products = $request->get('products');

            $data = [];
            foreach ($products as $product_id => $count){
                if ((int)$product_id > 0 && (int)$count > 0){
                    $data[$product_id] = (int)$count;
                }
            }

            if (count($data) > 0){
                $basket = session()->get('basket', []);

                foreach ($data as $product_id => $count){
                    //Add
                    if (isset($basket[$product_id])){
                        $current_count = (int)$basket[$product_id];
                        $basket[$product_id] = $current_count + $count;
                    }
                    //Update
                    else{
                        $basket[$product_id] = $count;
                    }
                }

                session()->put('basket', $basket);
                $data = $this->basket();
                $data['success'] = true;
                return response()->json($data);
            }

            return response()->json([
                'success' => false,
            ]);
        } catch (\Exception $exception){
            return response()->json([
                'success' => false,
            ]);
        }
    }

    public function orders(){
        $orders = Order::query()
            ->where('user_id', Auth::id())
            ->orderBy('status', 'asc')
            ->latest()
            ->paginate(5);

        return view('main.profile.orders', compact('orders'));
    }

    public function reports(){
        $catalogs = Catalog::query()->where('id','>=', Auth::user()->registerCatalog()->id)->get();
        return view('main.profile.reports', compact('catalogs'));
    }

    public function exportReport(Request $request){
        $catalog = Catalog::query()->findOrFail($request->get('catalog_id'));
        $user = User::query()->findOrFail(Auth::id());

        return Excel::download(new ReportExport($catalog, $user), trans('report.report') . '.xlsx');
    }
}
