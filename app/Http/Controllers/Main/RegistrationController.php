<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Mail\Registration;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Traits\CrudActions;

class RegistrationController extends Controller
{
    use CrudActions;

    public function index(){
        return view('main.registration');
    }

    public function send(Request $request,$locale){
        try{
            $validate = Validator::make($request->all(),[
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'address' => 'required|max:255',
                'passport' => 'required|max:255',
                'phone' => 'required|max:255',
                'email' => 'required|email|unique:users,email,NULL,id|max:255',
                'coordinator' => 'required|exists:users,code|max:255',
                'privacy_policy' => 'required'
            ]);

            if ($validate->fails()){
                return response()->json([
                    'success' => false,
                    'errors' => $validate->errors()->getMessages()
                ]);
            }

            $coordinator = User::query()->where('code', $request->get('coordinator'))->first();

            //Not verified
            if (!$coordinator->verified){
                return response()->json([
                    'success' => false,
                    'errors' => [
                        'coordinator' => [trans('main.coordinator_not_verified')]
                    ]
                ]);
            }

            //Blocked
            if ($coordinator->blocked){
                return response()->json([
                    'success' => false,
                    'errors' => [
                        'coordinator' => [trans('main.coordinator_blocked')]
                    ]
                ]);
            }

            //Generate password and token
            $password = $this->generatePassword(8);
            $email_token = $this->generateEmailToken(32);

            //Create user
            $user = new User();
            $user->name = $request->get('name');
            $user->surname = $request->get('surname');
            $user->address = $request->get('address');
            $user->passport = $request->get('passport');
            $user->phone = $request->get('phone');
            $user->register_catalog = (new Controller())->active_catalog->id;
            $user->email = $request->get('email');
            $user->password = Hash::make($password);
            $user->coordinator_id = $coordinator->id;
            $user->role_id = 1;
            $user->email_token = $email_token;
            $user->save();

            //Setting code
            $code = $this->generateCode( 8, $user->id);
            $user->code = $code;
            $user->save();

            //Send to our address
            $data = [];
            $data['user'] = User::query()->where('id', $user->id)->with('coordinator')->first();
            $data['subject'] = trans('main.new_user', [], $this->default['prefix']);
            $data['reply'] = $user['email'];
            $data['view'] = 'main.mails.registration-our-mail';
            Mail::to(config('mail.registration.address'))->send(new Registration($data));

            //Send to user address
            $data['subject'] = trans('main.registration', [], $this->default['prefix']);
            $data['reply'] = config('mail.registration.address');
            $data['view'] = 'main.mails.registration-user-mail';
            $data['password'] = $password;
            Mail::to($request->get('email'))->send(new Registration($data));

            return response()->json(['success' => true]);
        } catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function verify(Request $request, $locale){
        $user = User::query()->where('email_token', $request->get('email_token'))->first();

        if (!is_null($user) && !$user->blocked){
            $user->email_token = null;
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->save();
            Auth::loginUsingId($user->id);
            return redirect(route('locale.profile.index', $locale));
        }

        return redirect(route('locale.home', $locale));
    }
}
