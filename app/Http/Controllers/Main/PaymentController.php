<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\PaymentHistory;
use DB;
use Auth;
use Carbon\Carbon;

class PaymentController extends Controller
{
    // CONVERSE BANK
    public function converse_post(Request $request){
        // dd(env('CONVERSE_PASSWORD'));
        define('BACK_URL', env('APP_URL') . '/ru/profile/converse-success');
        define('URL', 'https://ipay.arca.am/payment/rest/');
        // define('TEST_URL', 'https://ipaytest.arca.am:8445/payment/rest/'); 
        define('ARCA_REGISTER_URL', 'register.do');
        
        if((!$request->has('price')) || (!is_integer((int) $request->input('price')))){
            return redirect()->route('locale.profile.index')->with('error_payment', 'Ошибка платежа!');
        }

        $dbsave = DB::table('payment_histories')->insert([
            'user_id'=> Auth::id(),
            'amount'=> $_POST['price'],
            'method'=> 'conversebank',
            'success'=> 0,
            'created_at'=>Carbon::now()
        ]);

        $currency = array(
            'AMD' => '051',
            'USD' => '840'
        );
        $payment_err = '';
        $price = $_POST['price']; 
        $order_id = time() . mt_rand(); // generate custom order number
    
        $PostFields = array();
        $PostFields['userName'] = env('CONVERSE_LOGIN');
        $PostFields['password'] = env('CONVERSE_PASSWORD');
        $PostFields['returnUrl'] = BACK_URL;
        $PostFields['amount'] = $price.'00'; // require add 00 to and of price
        $PostFields['currency'] = $currency['AMD'];
        $PostFields['orderNumber'] = $order_id;
        $PostFields['language'] = 'ru';
        $PostFields['description'] = 'Avon - Balance replenishment';
    
        $PostFieldsArr = $PostFields;
        $PostFields = http_build_query($PostFields);
        $Response = $this->send_curl_request(URL . ARCA_REGISTER_URL, $PostFields);
        if($Response === false){
            $payment_err = 'Payment connect failed!';
        }
        $Response = json_decode($Response, true);
        if (empty($Response)){
            if($payment_err == '') $payment_err = 'Empty response';
        }
        else if(!empty($Response) && isset($Response['errorMessage'])){
            $payment_err = $Response['errorMessage'];
        }
        else{
            $Message = "<form action='" . $Response['formUrl'] . "' accept-charset='UTF-8' id='sendPayment' method='GET'>";
            $Message .= "<input type='hidden' name='mdOrder' value='" . $Response['orderId'] . "'>";
            $Message .= "<input type='Submit' style='display: none;' value='Send Payment'>";
            $Message .= "</form>";
            $Message .= "<script>document.getElementById('sendPayment').submit();</script>";
            echo $Message;
            die;
        }
        DB::table('payment_histories')->where('user_id', Auth::id())->where('success', 0)->delete();
        echo $payment_err;
    }
      
    public function send_curl_request($URL, $PostFields){
        $CurlOptions = array(
            CURLOPT_URL => $URL,
            // CURLOPT_PORT => '8445',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSLVERSION => '1',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $PostFields,
        );
        $CH = curl_init();
        if ($CH === false) {
            echo 'Initialization error #' . curl_errno($CH) . ' ---- ' . curl_error($CH);
        } else {
            if (curl_setopt_array($CH, $CurlOptions)) {
                $Result = curl_exec($CH);
                if (curl_errno($CH) > 0) {
                    $RetStr = false;
                } else {
                    $RetStr = $Result;
                }
            }
            curl_close($CH);
        }
        return $RetStr;
    }           
 
    public function back_url_conerse(Request $request){    
        define('URL', 'https://ipay.arca.am/payment/rest/');
        $ch = curl_init(URL . 'getOrderStatusExtended.do?userName='. env('CONVERSE_LOGIN') . '&password=' . env('CONVERSE_PASSWORD') . '&orderId=' . $_GET['orderId'] . '&language=ru');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);

    	curl_close($ch);
    	$orderStatus = json_decode($res, true)['orderStatus'];
    	
    	if($orderStatus == 2){ 
            $amount = DB::table('payment_histories')->where('user_id', Auth::id())->where('success', 0)->first()->amount;
            DB::table('payment_histories')->where('user_id', Auth::id())->where('success', 0)->update(['success'=> 1]);
            $user = User::find(Auth::id());
            $user->balance = $user->balance + $amount;
            $user->save();
            return redirect()->route('locale.profile.index')->with('success_payment', 'Платеж успешно обработан!');
    	}
    	else{
            DB::table('payment_histories')->where('user_id', Auth::id())->where('success', 0)->delete();
    	    $messageSuccess = \Lang::get('global.order_fail');
            $request->session()->flash('alert-fail-payment', $messageSuccess);
    	    return redirect()->route('locale.profile.index')->with('error_payment', 'Ошибка платежа!');
    	}
    	
    } 
}
