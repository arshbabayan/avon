<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\Page;

class CatalogController extends Controller{

    public function index($locale, $name){
        $catalog = Catalog::query()
            ->where('name',$name)
            ->where('active',true)
            ->with('pages','pages.products','pages.products.images')
            ->firstOrFail();
        return view('main.catalog.index', compact('catalog'));
    }

    public function getPage($locale, $name, $page){
        try{
            $catalog = Catalog::query()
                ->where('name', $name)
                ->where('active',true)
                ->first();
            $page = Page::query()->where([
                'id' =>$page,
                'catalog_id' => $catalog->id
            ])->with('products','products.images')->first();

            if (in_array(null, [$catalog, $page], true)){
                return response()->json(['success' => false]);
            }

            //Append translates
            $products = $page->products->each(function($row){
                $row->append('info');
            })->keyBy('id');

            return response()->json([
                'success' => true,
                'products' => $products,
            ]);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
