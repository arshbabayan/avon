<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Information;

class SinglePagesController extends Controller
{
    public function businessModel(){
        $current_information = Information::query()->where('prefix','business-model')->first();
        return view('main.pages.business-model', compact('current_information'));
    }

    public function aboutCompany(){
        $current_information = Information::query()->where('prefix','about-company')->first();
        return view('main.pages.about-company', compact('current_information'));
    }

    public function paymentOrders(){
        $current_information = Information::query()->where('prefix','payment-orders')->first();
        return view('main.pages.payment-orders', compact('current_information'));
    }

    public function deliveryPoints(){
        $current_information = Information::query()->where('prefix','delivery-points')->first();
        $addresses = Address::all();
        return view('main.pages.delivery-points', compact('current_information', 'addresses'));
    }

    public function termsOfUse(){
        $current_information = Information::query()->where('prefix','terms-of-use')->first();
        return view('main.pages.terms-of-use', compact('current_information'));
    }

    public function confidentiality(){
        $current_information = Information::query()->where('prefix','confidentiality')->first();
        return view('main.pages.confidentiality', compact('current_information'));
    }
}
