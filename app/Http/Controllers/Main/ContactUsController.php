<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactUsController extends Controller
{
    public function index(){
        return view('main.contact-us');
    }

    public function send(Request $request,$locale){
        try{
            $data = $request->only('full_name', 'email', 'phone', 'message');

            $validate = Validator::make($data,[
                'full_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required'
            ]);

            if ($validate->fails()){
                return response()->json([
                    'success' => false,
                    'errors' => $validate->errors()->getMessages()
                ]);
            }

            $data['subject'] = trans('main.contact_us', [], $this->default['prefix']);
            $data['reply'] = $data['email'];
            $data['view'] = 'main.mails.contact-us';

            Mail::to(config('mail.contact_us.address'))->send(new ContactUs($data));

            return response()->json(['success' => true]);
        } catch (\Exception $exception){
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
