<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Traits\Basket;

class BasketController extends Controller
{
    use Basket;

    public function index(){
        $addresses = Address::all();
        return view('main.order.basket', compact('addresses'));
    }

    public function update(Request $request){
        try{
            $product_id = $request->get('product_id');
            $count = (int)$request->get('count');
            $type = $request->get('type');

            //Validation for basket
            $product = Product::query()->with('images')->find($product_id);
            if (is_null($product) || !in_array($type, ['add', 'update', 'remove'])){
                return response()->json(['success' => false]);
            }

            $basket = session()->get('basket', []);

            //Add or remove from basket
            if ($type === 'add'){
                $new_count = $count < 1 ? 1 : $count;
                //Add
                if (isset($basket[$product_id])){
                    $current_count = (int)$basket[$product_id];
                    $basket[$product_id] = $current_count + $new_count;
                }
                //Update
                else{
                    $basket[$product_id] = $new_count;
                }
            }
            else if ($type === 'update'){
                if (isset($basket[$product_id])){
                    $basket[$product_id] = $count < 1 ? 1 : $count;
                }
            }
            else if ($type === 'remove'){
                if (isset($basket[$product_id])){
                    unset($basket[$product_id]);
                }
            }

            session()->put('basket', $basket);

            $data = $this->basket();
            $data['success'] = true;

            return response()->json($data);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }
}
