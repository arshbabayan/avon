<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\Basket;

class GlobalController extends Controller
{
    use Basket;

    public function setUser(){
        return response()->json([
            'easy_start' => (boolean)(Auth::user() && Auth::user()->easyStart()),
            'easy_start_step' => (int)(Auth::user() ? Auth::user()->easyStartStep() : 0),
        ]);
    }

    public function updateBasketPage(Request $request){
        $address = (int)$request->get('address');
        $journal = (boolean)($request->get('journal') === 'true');
        return response()->json(Basket::basket($address, $journal));
    }
}
