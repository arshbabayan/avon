<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Information;
use App\Models\Slider;

class IndexController extends Controller{
    public function index(){
        $sliders = Slider::query()->orderBy('id','desc')->get();

        $current_information = Information::query()->whereIn('prefix',[
            'how-to-join-our-team', 'how-to-become-a-coordinator', 'how-to-become-a-regional-manager'
        ])->get()->keyBy('prefix');

        return view('main.index', compact('sliders','current_information'));
    }
}
