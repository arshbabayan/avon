<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Mail\OrderMail;
use App\Models\Order;
use App\Models\OrderProducts;
use App\Traits\Basket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    use Basket;

    public function checkout(Request $request){
        try {
            //Getting basket
            $address = (int)$request->get('address');
            $journal = (boolean)($request->get('journal') === 'true');
            $basket = $this->basket($address,$journal);

            //If no user or empty basket
            if ((Auth::guest() || Auth::user()['role_id'] === 9) || count($basket['cache']) === 0){
                return response()->json([
                    'success' => false,
                    'error' => 'invalid'
                ]);
            }

            //Getting user
            $user = Auth::user();

            //If maximum size is invalid
            if ($basket['finally_total'] > $this->maxSize()){
                return response()->json([
                    'success' => false,
                    'error' => 'max_size_error',
                    'size' => $this->maxSize()
                ]);
            }

            //If user has debt
            if ($user['balance'] < 0){
                return response()->json([
                    'success' => false,
                    'error' => 'has_debt',
                ]);
            }

            $payed = 0;
            $will_be_pay = 0;
            $balance = $user['balance'] - $basket['finally_total'];

            if ($balance < 0){
                $payed += $user['balance'];
                $will_be_pay = -$balance;
            } else{
                $payed = $basket['finally_total'];
            }

            //Insert order
            $order = new Order();
            $order->user_id = $user['id'];
            $order->catalog_id = $this->active_catalog->id;
            $order->status = $balance < 0 ? 0 : 1;
            $order->payed = $payed;
            $order->will_be_pay = $will_be_pay;
            $order->address_id = $address;

            $order->products_with_discount_total = $basket['products_with_discount_total'];
            $order->affordable_discount = $basket['affordable_discount'];
            $order->products_with_discount_finally_total = $basket['products_with_discount_finally_total'];

            $order->products_not_discount_total = $basket['products_not_discount_total'];
            $order->products_not_discount_finally_total = $basket['products_not_discount_finally_total'];

            $order->products_finally_total = $basket['products_finally_total'];
            $order->bonus = $basket['basket_bonus'];
            $order->order_total = $basket['order_total'];

            $order->transportation_costs = $basket['transportation_costs'];
            $order->service_fee = $basket['service_fee'];
            $order->packaging = $basket['packaging'];
            $order->journal = $basket['journal'];
            $order->finally_total = $basket['finally_total'];

            $order->used_prepayment = $payed - $user['bonus'] < 0 ? 0 : $payed - $user['bonus'];
            $order->unused_discount = $user['bonus'] - $basket['basket_bonus'];
            $order->unused_prepayment = $balance < 0 ? 0 : $balance;

            $order->in_processed_at = date('Y-m-d H:i:s');
            $order->payed_at = $balance < 0 ? null : date('Y-m-d H:i:s');
            $order->save();

            //Insert order products
            $insertData = [];
            foreach ($basket['products'] as $product_id => $product){
                array_push($insertData,[
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'price' => $product->discount($basket['cache'][$product->id]),
                    'count' => $basket['cache'][$product->id],
                    'not_discount' => $product->not_discount,
                    'returned' => false,
                    'returned_at' => null,
                ]);
            }
            OrderProducts::query()->insert($insertData);

            //Order data with products and user
            $order = Order::query()->where('id', $order->id)->with('user', 'items', 'items.product')->first();

            //Sending to mail
            $mail_data['subject'] = trans('main.order', [], $this->default['prefix']);
            $mail_data['reply'] = config('mail.contact_us.address');
            $mail_data['order'] = $order;
            Mail::to($user['email'])->send(new OrderMail($mail_data));

            //Start update user
            User::query()->where('id',Auth::id())->update([
                'balance' => $balance,
                'bonus' => $user['bonus'] - $basket['basket_bonus']
            ]);
            //End update user

            //Clear basket
            session()->put('basket',[]);

            return response()->json([
                'success' => true,
            ]);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
