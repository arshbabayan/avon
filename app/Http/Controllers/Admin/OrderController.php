<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProducts;
use Illuminate\Http\Request;
use App\Traits\Basket;

class OrderController extends Controller
{
    use Basket;

    public $type;
    public $types;

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->type = $request->segment(4);
        if (!in_array($this->type, ['finished', 'payed', 'in_processed', 'canceled'])){
            abort(404);
        }

        $finished = Order::query()->where('status', 2)->get()->count();
        $payed = Order::query()->where('status', 1)->get()->count();
        $in_processed = Order::query()->where('status', 0)->get()->count();
        $canceled = Order::query()->where('status', 3)->get()->count();

        $this->types = [
            'finished' => $finished,
            'payed' => $payed,
            'in_processed' => $in_processed,
            'canceled' => $canceled,
        ];
    }

    public function index(Request $request, $locale, $type){
        $orders = Order::query();
        $types = $this->types;

        switch ($type) {
            case 'finished':
                $orders = $orders->where('status', 2)->orderByDesc('finished_at');
                break;
            case 'payed':
                $orders = $orders->where('status', 1)->orderByDesc('payed_at');
                break;
            case 'in_processed':
                $orders = $orders->where('status', 0)->orderByDesc('in_processed_at');
                break;
            case 'canceled':
                $orders = $orders->where('status', 3)->orderByDesc('canceled_at');
                break;
        }
        $orders = $orders->latest()->paginate(25);

        return view('admin.orders.index', compact('orders', 'types', 'type'));
    }

    public function products($locale, $type, $order_id){
        $order = Order::query()->findOrFail($order_id);
        $types = $this->types;

        return view('admin.orders.products', compact('order', 'types', 'type'));
    }

    public function finish($locale, $type, $order_id){
        $order = Order::query()->findOrFail($order_id);
        $update = [
            'status' => 2,
            'finished_at' => date('Y-m-d H:i:s'),
        ];
        $order->update($update);
        return redirect()->back()->with('sweet_alert', 'order_finished');
    }

    public function pay($locale, $type, $order_id){
        $order = Order::query()->findOrFail($order_id);
        $update = [
            'status' => 1,
            'payed_at' => date('Y-m-d H:i:s'),
        ];
        $order->update($update);
        return redirect()->back()->with('sweet_alert', 'order_payed');
    }

    public function change($locale, $type, $order_id, $which, $action){
        if (is_null(Order::query()->find($order_id))){
            abort(404);
        }

        if ($which !== 'all' && is_null(OrderProducts::query()->where('order_id', $order_id)->where('id', $which)->first())){
            abort(404);
        }

        if (!in_array($action, ['cancel', 'process'])){
            abort(404);
        }

        $current_status = Order::query()->find($order_id)['status'];

        $information = self::recalculate($order_id, $which, $action);

        $message = '';

        if ($which === 'all'){
            switch ($information['status']) {
                case 0:
                    $message = 'order_in_processed';
                    break;
                case 3:
                    $message = 'order_canceled';
                    break;
            }
        } else{
            $message = $action === 'cancel' ? 'order_item_canceled' : 'order_item_in_processed';
        }

        if ($information['status'] !== $current_status){
            $new_type = $information['status'] === 0 ? 'in_processed' : 'canceled';
            return redirect(route('locale.admin.orders.index', ['locale' => $locale, 'type' => $new_type]))->with('sweet_alert', $message);
        } else{
            return redirect()->back()->with('sweet_alert', $message);
        }
    }

    public static function recalculate($order_id, $which, $action){
        $order = Order::query()->with('user', 'address', 'items')->find($order_id);
        $current_date = date('Y-m-d H:i:s');

        //Order properties
        $status = 0;
        $products_with_discount_total = 0;
        $affordable_discount = 0;
        $products_with_discount_finally_total = 0;
        $products_not_discount_total = 0;
        $products_not_discount_finally_total = 0;
        $products_finally_total = 0;
        $order_total = 0;
        $transportation_costs = 0;
        $service_fee = 0;
        $packaging = 0;
        $finally_total = 0;
        $in_processed_at = null;
        $canceled_at = null;

        //Update order items
        if ($which === 'all'){
            $items = OrderProducts::query()->with('product')->where('order_id', $order_id)->get();
            if ($action === 'cancel'){
                foreach ($items as $item){
                    $item->update([
                        'returned' => true,
                        'returned_at' => $current_date
                    ]);
                }
                $status = 3;
                $canceled_at = $current_date;
            }
            else{
                foreach ($items as $item){
                    $item->update([
                        'price' => self::prices($order['user'], $item['product'], $item['count'])['discount'],
                        'returned' => false,
                        'returned_at' => null
                    ]);
                }
                $in_processed_at = $current_date;
            }
        }
        else{
            $item = OrderProducts::query()->with('product')->find($which);
            if ($action === 'cancel'){
                $item->update([
                    'returned' => true,
                    'returned_at' => $current_date
                ]);
                $status = $order['items']->count() === $order['returnedProducts']->count() ? 3 : 0;
                $in_processed_at = $status === 0 ? $current_date : null;
                $canceled_at = $status === 3 ? $current_date : null;
            }
            else{
                $item->update([
                    'price' => self::prices($order['user'], $item['product'], $item['count'])['discount'],
                    'returned' => false,
                    'returned_at' => null
                ]);
                $in_processed_at = $current_date;
            }
        }

        if ($status !== 3){
            $items = OrderProducts::query()->with('product')->where('order_id', $order_id)->get();
            foreach ($items as $item){
                if (!$item['returned']){
                    if ($item['product']['not_discount'] === 0){
                        $products_with_discount_total += $item['count'] * $item['price'];
                    } else{
                        $products_not_discount_total += $item['count'] * $item['price'];
                    }
                }
            }

            //Discount
            $total_for_get_discount = $products_with_discount_total + $products_not_discount_total;
            if ($total_for_get_discount > 49999){
                $affordable_discount = 30;
            } elseif ($total_for_get_discount > 29999){
                $affordable_discount = 25;
            } elseif ($total_for_get_discount > 14999){
                $affordable_discount = 20;
            } elseif ($total_for_get_discount > 4999){
                $affordable_discount = 15;
            }

            $products_with_discount_finally_total = ((100 - ($affordable_discount)) * $products_with_discount_total) / 100;
            $products_not_discount_finally_total = $products_not_discount_total;

            //Products finally total
            $products_finally_total = $products_with_discount_finally_total + $products_not_discount_finally_total;

            //Total
            $order_total = $products_finally_total - $order['bonus'];
            $transportation_costs = (int)self::transportationCosts($order_total, $order['address_id']);
            $service_fee = (int)self::serviceFee();
            $packaging = (int)self::packaging();
            $finally_total = (int)self::finallyTotal($order_total, $order['address_id'], $order['journal']);
        }

        $order->update([
            'status' => $status,
            'products_with_discount_total' => $products_with_discount_total,
            'affordable_discount' => $affordable_discount,
            'products_with_discount_finally_total' => $products_with_discount_finally_total,
            'products_not_discount_total' => $products_not_discount_total,
            'products_not_discount_finally_total' => $products_not_discount_finally_total,
            'products_finally_total' => $products_finally_total,
            'order_total' => $order_total,
            'transportation_costs' => $transportation_costs,
            'service_fee' => $service_fee,
            'packaging' => $packaging,
            'finally_total' => $finally_total,
            'in_processed_at' => $in_processed_at,
            'canceled_at' => $canceled_at,
        ]);

        return [
            'status' => $status
        ];
    }

    private static function prices($user, $product, $count) {
        $price = $product['price'];
        $discount = $product['discount'];

        if ($product['discount'] === 0) {
            $discount = $product['price'];
        }
        if ($count === 1) {
            if ($product['discount'] > 0) {
                $price = $product['price'];
                $discount = $product['discount'];
            }
        } else {
            if ($product['discount_for_more'] > 0) {
                $price = $product['discount'] > 0 ? $product['discount'] : $product['price'];
                $discount = $product['discount_for_more'];
            }
        }

        if ($user && $user->easyStart() && $product['easy_start'] && $user->easyStartStep() === $product['easy_start_step']) {
            $discount = $product['easy_start_price'];
        }

        return [
            'price' => $price,
            'discount' => $discount,
        ];
    }
}
