<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderSystemExport;
use App\Exports\ProductsSystemExport;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderProducts;
use App\User;
use Illuminate\Http\Request;
use Excel;

class ReportController extends Controller
{
    public function orders(Request $request){
        $addresses = Address::all();
        $coordinators = User::query()->where('role_id', '!=', 9)->get();

        $data = Order::query();
        $from_date = $request->get('from_date', null);
        $to_date = $request->get('to_date', null);
        $status = $request->get('status', null);
        $address = $request->get('address', null);
        $coordinator = $request->get('coordinator', null);

        if (!is_null($from_date)){
            $data = $data->where('created_at', '>=', $from_date);
        }
        if (!is_null($to_date)){
            $data = $data->where('created_at', '<=', $to_date);
        }
        if (!is_null($status)){
            $data = $data->where('status', $status);
        }
        if (!is_null($address)){
            $data = $data->where('address_id', $address);
        }
        if (!is_null($coordinator)){
            $user_ids = User::query()->where('id', $coordinator)->orWhere('coordinator_id', $coordinator)->get()->pluck('id')->toArray();
            $data = $data->whereIn('user_id', $user_ids);
        }

        $data = $data->with('catalog', 'user', 'address')->latest()->paginate(25);

        return view('admin.report.orders', compact('data', 'coordinators', 'addresses'));
    }

    public function ordersDownload(Request $request){
        $data = Order::query();
        $from_date = $request->get('from_date', null);
        $to_date = $request->get('to_date', null);
        $status = $request->get('status', null);
        $address = $request->get('address', null);
        $coordinator = $request->get('coordinator', null);

        if (!is_null($from_date)){
            $data = $data->where('created_at', '>=', $from_date);
        }
        if (!is_null($to_date)){
            $data = $data->where('created_at', '<=', $to_date);
        }
        if (!is_null($status)){
            $data = $data->where('status', $status);
        }
        if (!is_null($address)){
            $data = $data->where('address_id', $address);
        }
        if (!is_null($coordinator)){
            $user_ids = User::query()->where('id', $coordinator)->orWhere('coordinator_id', $coordinator)->get()->pluck('id')->toArray();
            $data = $data->whereIn('user_id', $user_ids);
        }

        $data = $data->with('catalog', 'user', 'address')->orderByDesc('created_at')->get();

        return Excel::download(new OrderSystemExport($data), trans('report.report') . '.xlsx');
    }

    public function products(Request $request){
        $addresses = Address::all();
        $coordinators = User::query()->where('role_id', '!=', 9)->get();

        $data = OrderProducts::query();
        $from_date = $request->get('from_date', null);
        $to_date = $request->get('to_date', null);
        $address = $request->get('address', null);
        $coordinator = $request->get('coordinator', null);
        $from_articul = $request->get('from_articul', null);
        $to_articul = $request->get('to_articul', null);
        $returned = $request->get('returned', null);

        if (!is_null($from_date)){
            $data = $data->whereHas('order', function ($q) use ($from_date){
                 return $q->where('created_at', '>=', $from_date);
            });
        }
        if (!is_null($to_date)){
            $data = $data->whereHas('order', function ($q) use ($to_date){
                return $q->where('created_at', '<=', $to_date);
            });
        }
        if (!is_null($address)){
            $data = $data->whereHas('order', function ($q) use ($address){
                return $q->where('address_id', $address);
            });
        }
        if (!is_null($coordinator)){
            $user_ids = User::query()->where('id', $coordinator)->orWhere('coordinator_id', $coordinator)->get()->pluck('id')->toArray();
            $data = $data->whereHas('order', function ($q) use ($user_ids){
                return $q->whereIn('user_id', $user_ids);
            });
        }
        if (!is_null($from_articul)){
            $data = $data->whereHas('product', function ($q) use ($from_articul){
                return $q->where('articul', '>=', (int)$from_articul);
            });
        }
        if (!is_null($to_articul)){
            $data = $data->whereHas('product', function ($q) use ($to_articul){
                return $q->where('articul', '<=', (int)$to_articul);
            });
        }
        if (!is_null($returned)){
            $data = $data->where('returned', (boolean)$returned);
        }

        $data = $data->with('product')->get()->groupBy('product_id');

        return view('admin.report.products', compact('data', 'coordinators', 'addresses'));
    }

    public function productsDownload(Request $request){
        $addresses = Address::all();
        $coordinators = User::query()->where('role_id', '!=', 9)->get();

        $data = OrderProducts::query();
        $from_date = $request->get('from_date', null);
        $to_date = $request->get('to_date', null);
        $address = $request->get('address', null);
        $coordinator = $request->get('coordinator', null);
        $from_articul = $request->get('from_articul', null);
        $to_articul = $request->get('to_articul', null);
        $returned = $request->get('returned', null);

        if (!is_null($from_date)){
            $data = $data->whereHas('order', function ($q) use ($from_date){
                return $q->where('created_at', '>=', $from_date);
            });
        }
        if (!is_null($to_date)){
            $data = $data->whereHas('order', function ($q) use ($to_date){
                return $q->where('created_at', '<=', $to_date);
            });
        }
        if (!is_null($address)){
            $data = $data->whereHas('order', function ($q) use ($address){
                return $q->where('address_id', $address);
            });
        }
        if (!is_null($coordinator)){
            $user_ids = User::query()->where('id', $coordinator)->orWhere('coordinator_id', $coordinator)->get()->pluck('id')->toArray();
            $data = $data->whereHas('order', function ($q) use ($user_ids){
                return $q->whereIn('user_id', $user_ids);
            });
        }
        if (!is_null($from_articul)){
            $data = $data->whereHas('product', function ($q) use ($from_articul){
                return $q->where('articul', '>=', (int)$from_articul);
            });
        }
        if (!is_null($to_articul)){
            $data = $data->whereHas('product', function ($q) use ($to_articul){
                return $q->where('articul', '<=', (int)$to_articul);
            });
        }
        if (!is_null($returned)){
            $data = $data->where('returned', (boolean)$returned);
        }

        $data = $data->with('product')->get()->groupBy('product_id');

        return Excel::download(new ProductsSystemExport($data), trans('report.report') . '.xlsx');
    }
}
