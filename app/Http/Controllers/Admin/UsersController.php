<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\Registration;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Traits\CrudActions;

class UsersController extends Controller
{
    use CrudActions;

    public function index(Request $request, $locale)
    {
        $type = $request->get('type');
        if (!in_array($type, ['verified', 'unverified', 'blocked', 'deleted'])){
            return redirect(route('locale.admin.users.index', ['locale' => $locale, 'type' => 'verified']));
        }

        $verified = User::query()
            ->where('role_id', '<', 9)
            ->where('verified', true)
            ->where('blocked', false)
            ->get()->count();

        $unverified = User::query()
            ->where('role_id', '<', 9)
            ->where('verified', false)
            ->where('blocked', false)
            ->get()->count();

        $blocked = User::query()
            ->where('role_id', '<', 9)
            ->where('verified', false)
            ->where('blocked', true)
            ->get()->count();

        $deleted = User::query()
            ->where('role_id', '<', 9)
            ->onlyTrashed()
            ->get()->count();

        $types = [
            'verified' => $verified,
            'unverified' => $unverified,
            'blocked' => $blocked,
            'deleted' => $deleted,
        ];

        $users = User::query();

        switch ($type){
            case 'verified':
                $users = $users
                    ->where('verified',true)
                    ->where('blocked',false)
                    ->orderBy('verified_at', 'desc');
                break;
            case 'unverified':
                $users = $users
                    ->where('verified',false)
                    ->where('blocked',false)
                    ->orderBy('verified_at', 'desc');
                break;
            case 'blocked':
                $users = $users
                    ->where('verified',false)
                    ->where('blocked',true)
                    ->orderBy('blocked_at', 'desc');
                break;
            default:
                $users = $users->onlyTrashed()->orderBy('deleted_at', 'desc');
        }

        $users = $users
            ->where('role_id', '<', 9)
            ->with('coordinator')
            ->latest()
            ->paginate(25);

        return view('admin.users.index', compact('users', 'types'));
    }

    public function create()
    {
        $coordinators = User::query()->where('role_id', '<', 9)->get();
        $roles = Role::query()->where('id', '<', 9)->get();
        return view('admin.users.create', compact('coordinators', 'roles'));
    }

    public function store(Request $request, $locale)
    {
        $values = $request->all('name', 'surname', 'address', 'passport', 'phone', 'balance', 'bonus', 'email', 'role', 'coordinator');

        $rules = [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'address' => 'required|max:255',
            'passport' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email|unique:users,email,NULL,id|max:255',
            'role' => 'required|exists:roles,id|max:255',
        ];

        if ($request->get('coordinator')) {
            $rules['coordinator'] = 'exists:users,id|max:255';
        }

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        //Generate password and token
        $password = $this->generatePassword(8);
        $email_token = $this->generateEmailToken(32);

        //Create user
        $user = new User();
        $user->name = $values['name'];
        $user->surname = $values['surname'];
        $user->address = $values['address'];
        $user->passport = $values['passport'];
        $user->phone = $values['phone'];
        $user->balance = floatval($values['balance']);
        $user->bonus = floatval($values['bonus']);
        $user->register_catalog = (new Controller())->active_catalog->id;
        $user->email = $values['email'];
        $user->password = Hash::make($password);
        $user->coordinator_id = (int)$values['coordinator'] ?? 0;
        $user->role_id = (int)$values['role'] ?? 1;
        $user->email_token = $email_token;
        $user->save();

        //Setting code
        $code = $this->generateCode( 8, $user->id);
        $user->code = $code;
        $user->save();

        //Send to our address
        $data = [];
        $data['user'] = User::query()->where('id', $user->id)->with('coordinator')->first();
        $data['subject'] = trans('main.new_user', [], $this->default['prefix']);
        $data['reply'] = $user['email'];
        $data['view'] = 'main.mails.registration-our-mail';
        Mail::to(config('mail.registration.address'))->send(new Registration($data));

        //Send to user address
        $data['subject'] = 'Avon Cosmetics' . ' | ' . trans('main.registration', [], $this->default['prefix']);
        $data['reply'] = config('mail.registration.address');
        $data['view'] = 'main.mails.registration-user-mail';
        $data['password'] = $password;
        Mail::to($request->get('email'))->send(new Registration($data));

        return redirect(route('locale.admin.users.index',$locale))->with('sweet_alert', 'added');
    }

    public function show($locale, $id)
    {
        $user = User::query()->with('coordinator')->findOrFail($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($locale,$id)
    {
        $coordinators = User::query()->where('id', '!=', $id)->where('role_id', '<', 9)->get();
        $roles = Role::query()->where('id', '<', 9)->get();
        $user = User::query()->with('coordinator')->findOrFail($id);

        return view('admin.users.edit', compact('coordinators', 'roles','user'));
    }

    public function update(Request $request, $locale, $id)
    {
        $user = User::query()->with('coordinator')->findOrFail($id);
        $values = $request->all('name', 'surname', 'address', 'passport', 'phone', 'balance', 'bonus', 'email', 'role', 'coordinator');

        $rules = [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'address' => 'required|max:255',
            'passport' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id . ',id|max:255',
            'role' => 'required|exists:roles,id|max:255',
        ];

        if ($request->get('coordinator')) {
            $rules['coordinator'] = 'exists:users,id|max:255';
        }

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $user->name = $values['name'];
        $user->surname = $values['surname'];
        $user->address = $values['address'];
        $user->passport = $values['passport'];
        $user->phone = $values['phone'];
        $user->balance = floatval($values['balance']);
        $user->bonus = floatval($values['bonus']);
        $user->email = $values['email'];
        $user->coordinator_id = (int)$values['coordinator'] ?? 0;
        $user->role_id = (int)$values['role'] ?? 1;
        $user->save();

        return redirect()->back()->with('sweet_alert', 'updated');
    }

    public function destroy(Request $request, $locale, $id)
    {
        $user = User::query()->findOrFail($id);
        $user->delete();
        return redirect()->back()->with('sweet_alert', 'deleted');
    }

    public function verify(Request $request, $locale, $id){
        $user = User::query()->findOrFail($id);
        $update = [
            'verified' => $user->verified ? false : true,
            'verified_at' => $user->verified ? null : date('Y-m-d H:i:s'),
        ];
        $user->update($update);
        $message = $user->verified ? 'verified' : 'unverified';

        return redirect()->back()->with('sweet_alert', $message);
    }

    public function block(Request $request, $locale, $id){
        $user = User::query()->findOrFail($id);
        $update = [
            'blocked' => $user->blocked ? false : true,
            'blocked_at' => $user->blocked ? null : date('Y-m-d H:i:s'),
        ];
        $user->update($update);
        $message = $user->blocked ? 'blocked' : 'unblocked';

        return redirect()->back()->with('sweet_alert', $message);
    }

    public function restore(Request $request, $locale, $id){
        User::withTrashed()->findOrFail($id)->restore();
        $message = 'restored';

        return redirect()->back()->with('sweet_alert', $message);
    }
}
