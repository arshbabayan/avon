<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Catalog;
use App\User;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use App\Traits\CrudActions;
use Illuminate\View\View;

class CatalogController extends Controller
{
    use CrudActions;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $catalogs = Catalog::query()
                ->where('name', 'LIKE', "%$keyword%")
                ->withCount('pages')
                ->latest()
                ->paginate($perPage);
        } else {
            $catalogs = Catalog::query()->withCount('pages')->latest()->paginate($perPage);
        }

        return view('admin.catalog.index', compact('catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.catalog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @param $locale
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request, $locale)
    {
        $rules = [
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif|required|max:10000',
            'catalog_name' => 'required|unique:catalogs,name,NULL,id|max:255',
        ];

        $validate = Validator::make($request->all(),$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $thumbnail_path = time() . $request->file('thumbnail')->getClientOriginalName();
        $request->file('thumbnail')->move('main/catalogs',$thumbnail_path);

        $catalog = new Catalog();
        $catalog->thumbnail = $thumbnail_path;
        $catalog->name = $request->get('catalog_name');
        $catalog->deadline = date('Y-m-d', strtotime(date('Y-m-d'). ' + 21 days'));
        $catalog->save();

        return redirect(route('locale.admin.catalog.index',$locale))->with('sweet_alert', 'added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $locale
     * @param  int  $id
     *
     * @return View
     */
    public function show($locale, $id)
    {
        $catalog = Catalog::query()->withCount('pages')->findOrFail($id);
        return view('admin.catalog.show', compact('catalog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $locale
     * @param  int  $id
     *
     * @return View
     */
    public function edit($locale, $id)
    {
        $catalog = Catalog::query()->findOrFail($id);
        return view('admin.catalog.edit', compact('catalog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $locale
     * @param int $id
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $locale, $id)
    {
        $catalog = Catalog::query()->findOrFail($id);

        $rules = [
            'catalog_name' => "required|unique:catalogs,name," . $catalog->id . ",id|max:255",
        ];

        if ($request->hasFile('thumbnail')){
            $rules['thumbnail'] = 'image|mimes:jpeg,png,jpg,gif|required|max:10000';
        }

        $validate = Validator::make($request->all(),$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        if ($request->hasFile('thumbnail')){
            @unlink(public_path('main/catalogs/' . $catalog->thumbnail));
            $thumbnail_path = time() . $request->file('thumbnail')->getClientOriginalName();
            $request->file('thumbnail')->move('main/catalogs',$thumbnail_path);
            $catalog->thumbnail = $thumbnail_path;
        }

        $catalog->name = $request->get('catalog_name');
        $catalog->save();

        return redirect(route('locale.admin.catalog.index',$locale))->with('sweet_alert', 'updated');
    }

    public function activate($locale, $id)
    {
        if (Catalog::all()->count() > 1){
            User::updateUsersEasyStart();
            User::updateUsersBonus();
            User::query()->where('inactive_catalogs_count', '>', 17)->delete();
        }
        Catalog::query()->update([
                'last_active' => false,
            ]);
        $this->active_catalog->update(['last_active' => true]);
        Catalog::query()->update([
                'active' => false,
            ]);
        
        Catalog::query()->where('id',$id)->update(['active' => true]);
        return redirect(route('locale.admin.catalog.index',$locale))->with('sweet_alert', 'activated');
    }
}
