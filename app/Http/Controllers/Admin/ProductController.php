<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductStatuses;
use App\Models\ProductTranslations;
use App\Models\Status;
use App\Traits\CrudActions;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ProductController extends Controller
{
    use CrudActions;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $products = Product::query()->where('articul', 'LIKE', "%$keyword%")->with('images')->latest()->paginate($perPage);
        } else {
            $products = Product::query()->with('images')->latest()->paginate($perPage);
        }

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $statuses = Status::all();
        return view('admin.product.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @param $locale
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request,$locale)
    {
        $requestData = $this->cleanFloara($request->all(),['description', 'composition']);
        $values = $requestData[$this->default['prefix']];
        $values['slug'] = Str::slug($values['title']);
        $values['articul'] = $requestData['articul'];
        $values['price'] = $requestData['price'];
        $values['discount'] = $requestData['discount'];
        $values['discount_for_more'] = $requestData['discount_for_more'];
        $values['easy_start'] = $requestData['easy_start'] ?? false ? true : false;
        $values['easy_start_price'] = $values['easy_start'] ? $requestData['easy_start_price'] : 0;
        $values['easy_start_step'] = $values['easy_start'] ? $requestData['easy_start_step'] : 0;
        $values['not_discount'] = $requestData['not_discount'] ?? false ? true : false;
        $values['statuses'] = $requestData['statuses'] ?? [];
        $values['product_images'] = $this->getOnlyValidImages($requestData['product_images'] ?? []);

        $rules = [
            'articul' => 'required|max:255',
            'title' => 'required|max:255',
        ];

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $product = new Product();
        $product->slug = $values['slug'];
        $product->articul = (int)$values['articul'];
        $product->price = (int)$values['price'];
        $product->discount = (int)$values['discount'];
        $product->discount_for_more = (int)$values['discount_for_more'];
        $product->easy_start = (boolean)$values['easy_start'];
        $product->easy_start_price = (int)$values['easy_start_price'];
        $product->easy_start_step = (int)$values['easy_start_step'];
        $product->not_discount = (boolean)$values['not_discount'];
        $product->save();

        //Insert translations
        $insertData = $this->getOnlyFullTranslations($requestData,'product_id', $product->id, ['description','composition']);
        ProductTranslations::query()->insert($insertData);

        //Insert images
        if (count($values['product_images']) > 0){
            $insertImages = [];
            foreach ($values['product_images'] as $product_image){
                $image_path = time() . $product_image->getClientOriginalName();
                $product_image->move('main/products',$image_path);
                array_push($insertImages,[
                    'product_id' => $product->id,
                    'image' => $image_path
                ]);
            }
            ProductImages::query()->insert($insertImages);
        }

        //Insert statuses
        if (count($values['statuses']) > 0) {
            $insertStatuses = [];
            foreach ($values['statuses'] as $status_id){
                array_push($insertStatuses,[
                    'product_id' => $product->id,
                    'status_id' => $status_id
                ]);
            }
            ProductStatuses::query()->insert($insertStatuses);
        }

        return redirect(route('locale.admin.product.index',$locale))->with('sweet_alert', 'added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  string  $locale
     *
     * @return View
     */
    public function show($locale,$id)
    {
        $product = Product::query()->with('images', 'statuses')->findOrFail($id);

        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param  string  $locale
     *
     * @return View
     */
    public function edit($locale,$id)
    {
        $product = Product::query()->with('images', 'statuses')->findOrFail($id);
        $statuses = Status::all();

        return view('admin.product.edit', compact('product', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $locale
     * @param int $id
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $locale, $id)
    {
        $product = Product::query()->with('images')->find($id);

        $requestData = $this->cleanFloara($request->all(),['description', 'composition']);
        $values = $requestData[$this->default['prefix']];
        $values['slug'] = Str::slug($values['title']);
        $values['articul'] = $requestData['articul'];
        $values['price'] = $requestData['price'];
        $values['discount'] = $requestData['discount'];
        $values['discount_for_more'] = $requestData['discount_for_more'];
        $values['easy_start'] = $requestData['easy_start'] ?? false ? true : false;
        $values['easy_start_price'] = $values['easy_start'] ? $requestData['easy_start_price'] : 0;
        $values['easy_start_step'] = $values['easy_start'] ? $requestData['easy_start_step'] : 0;
        $values['not_discount'] = $requestData['not_discount'] ?? false ? true : false;
        $values['product_images'] = $this->getOnlyValidImages($requestData['product_images'] ?? []);
        $values['statuses'] = $requestData['statuses'] ?? [];

        $rules = [
            'articul' => 'required|max:255',
            'title' => 'required|max:255',
        ];

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $product->slug = $values['slug'];
        $product->articul = (int)$values['articul'];
        $product->price = (int)$values['price'];
        $product->discount = (int)$values['discount'];
        $product->discount_for_more = (int)$values['discount_for_more'];
        $product->easy_start = (boolean)$values['easy_start'];
        $product->easy_start_price = (int)$values['easy_start_price'];
        $product->easy_start_step = (int)$values['easy_start_step'];
        $product->not_discount = (boolean)$values['not_discount'];
        $product->save();

        //Insert translations
        $insertData = $this->getOnlyFullTranslations($requestData,'product_id', $product->id, ['description','composition']);
        ProductTranslations::query()->where('product_id',$product->id)->delete();
        ProductTranslations::query()->insert($insertData);

        //Update images
        if (count($values['product_images']) > 0){
            foreach ($values['product_images'] as $image_id => $image){
                $has_image = ProductImages::query()->find($image_id);

                if (is_null($has_image)){
                    $image_path = time() . $image->getClientOriginalName();
                    $image->move('main/products',$image_path);
                    ProductImages::query()->create([
                        'product_id' => $product->id,
                        'image' => $image_path
                    ]);
                } else{
                    @unlink(public_path('main/products/' . $has_image->image));
                    $image_path = time() . $image->getClientOriginalName();
                    $image->move('main/products',$image_path);
                    $has_image->update(['image' => $image_path]);
                }
            }
        }

        //Update statuses
        if(count($values['statuses']) > 0){
            $insertStatuses = [];
            foreach ($values['statuses'] as $status_id){
                array_push($insertStatuses,[
                    'product_id' => $product->id,
                    'status_id' => $status_id
                ]);
            }
            ProductStatuses::query()->where('product_id',$product->id)->delete();
            ProductStatuses::query()->insert($insertStatuses);
        }

        return redirect(route('locale.admin.product.index',$locale))->with('sweet_alert', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $locale
     * @param int $id
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy($locale, $id)
    {
        $product = Product::query()->findOrFail($id);

        $product->delete();

        return redirect(route('locale.admin.product.index',$locale))->with('sweet_alert', 'deleted');
    }

    public function deleteImage($locale, $product_id, $image){
        $product = Product::query()->with('images')->find($product_id);
        $product_image = ProductImages::query()->where([
            'id' => $image,
            'product_id' => $product->id,
        ])->first();

        if (is_null($product) || is_null($product_image)){
            return response()->json(['success' => false]);
        }

        @unlink(public_path('main/products/' . $product_image->image));
        $product_image->delete();
        return response()->json(['success' => true]);
    }
}
