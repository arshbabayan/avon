<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login($locale){
        if (Auth::guest() || Auth::user()->role_id !== 9){
            return view('admin.login');
        }
        else{
            return redirect(route('locale.admin.orders.index',['locale' => $locale, 'type' => 'finished']));
        }
    }

    public function sign(Request $request,$locale){
        $validator = Validator::make($request->all(),[
            'email' => 'required|max:255',
            'password' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('invalid_admin',true)->withInput($request->input());
        }
        $data = array(
            'email'  => $request->get('email'),
            'password'  => $request->get('password')
        );
        if (Auth::attempt($data) && Auth::user()->role_id == 9) {
            return redirect(route('locale.admin.orders.index',['locale' => $locale, 'type' => 'finished']));
        }
        Auth::logout();
        return redirect()->back()->with('invalid_admin',true)->withInput($request->input());
    }


    public function logout($locale){
        Auth::logout();
        return redirect(route('locale.admin.login',$locale));
    }
}
