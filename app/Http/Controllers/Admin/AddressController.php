<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Address;
use App\Models\AddressTranslations;
use App\Traits\CrudActions;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class AddressController extends Controller
{
    use CrudActions;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $addresses = Address::query()->whereHas('translates', function($q) use($keyword){
                return $q->where('title', 'LIKE', "%$keyword%");
            })->latest()->paginate($perPage);
        } else {
            $addresses = Address::query()->latest()->paginate($perPage);
        }

        return view('admin.address.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @param $locale
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request,$locale)
    {
        $values = $request->get($this->default['prefix']);
        $values['percent'] = floatval($request->get('percent'));
        $values['embed'] = $request->get('embed');

        $rules = [
            'title' => 'required|max:255',
            'percent' => 'required|min:0',
            'embed' => 'required',
        ];

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $address = new Address();
        $address->percent = $values['percent'];
        $address->embed = $values['embed'];
        $address->save();

        $insertData = $this->getOnlyFullTranslations($request->all(),'address_id',$address->id);
        AddressTranslations::query()->insert($insertData);

        return redirect(route('locale.admin.address.index',$locale))->with('sweet_alert', 'added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  int  $locale
     *
     * @return View
     */
    public function show($locale,$id)
    {
        $address = Address::query()->findOrFail($id);

        return view('admin.address.show', compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param  int  $locale
     *
     * @return View
     */
    public function edit($locale,$id)
    {
        $address = Address::query()->findOrFail($id);

        return view('admin.address.edit', compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $locale
     * @param int $id
     *
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, $locale, $id)
    {
        $address = Address::query()->findOrFail($id);

        $values = $request->get($this->default['prefix']);
        $values['percent'] = floatval($request->get('percent'));
        $values['embed'] = $request->get('embed');

        $rules = [
            'title' => 'required|max:255',
            'percent' => 'required|min:0',
            'embed' => 'required',
        ];

        $validate = Validator::make($values,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $address->percent = $values['percent'];
        $address->embed = $values['embed'];
        $address->save();

        $insertData = $this->getOnlyFullTranslations($request->all(),'address_id',$address->id);
        AddressTranslations::query()->where('address_id', $address->id)->delete();
        AddressTranslations::query()->insert($insertData);

        return redirect(route('locale.admin.address.index',$locale))->with('sweet_alert', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $locale
     * @param int $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($locale, $id)
    {
        Address::destroy($id);

        return redirect(route('locale.admin.address.index',$locale))->with('sweet_alert', 'deleted');
    }
}
