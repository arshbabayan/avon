<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $perPage = 25;
        $sliders = Slider::query()->latest()->paginate($perPage);
        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request,$locale)
    {
        $validate = Validator::make($request->all(),[
            'image' => 'image|mimes:jpeg,png,jpg,gif|required|max:10000',
        ]);

        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $image_path = time() . $request->file('image')->getClientOriginalName();
        $request->file('image')->move('main/sliders',$image_path);

        $slider = new Slider();
        $slider->image = $image_path;
        $slider->save();

        return redirect(route('locale.admin.slider.index',$locale))->with('sweet_alert', 'added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function show($locale,$id)
    {
        $slider = Slider::query()->findOrFail($id);

        return view('admin.slider.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function edit($locale,$id)
    {
        $slider = Slider::query()->findOrFail($id);

        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $locale, $id)
    {
        $slider = Slider::query()->findOrFail($id);

        if ($request->hasFile('image')){
            $validate = Validator::make($request->all(),[
                'image' => 'image|mimes:jpeg,png,jpg,gif|required|max:10000',
            ]);

            if ($validate->fails()){
                return redirect()->back()->withErrors($validate)->withInput($request->input());
            }

            @unlink(public_path('main/sliders/' . $slider->image));
            $image_path = time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('main/sliders',$image_path);

            $slider->image = $image_path;
            $slider->save();

            return redirect(route('locale.admin.slider.index',$locale))->with('sweet_alert', 'updated');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($locale, $id)
    {
        $slider = Slider::query()->findOrFail($id);
        @unlink(public_path('main/sliders/' . $slider->image));
        $slider->delete();
        return redirect(route('locale.admin.slider.index',$locale))->with('sweet_alert', 'deleted');
    }
}
