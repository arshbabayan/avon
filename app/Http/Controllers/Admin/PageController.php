<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Catalog;
use App\Models\Page;
use App\Models\PageProducts;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, $locale, $catalog_id)
    {
        $perPage = 25;
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $pages = Page::query()->where('catalog_id',$catalog->id)->withCount('products')->paginate($perPage);
        return view('admin.page.index', compact('catalog','pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($locale, $catalog_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $products = Product::all();
        return view('admin.page.create',compact('catalog','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request,$locale, $catalog_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $products = $request->get('products') ?? [];

        $validate = Validator::make($request->all(),[
            'image' => 'image|mimes:jpeg,png,jpg,gif|required|max:10000',
        ]);

        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }

        $image_path = time() . $request->file('image')->getClientOriginalName();
        $request->file('image')->move('main/pages',$image_path);

        $page = new Page();
        $page->catalog_id = $catalog->id;
        $page->image = $image_path;
        $page->save();

        //Insert products
        $insertData = [];
        foreach ($products as $product_id){
            array_push($insertData,[
                'page_id' => $page->id,
                'product_id' => $product_id
            ]);
        }
        PageProducts::query()->insert($insertData);

        return redirect(route('locale.admin.catalog.page.index',['locale' => $locale, 'catalog' => $catalog_id]))->with('sweet_alert', 'added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($locale,$catalog_id,$page_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $page = Page::query()->where([
            'id' => $page_id,
            'catalog_id' => $catalog_id,
        ])->with('products', 'products.images')->firstOrFail();

        return view('admin.page.show', compact('catalog','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($locale,$catalog_id,$page_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $page = Page::query()->where([
            'id' => $page_id,
            'catalog_id' => $catalog_id,
        ])->with('products')->firstOrFail();
        $products = Product::all();

        return view('admin.page.edit', compact('catalog','page', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $locale, $catalog_id, $page_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $page = Page::query()->where([
            'id' => $page_id,
            'catalog_id' => $catalog_id,
        ])->firstOrFail();

        $products = $request->get('products') ?? [];

        if ($request->hasFile('image')){
            $validate = Validator::make($request->all(),[
                'image' => 'image|mimes:jpeg,png,jpg,gif|required|max:10000',
            ]);

            if ($validate->fails()){
                return redirect()->back()->withErrors($validate)->withInput($request->input());
            }

            @unlink(public_path('main/pages/' . $page->image));
            $image_path = time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('main/pages',$image_path);

            $page->image = $image_path;
            $page->save();
        }

        //Update products
        $updateData = [];
        foreach ($products as $product_id){
            array_push($updateData,[
                'page_id' => $page->id,
                'product_id' => $product_id
            ]);
        }
        PageProducts::query()->where('page_id',$page->id)->delete();
        PageProducts::query()->insert($updateData);

        return redirect(route('locale.admin.catalog.page.index',['locale' => $locale, 'catalog' => $catalog_id]))->with('sweet_alert', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($locale, $catalog_id, $page_id)
    {
        $catalog = Catalog::query()->findOrFail($catalog_id);
        $page = Page::query()->where([
            'id' => $page_id,
            'catalog_id' => $catalog_id,
        ])->firstOrFail();

        @unlink(public_path('main/pages/' . $page->image));

        $page->delete();

        return redirect(route('locale.admin.catalog.page.index',['locale' => $locale, 'catalog' => $catalog_id]))->with('sweet_alert', 'deleted');
    }
}
