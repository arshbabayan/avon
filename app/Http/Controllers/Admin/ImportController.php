<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\ImportProducts;
use App\Models\Catalog;
use App\Models\Page;
use App\Models\PageProducts;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductStatuses;
use App\Models\ProductTranslations;
use App\Services\ImportService;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use ZanySoft\Zip\Zip;
use Illuminate\Support\Facades\Validator;
use App\Traits\CrudActions;

class ImportController extends Controller
{
    use CrudActions;

    public function index(){
        $requiredHeader = ImportService::getRequiredHeader();
        $catalogs = Catalog::all();
        return view('admin.import.index', compact('requiredHeader', 'catalogs'));
    }

    public function importing(Request $request, $locale, ImportService $importService){
        try{
            ini_set('max_execution_time', 6000);

            $validate = Validator::make($request->all(),[
                'catalog' => 'required',
                'import_file' => 'required|mimes:xlsx',
                'products_images' => 'mimes:zip',
                'pages_images' => 'mimes:zip',
            ]);

            if ($validate->fails()){
                return redirect()->back()->withErrors($validate)->withInput($request->input());
            }

            $sheet = Excel::toArray(new ImportProducts, $request->file('import_file'));

            $importService->setProductsImages($request->file('products_images'));
            $importService->setPagesImages($request->file('pages_images'));
            $importService->setSheet($sheet);

            if ($importService->invalid()){
                return redirect()->back()->with($importService->getErrors());
            }

            $catalog_id = $request->get('catalog');
            $importData = $importService->getImport();
            $pages = [];

            $files = [
                'pages' => [],
                'products' => [],
            ];

            //Extract pages files
            if ($request->hasFile('pages_images')){
                $zip = Zip::open($request->file('pages_images'));
                $zip->extract(public_path('extract/pages'));
                $files['pages'] = $zip->listFiles();
                $zip->close();
            }

            //Extract products files
            if ($request->hasFile('products_images')){
                $zip = Zip::open($request->file('products_images'));
                $zip->extract(public_path('extract/products'));
                $files['products'] = $zip->listFiles();
                $zip->close();
            }

            //Sort pages
            usort($files['pages'], function ($a, $b) {
                return (int)$a - (int)$b;
            });

            //Sort products
            usort($files['products'], function ($a, $b) {
                return (int)$a - (int)$b;
            });

            //Insert pages
            if (count($files['pages']) > 0){
                foreach ($files['pages'] as $pageFile){
                    $pageName = (string) Str::uuid() . $pageFile;
                    copy(public_path('extract/pages/' . $pageFile), public_path('main/pages/' . $pageName));
                    $page = new Page();
                    $page->catalog_id = $catalog_id;
                    $page->image = $pageName;
                    $page->save();
                    $pages[$pageFile] = $page->id;
                }
            }

            //Insert products
            foreach ($importData as $data){
                //Create product
                $product = new Product();
                $product->slug = $data['fields']['slug'];
                $product->articul = $data['fields']['articul'];
                $product->price = $data['fields']['price'];
                $product->discount = $data['fields']['discount'];
                $product->discount_for_more = $data['fields']['discount_for_more'];
                $product->easy_start = $data['fields']['easy_start'];
                $product->easy_start_price = $data['fields']['easy_start_price'];
                $product->easy_start_step = $data['fields']['easy_start_step'];
                $product->not_discount = $data['fields']['not_discount'];
                $product->save();

                //Insert translations
                $insertData = $this->getOnlyFullTranslations($data['translates'],'product_id', $product->id, ['description','composition']);
                ProductTranslations::query()->insert($insertData);

                //Insert statuses
                if (count($data['statuses']) > 0) {
                    $insertStatuses = [];
                    foreach ($data['statuses'] as $status_id){
                        array_push($insertStatuses,[
                            'product_id' => $product->id,
                            'status_id' => $status_id
                        ]);
                    }
                    ProductStatuses::query()->insert($insertStatuses);
                }

                //Insert images
                if (count($data['products_images']) > 0) {
                    $productsImages = [];
                    foreach ($data['products_images'] as $productFile){
                        $productName = (string) Str::uuid() . $productFile;
                        copy(public_path('extract/products/' . $productFile), public_path('main/products/' . $productName));
                        array_push($productsImages,[
                            'product_id' => $product->id,
                            'image' => $productName
                        ]);
                    }
                    ProductImages::query()->insert($productsImages);
                }

                //Insert images
                if (count($data['pages_images']) > 0) {
                    $pageProducts = [];
                    foreach ($data['pages_images'] as $page){
                        array_push($pageProducts,[
                            'page_id' => $pages[$page],
                            'product_id' => $product->id,
                        ]);
                    }
                    PageProducts::query()->insert($pageProducts);
                }
            }

            File::deleteDirectory(public_path('extract'));

            return redirect(route('locale.admin.import.index',$locale))->with('sweet_alert', 'imported');
        } catch (\Exception $exception){
            dd($exception->getMessage());
            return redirect(route('locale.admin.import.index',$locale))->with([
                'sweet_alert' => 'something_is_wrong',
                'sweet_type' => 'warning',
            ]);
        }
    }
}
