<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Information;
use App\Models\InformationTranslations;
use App\Traits\CrudActions;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    use CrudActions;

    public function index($locale){
        $information = Information::all();
        return view('admin.information.index',compact('information'));
    }

    public function show($locale, $prefix){
        $information = Information::query()->where('prefix',$prefix)->firstOrFail();
        return view('admin.information.show',compact('information'));
    }

    public function update(Request $request, $locale, $prefix){
        $information = Information::query()->where('prefix',$prefix)->firstOrFail();
        $requestData = $request->except('_token','_method');
        $informationData = $informationTranslationsData = [];

        foreach ($requestData as $key => $field){
            if (in_array($key,$this->languages->pluck('prefix')->toArray())){
                $informationTranslationsData[$key] = $field;
            } else{
                $informationData[$key] = $field;
            }
        }

        //Inner data
        $informationData = $this->cleanFloara($informationData,[],array_keys($informationData));
        $updateInformationData = [
            'info' => count($informationData) > 0 ? json_encode($informationData) : json_encode((Object)[])
        ];
        $information->update($updateInformationData);

        //Translations
        if (count($informationTranslationsData) > 0){
            $informationTranslationsData = $this->cleanFloara($informationTranslationsData,array_keys($informationTranslationsData[$this->default['prefix']]),[]);
            $updateInformationTranslationsData = [];
            foreach ($this->languages as $language){
                array_push($updateInformationTranslationsData,[
                    'information_id' => $information->id,
                    'language_id' => $language->id,
                    'info' => json_encode($informationTranslationsData[$language['prefix']])
                ]);
            }

            InformationTranslations::query()->where('information_id',$information->id)->delete();
            InformationTranslations::query()->insert($updateInformationTranslationsData);
        }

        return redirect(route('locale.admin.information.show',['locale' => $locale, 'prefix' => $information['prefix']]))->with('sweet_alert', 'updated');
    }
}
