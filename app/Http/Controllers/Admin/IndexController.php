<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class IndexController extends Controller
{
    public function personalDetails(){
        return view('admin.personal-details');
    }

    public function updateAdmin(Request $request, $locale){
        $requestData = $request->only('new_password','retry_password','current_password');

        $rules = [
            'new_password' => 'required|min:6|max:20',
            'retry_password' => 'required|min:6|max:20',
        ];

        $validate = Validator::make($requestData,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate);
        }

        if ($requestData['new_password'] !== $requestData['retry_password']){
            return redirect()->back()->with([
                'password_error' => trans('admin.unequal_passwords')
            ]);
        }

        $same_passwords = Hash::check($requestData['new_password'],Auth::user()->password);
        if ($same_passwords){
            return redirect()->back()->with([
                'password_error' => trans('admin.same_passwords')
            ]);
        }

        $check = Hash::check($requestData['current_password'],Auth::user()->password);
        if ($check){
            User::query()->where('id',Auth::id())->update([
                'password' => Hash::make($requestData['new_password'])
            ]);
            return redirect()->back()->with('sweet_alert', 'updated');
        }
        else{
            return redirect()->back()->with([
                'password_error' => trans('admin.wrong_password')
            ]);
        }

    }

    public function redactorImageUpload(Request $request){
        $path = time() . $request->file('image')->getClientOriginalName();
        $request->file('image')->move(public_path('main/images'),$path);
        return response()->json(['link' => '/main/images/' . $path]);
    }
}
