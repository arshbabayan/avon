<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('main.mails.order',['order' => $this->data['order']])
//            ->attach($this->data['pdf'], [
//                'as' => date('Y-m-d H:i:s') . '.pdf',
//                'mime' => 'application/pdf'
//            ])
            ->subject($this->data['subject'])
            ->replyTo($this->data['reply']);
    }
}
