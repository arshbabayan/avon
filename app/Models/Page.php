<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['catalog_id', 'image'];

    public function catalog(){
        return $this->hasOne(Catalog::class,'id','catalog_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class,'page_products','page_id','product_id');
    }

}
