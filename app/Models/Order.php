<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'catalog_id',
        'status',
        'payed',
        'will_be_pay',
        'address_id',
        'products_with_discount_total',
        'affordable_discount',
        'products_with_discount_finally_total',
        'products_not_discount_total',
        'products_not_discount_finally_total',
        'products_finally_total',
        'bonus',
        'order_total',
        'transportation_costs',
        'service_fee',
        'packaging',
        'journal',
        'finally_total',
        'used_prepayment',
        'unused_discount',
        'unused_prepayment',
        'in_processed_at',
        'payed_at',
        'finished_at',
        'canceled_at',
    ];

    public function catalog(){
        return $this->hasOne(Catalog::class, 'id','catalog_id');
    }

    public function user(){
        return $this->hasOne(User::class, 'id','user_id')->withTrashed();
    }

    public function address(){
        return $this->hasOne(Address::class, 'id','address_id');
    }

    public function items(){
        return $this->hasMany(OrderProducts::class, 'order_id','id');
    }

    public function withDiscountProducts(){
        return $this->hasMany(OrderProducts::class, 'order_id','id')->where('not_discount', false)->where('returned', false);
    }

    public function notDiscountProducts(){
        return $this->hasMany(OrderProducts::class, 'order_id','id')->where('not_discount', true)->where('returned', false);
    }

    public function returnedProducts(){
        return $this->hasMany(OrderProducts::class, 'order_id','id')->where('returned', true)->orderByDesc('returned_at');
    }
}
