<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'product_id',
        'price',
        'count',
        'not_discount',
        'returned',
        'returned_at'
    ];

    public function order(){
        return $this->hasOne(Order::class, 'id','order_id');
    }

    public function product(){
        return $this->hasOne(Product::class, 'id','product_id')->withTrashed();
    }
}
