<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['percent', 'embed'];

    public function translates(){
        return $this->hasMany(AddressTranslations::class, 'address_id','id');
    }

    public function translations($lang_id){
        return $this->hasOne(AddressTranslations::class, 'address_id','id')->where('language_id',$lang_id)->first();
    }

    public function translate($attr){
        $current = Language::query()->where('prefix',app()->getLocale())->first()->id;
        $default = Language::query()->where('prefix',config('app.fallback_locale'))->first()->id;
        return $this->translations($current)->{$attr} ?? $this->translations($default)->{$attr};
    }
}
