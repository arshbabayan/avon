<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationTranslations extends Model
{
    public $timestamps = false;

    protected $fillable = ['language_id', 'information_id', 'info'];
}
