<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStatuses extends Model
{
    public $timestamps = false;

    protected $fillable = ['product_id', 'status_id'];
}
