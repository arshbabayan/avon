<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageProducts extends Model
{
    public $timestamps = false;

    protected $fillable = ['page_id', 'product_id'];
}
