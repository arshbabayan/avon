<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    use SoftDeletes;

    protected $appends = ['info'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'articul',
        'price',
        'discount',
        'discount_for_more',
        'easy_start',
        'easy_start_price',
        'easy_start_step',
        'not_discount'
    ];

    public function translates(){
        return $this->hasMany(ProductTranslations::class, 'product_id','id');
    }

    public function translations($lang_id){
        return $this->hasOne(ProductTranslations::class, 'product_id','id')->where('language_id',$lang_id)->first();
    }

    public function translate($attr){
        $current = Language::query()->where('prefix',app()->getLocale())->first()->id;
        $default = Language::query()->where('prefix',config('app.fallback_locale'))->first()->id;
        return $this->translations($current)->{$attr} ?? $this->translations($default)->{$attr};
    }

    public function images(){
        return $this->hasMany(ProductImages::class, 'product_id','id');
    }

    public function pages(){
        return $this->belongsToMany(Page::class,'page_products','product_id','page_id');
    }

    public function statuses(){
        return $this->belongsToMany(Status::class,'product_statuses','product_id','status_id');
    }

    public function topImage(){
        if (count($this['images']) > 0){
            return asset('/main/products/' . $this['images'][0]->image);
        } else{
            return '/main/assets/img/image-placeholder.png';
        }
    }

    public function price($count){
        return $this->_prices($count)->price;
    }

    public function discount($count){
        return $this->_prices($count)->discount;
    }

    public function doublePrices($count){
        return $this->_prices($count)->double_prices;
    }

    private function _prices($count) {
        $price = $this['price'];
        $discount = $this['discount'];
        $double_prices = true;

        if ($this['discount'] === 0) {
            $double_prices = false;
            $discount = $this['price'];
        }
        if ($count === 1) {
            if ($this['discount'] > 0) {
                $double_prices = true;
                $price = $this['price'];
                $discount = $this['discount'];
            }
        } else {
            if ($this['discount_for_more'] > 0) {
                $double_prices = true;
                $price = $this['discount'] > 0 ? $this['discount'] : $this['price'];
                $discount = $this['discount_for_more'];
            }
        }

        if (Auth::user() && Auth::user()->easyStart() && $this['easy_start'] && Auth::user()->easyStartStep() === $this['easy_start_step']) {
            $double_prices = false;
            $discount = $this['easy_start_price'];
        }

        return (Object)[
            'price' => $price,
            'discount' => $discount,
            'double_prices' => $double_prices,
        ];
    }

    public function getInfoAttribute(){
        return [
            'title' => $this->translate('title'),
            'description' => $this->translate('description'),
            'composition' => $this->translate('composition'),
        ];
    }

}
