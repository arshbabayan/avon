<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'information';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['prefix','info'];

    public function translates(){
        return $this->hasMany(InformationTranslations::class, 'information_id','id');
    }

    public function translations($lang_id){
        return $this->hasOne(InformationTranslations::class, 'information_id','id')->where('language_id',$lang_id)->first();
    }

    public function translate($attr){
        $current = Language::query()->where('prefix',app()->getLocale())->first()->id;
        $default = Language::query()->where('prefix',config('app.fallback_locale'))->first()->id;
        return $this->translations($current)->{$attr} ?? $this->translations($default)->{$attr};
    }
}
