<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressTranslations extends Model
{
    public $timestamps = false;

    protected $fillable = ['language_id', 'address_id', 'title'];
}
