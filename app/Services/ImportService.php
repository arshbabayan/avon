<?php

namespace App\Services;

use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Traits\CrudActions;

class ImportService{
    use CrudActions;

    protected $header = [];
    protected $rows = [];
    protected $articuls = [];
    protected $equalArticuls = [];
    protected $productsImages = null;
    protected $pagesImages = null;
    protected $headerExcepts = [];
    protected $invalidRows = [];
    protected $missedProductsImages = [];
    protected $missedPagesImages = [];
    protected $import = [];

    public function setSheet(array $sheet){
        $handle = $sheet[0] ?? [];
        foreach ($handle as $index => $row){
            if (!$this->header) {
                $this->header = $row;
                $this->validateHeader($this->header);
            } else {
                $this->rows[] = $row;
                $this->validateRow($row, $index);
            }
        }
    }

    public function setProductsImages($file){
        $this->productsImages = $file;
    }

    public function setPagesImages($file){
        $this->pagesImages = $file;
    }

    public function validateHeader($header){
        foreach ($this->getRequiredHeader() as $key => $item){
            if ($item !== trim($header[$key])){
                $this->headerExcepts[] = $item;
            }
        }
    }

    public function validateRow($row, $index){
        if (!array_filter($row)){
            return;
        }

        $translates = [
            'am' => [
                'title' => $row[8],
                'description' => $row[11],
                'composition' => $row[14],
            ],
            'ru' => [
                'title' => $row[9],
                'description' => $row[12],
                'composition' => $row[15],
            ],
            'en' => [
                'title' => $row[10],
                'description' => $row[13],
                'composition' => $row[16],
            ],
        ];

        $translates = $this->cleanFloara($translates,['description', 'composition']);
        $statuses = Status::query()->whereIn('prefix',explode(', ', $row[18]))->get()->pluck('id')->toArray();
        $values = $translates[((new Controller())->default['prefix'])];
        $values['slug'] = Str::slug($values['title']);
        $values['articul'] = $row[0];
        $values['price'] = $row[1];
        $values['discount'] = $row[2];
        $values['discount_for_more'] = $row[3];
        $values['easy_start'] = !is_null($row[4]);
        $values['easy_start_price'] = is_null($row[4]) ? 0 : (is_null($row[5]) ? 500 : $row[5]);
        $values['easy_start_step'] = is_null($row[4]) ? 0 : (is_null($row[6]) ? 1 : $row[6]);
        $values['not_discount'] = !is_null($row[7]);

        $rules = [
            'articul' => 'required|max:255',
            'title' => 'required|max:255',
        ];

        $validate = Validator::make($values,$rules);

        //If Columns error
        if ($validate->fails()){
            $errors = [];
            foreach ($validate->errors()->messages() as $column => $messages){
                $errors[$column] = implode(' ', $messages);
            }
            $this->invalidRows[$index] = $errors;
        }

        //If equal articuls
        if (in_array($values['articul'], $this->articuls)){
            array_push($this->equalArticuls, $values['articul']);
        } else{
            array_push($this->articuls, $values['articul']);
        }

        //If images missing
        $this->missedProductsImages = array_merge($this->missedProductsImages, $this->missedAndExistsFiles($row[17], $this->productsImages)['missing']);
        $this->missedPagesImages = array_merge($this->missedPagesImages, $this->missedAndExistsFiles($row[19], $this->pagesImages)['missing']);

        //Import
        $this->import[] = [
            'fields' => [
                'slug' => (string)$values['slug'],
                'articul' => (int)$values['articul'],
                'price' => (int)$values['price'],
                'discount' => (int)$values['discount'],
                'discount_for_more' => (int)$values['discount_for_more'],
                'easy_start' => (boolean)$values['easy_start'],
                'easy_start_price' => (int)$values['easy_start_price'],
                'easy_start_step' => (int)$values['easy_start_step'],
                'not_discount' => (boolean)$values['not_discount'],
            ],
            'translates' => $translates,
            'statuses' => $statuses,
            'products_images' => $this->missedAndExistsFiles($row[17], $this->productsImages)['exists'],
            'pages_images' => $this->missedAndExistsFiles($row[19], $this->pagesImages)['exists'],
        ];
    }

    public function invalid(): bool
    {
        return in_array(true, [
            count($this->header) === 0,
            count($this->rows) === 0,
            count($this->headerExcepts) > 0,
            count($this->invalidRows) > 0,
            count($this->missedProductsImages) > 0,
            count($this->missedPagesImages) > 0,
            count($this->equalArticuls) > 0,
        ]);
    }

    public function getErrors(): array
    {
        return [
            'noHeader' => count($this->header) === 0,
            'noRows' => count($this->rows) === 0,
            'headerExcepts' => $this->headerExcepts,
            'invalidRows' => $this->invalidRows,
            'missedProductsImages' => array_unique($this->missedProductsImages),
            'missedPagesImages' => array_unique($this->missedPagesImages),
            'equalArticuls' => array_unique($this->equalArticuls),
        ];
    }

    public function getHeader(): array
    {
        return $this->header;
    }

    public function getRows(): array
    {
        return $this->rows;
    }

    public function getImport(): array
    {
        return $this->import;
    }

    public static function getRequiredHeader(): array
    {
        return [
            'Артикул',
            'Цена',
            'Цена со скидкой',
            'Цена со скидкой, если выбрано более одного',
            'Легкий старт',
            'Цена легкого старта',
            'Шаг легкого старта',
            'Не рассчитать во время основной скидок',
            'Заголовок AM',
            'Заголовок RU',
            'Заголовок EN',
            'Описание AM',
            'Описание RU',
            'Описание EN',
            'Состав AM',
            'Состав RU',
            'Состав EN',
            'Изображения',
            'Статусы',
            'Страница'
        ];
    }
}
