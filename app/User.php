<?php

namespace App;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\Order;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'address',
        'passport',
        'phone',
        'balance',
        'blocked',
        'verified',
        'easy_start',
        'easy_start_step',
        'bonus',
        'max_percent',
        'inactive_catalogs_count',
        'register_catalog',
        'email',
        'code',
        'password',
        'coordinator_id',
        'role_id',
        'email_token',
        'verified_at',
        'blocked_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function coordinator()
    {
        return $this->hasOne(User::class, 'id', 'coordinator_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    public function team()
    {
        return $this->hasMany($this, 'coordinator_id', 'id')->withTrashed();
    }

    public function representatives()
    {
        return $this->hasMany($this, 'coordinator_id', 'id')->where('role_id', 1)->withTrashed();
    }

    public function easyStart(){
        return $this['easy_start'] === 'active';
    }

    public function easyStartStep(){
        return (int)$this['easy_start_step'];
    }

    public function registerCatalog()
    {
        return $this->hasOne(Catalog::class, 'id', 'register_catalog')->first();
    }

    public function status($catalog_id){
        if ($this->easyStart() === 'active'){
            return trans('report.start') . ' ' . $this->easyStartStep();
        } else{
            if ($this->registerCatalog()->id === $catalog_id){
                return trans('report.new_user');
            }
            if ($this->inactive_catalogs_count > 0){
                return trans('report.inactive_catalogs',['count' => $this->inactive_catalogs_count]);
            }
        }
    }

    public function teamMinTradeCount($catalog_id){
        $more_than_1 = 0;
        $more_than_2 = 0;

        foreach ($this->representatives as $person){
            if ($person->trade($catalog_id) >= 5000 && $person->trade($catalog_id) <= 14999){
                $more_than_1++;
            } else if ($person->trade($catalog_id) >= 15000){
                $more_than_2++;
            }
        }
        return [
            'more_than_1' => $more_than_1,
            'more_than_2' => $more_than_2,
        ];
    }

    public function trade($catalog_id){
        $orders = $this->orders()->where([
            'catalog_id' => $catalog_id,
            'status' => 2
        ])->get();
        $trade = 0;
        foreach ($orders as $order){
            $trade += $order->order_total;
        }
        return $trade;
    }

    public function teamIds($team){
        $ids = [];
        foreach ($team as $child){
            $ids[] = $child->id;
            if (count($child->team) > 0){
                $ids = array_merge($ids, $this->teamIds($child->team));
            }
        }
        return $ids;
    }

    public function teamTrade($catalog_id, $bonus = false){
        $ids = $this->teamIds($this['team']);
        $team = User::query()->whereIn('id', $ids);

        if ($bonus){
            $team = $team->where([
                'coordinator_id' => $this['id'],
                'role_id' => 1
            ]);
        }

        $team = $team->get();

        $trade = $this->trade($catalog_id);
        foreach ($team as $person){
            $trade += $person->trade($catalog_id);
        }
        return $trade;
    }

    public function activePersons($count, $catalog_id){
        if (count($this->team) === 0){
            return false;
        }

        $actives = 0;
        $has = [];
        foreach ($this->team as $person){
            if ($person->trade($catalog_id) >= 15000){
                $actives++;
                if ($actives <= $count){
                    array_push($has, $person->id);
                }
            }
        }

        if (count($this->team) > $count * 2){
            $single = false;
            foreach ($this->team as $person){
                if ($person->trade($catalog_id) >= 5000 && !in_array($person->id, $has)){
                    $single = true;
                    break;
                }
            }
            return $single && $actives >= $count;
        } else{
            return $actives >= $count;
        }
    }

    public function turnover($catalog_id){
        $percent = 0;
        $role_id = 1;
        $bonus = 0;

        if ($this->trade($catalog_id) >= 15000){
            if ($this->activePersons(100, $catalog_id) && $this->teamTrade($catalog_id) >= 5000000){
                $percent = 8;
                $role_id = 8;
            } elseif ($this->activePersons(75, $catalog_id) && $this->teamTrade($catalog_id) >= 3000000){
                $percent = 7;
                $role_id = 7;
            } elseif ($this->activePersons(50, $catalog_id) && $this->teamTrade($catalog_id) >= 1800000) {
                $percent = 6;
                $role_id = 6;
            } elseif ($this->activePersons(30, $catalog_id) && $this->teamTrade($catalog_id) >= 960000) {
                $percent = 5;
                $role_id = 5;
            } elseif ($this->activePersons(15, $catalog_id) && $this->teamTrade($catalog_id) >= 480000) {
                $percent = 4;
                $role_id = 4;
            } elseif ($this->activePersons(8, $catalog_id) && $this->teamTrade($catalog_id) >= 240000) {
                $percent = 3;
                $role_id = 3;
            } elseif ($this->activePersons(3, $catalog_id) && $this->teamTrade($catalog_id) >= 90000) {
                $percent = 2;
                $role_id = 2;
            }
        }

        if ($percent > 0){
            $bonus = ($percent * $this->teamTrade($catalog_id, true)) / 100;
        }

        return [
            'percent' => $percent,
            'role_id' => $role_id,
            'bonus' => $bonus,
        ];
    }

    public static function updateUsersEasyStart()
    {
        $supportTrade = 15000;
        $supportLimit = 4;

        $users = self::query()->where('role_id', '!=', 9)->whereIn('easy_start',  ['start', 'active'])->get();
        $catalog_id = (new Controller())->active_catalog->id;

        $activeUsersIds = [];
        $inactiveUsersIds = [];

        foreach ($users as $user){
            if ($user->trade($catalog_id) >= $supportTrade && $user->easyStartStep() <= $supportLimit){
                array_push($activeUsersIds, $user['id']);
            } else{
                array_push($inactiveUsersIds, $user['id']);
            }
        }

        //Activate users
        self::query()->whereIn('id', $activeUsersIds)->update([
            'easy_start' => 'active',
        ]);

        foreach ($activeUsersIds as $activeUsersId){
            $user = User::query()->where('id', $activeUsersId)->first();
            $new_step = $user->easy_start_step + 1;
            $user->easy_start_step = $new_step;
            $user->save();
        }

        //Inactivate users
        self::query()->whereIn('id', $inactiveUsersIds)->update([
            'easy_start' => 'inactive',
        ]);
    }

    public static function updateUsersBonus(){
        $users = self::query()->where('role_id', '!=', 9)->get();
        $catalog_id = (new Controller())->active_catalog->id;
        foreach ($users as $user){
            $turnover = $user->turnover($catalog_id);

            $updateData = [
                'bonus' => $turnover['bonus'],
                'max_percent' => $turnover['percent'] > $user->max_percent ? $turnover['percent'] : $user->max_percent,
                'role_id' => $turnover['role_id'],
            ];
            if ($user->trade($catalog_id) > 0){
                $updateData['inactive_catalogs_count'] = 0;
            } else{
                $updateData['inactive_catalogs_count'] = $user->inactive_catalogs_count + 1;
            }

            $user->update($updateData);
        }
    }
}
