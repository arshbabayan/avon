$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Logout
    $('body').on('click','.nav a.logout', function(e){
        e.preventDefault();
        let href = $(this).data('href');
        swal({
            text: trans('logout_text'),
            type: 'success',
            showCancelButton: true,
            confirmButtonText: trans('yes'),
            cancelButtonText: trans('no'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        }).then(() => {
            window.location.href = href;
        }, (dismiss) => {}).catch(swal.loop);
    });


    //Floara
    let floara_values = [];
    function refreshFloara(){
        new FroalaEditor(".floara-textarea", {
            imageUploadURL: '/' + $('html').attr('lang') + '/admin/redactor-image-upload',
            imageUploadMethod: 'POST',
            imageUploadParam: 'image',
            requestHeaders: {
                'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
            },
            events: {
                'keyup': function (keyupEvent) {
                    floara_values[this.$oel[0].name] = this.$el[0].innerHTML;
                }
            }
        });
        setTimeout(function () {
            for (let name in floara_values){
                let textarea = $("body .floara-textarea[name='" + name + "']");
                let floara = $("body .floara-textarea[name='" + name + "']").parents('.floara').find('.fr-view');
                textarea.html(floara_values[name]);
                floara.html(floara_values[name]);
            }
        },1);
    }

    refreshFloara();

    $(".refresh-floara").click(function () {
        refreshFloara();
    });


    //Multi select
    $('.selectpicker').selectpicker();


    //Multiple images
    //Add multiple item
    $("body").on('click','.add_multiple_item',function () {
        let custom = $(".custom_multiple_area .multiple_item").clone();
        let new_item = custom;
        let name = $('.multiple_items').data('name');
        let number = $('.multiple_items .multiple_item').last().data('item') + 1;
        new_item.attr('data-item',number);
        new_item.find('.fileinput input').attr('name',name + '[' + number + ']');
        $(this).before(new_item);
    });

    //Delete item in create
    $("body").on('click','.multiple_item .delete_multiple_item_create',function () {
        $(this).parents('.multiple_item').remove();
    });

    //Delete item in edit
    $("body").on('click','.multiple_item .delete_multiple_item_edit',function () {
        let url = $(this).data('url');
        let multiple_item = $(this).parents('.multiple_item');

        if (url){
            swal({
                title: trans('are_you_sure'),
                text: trans('return_is_failed'),
                type: 'success',
                showCancelButton: true,
                confirmButtonText: trans('yes'),
                cancelButtonText: trans('no'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
            }).then(() => {
                $.post(url, function(response) {
                    if (response.success){
                        swal({
                            type:'success',
                            title:trans('deleted'),
                            showConfirmButton:false,
                            timer:2000
                        }).catch(swal.loop);
                        multiple_item.remove();
                    } else{
                        swal({
                            type:'warning',
                            title:trans('can_not_delete_last_product_image'),
                            showConfirmButton:false,
                            showCancelButton: false,
                            timer:4000
                        }).catch(swal.loop);
                    }
                });
            }, (dismiss) => {}).catch(swal.loop);
        } else{
            multiple_item.remove();
        }
    });

    //Other dashboard js

    //Easy start
    function easyStart(){
        if ($('.easy-start input').is(':checked')){
            $('.easy-start-price').slideDown(500);
            $('.easy-start-step').slideDown(500);
        } else{
            $('.easy-start-price').slideUp(500);
            $('.easy-start-step').slideUp(500);
        }
    }
    easyStart();
    $('body').on('change', '.easy-start input', function () {
        easyStart();
    })
});
