$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // ===== Scroll to Top ====
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) {
            $('#return-to-top').fadeIn(200);
        } else {
            $('#return-to-top').fadeOut(200);
        }
    });

    $('#return-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });

    $('.page').click(function () {
        $(this).removeClass('no-anim').toggleClass('flipped');
        $('.page > div').click(function (e) {
            e.stopPropagation();
        });
        reorder()
    });
    function reorder() {
        $(".book").each(function () {
            var pages = $(this).find(".page")
            var pages_flipped = $(this).find(".flipped")
            pages.each(function (i) {
                $(this).css("z-index", pages.length - i)
            })
            pages_flipped.each(function (i) {
                $(this).css("z-index", i + 1)
            })
        });
    }
    reorder();

    $(".basket-top-area").click(function () {
        $(".basket-card").fadeToggle(500);
    });

    $('.multiple-items').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        dots: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    dots: true,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    dots: true,
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
});
