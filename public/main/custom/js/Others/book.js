jQuery('#fb5').data('config', {
    "page_width": "550",
    "page_height": "715",
    "email_form": "office@somedomain.com",
    "zoom_double_click": "1",
    "zoom_step": "0.06",
    "double_click_enabled": "true",
    "tooltip_visible": "true",
    "toolbar_visible": "true",
    "gotopage_width": "30",
    "deeplinking_enabled": "true",
    "rtl": "false",
    'full_area': 'true',
    'lazy_loading_thumbs': 'false',
    'lazy_loading_pages': 'false'
});
