$(function () {
    let modal = $("#message-modal");

    //Edit profile
    $("#profile-edit-form-submit").click(function (e) {
        e.preventDefault();
        let data = $("#profile-edit-form-data").serializeArray();
        $.ajax({
            url:'/' + $("html").attr('lang') + '/profile/update',
            type:'post',
            dataType:'json',
            data:data,
            beforeSend: function() {
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                $("#profile-edit-form-data small").html("");
                if (response.success){
                    window.location.href = '/' + $("html").attr('lang') + '/profile/edit';
                } else{
                    for (let field in response.errors){
                        $("#profile-edit-form-data small." + field).html(response.errors[field][0]);
                    }
                }
            },
            error: function(){
                $("#profile-edit-form-data small").html("");
                modal.find('.message-title').html(trans('something_wrong'));
                modal.find('.message-text').html(trans('please_try_later'));
                modal.modal('show');
            },
            complete: function() {
                $('.preloader').css('display','none');
            }
        })
    });

    //Add new row
    $("body").on('click', '.products-table .pr-add', function () {
        let custom = $(".products-table .custom-pr-item").clone();
        custom.removeClass('custom-pr-item');
        custom.removeClass('d-none');
        custom.addClass('pr-item');
        $(".products-table .last-tr").before(custom);
    });

    //Delete row
    $("body").on('click', '.products-table .pr-delete', function () {
        $(this).parents('.pr-item').remove();
    });

    //Get product by articul
    $("body").on('keyup', '.products-table .pr-articul',function () {
        let articul = $(this).val();
        let item = $(this).parents('.pr-item');

        $.ajax({
            url:'/' + $("html").attr('lang') + '/profile/get-product/' + articul,
            type:'post',
            dataType:'json',
            beforeSend: function() {
                item.find('.pr-preloader').css('display','flex');
            },
            success:function (response) {
                if (response.success){
                    item.find('.pr-count').attr('data-id', response.product_id);
                    item.find('.pr-title').val(response.title);
                } else{
                    item.find('.pr-title').val(trans('product_exist'));
                }
            },
            complete: function() {
                item.find('.pr-preloader').css('display','none');
            },
        })
    });

    //Store in basket
    $("body").on('click', '.products-table .add-to-baskets',function () {
        let data = {
            products:{}
        };
        $(".products-table .pr-item .pr-count").each(function () {
            if (data.products[$(this).data('id')]){
                let current_count = data.products[$(this).data('id')];
                data.products[$(this).data('id')] = parseInt(current_count) + parseInt($(this).val());
            } else{
                data.products[$(this).data('id')] = parseInt($(this).val());
            }
        });

        $.ajax({
            url:'/' + $("html").attr('lang') + '/profile/store-to-basket',
            type:'post',
            data:data,
            dataType:'json',
            beforeSend: function() {
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                if (response.success){
                    $('.products-table .pr-item').remove();
                    Actions.updateBasketTopMenu(response);
                    modal.find('.message-title').html(trans('products_stored_in_basket'));
                    modal.find('.message-text').html($('<a href="/' + $("html").attr('lang') + '/basket"' + ' class="text-pink">' + trans('check_cart') + '</a>'));
                    modal.modal('show');
                } else{
                    modal.find('.message-title').html(trans('something_wrong'));
                    modal.find('.message-text').html(trans('please_try_later'));
                    modal.modal('show');
                }
            },
            complete: function() {
                $('.preloader').css('display','none');
            },
        })
    });

    //Open order
    $('body').on('click', '.open-order', function () {
        $(this).parents('.order-container').find('.order-content').slideToggle(500);
        $(this).html($(this).html().trim() === trans('open') ? trans('close') : trans('open'));
    });

    //Export report
    $("body").on('change','.catalogs-select',function () {
        if ($(this).val()){
            $('.download-report').removeAttr('disabled');
            $('.download-report-form').find('.catalog_id').val($(this).val());
        } else{
            $('.download-report').attr('disabled', 'disabled');
        }
    });

    //Download report
    $('body').on('click', '.download-report', function () {
        $('.download-report-form').submit();
    });
});
