$(function () {
    let products = {};
    let selected = {};

    //Get products for current page
    $('body').on('click', '#fb5-book .catalog-page',function () {
        let catalog_name = $('#fb5-book').data('catalog_name');
        let page_id = $(this).data('page_id');

        $.ajax({
            url:'/' + $("html").attr('lang') + '/catalog/' + catalog_name +'/get-page/' + page_id,
            type:'post',
            dataType:'json',
            success:function (response) {
                if (response.success){
                    let modal = $("#page-products-modal");

                    //Set content to global variables
                    products = response.products;
                    selected = {};

                    //Delete old data
                    modal.find('.modal-body .product-item').not('.product-custom').remove();
                    modal.find('.modal-body hr.bg-pink').remove();

                    if (Object.keys(products).length > 0){
                        let last_item = 0;
                        let custom = null;

                        for (let id in products){
                            if (products.hasOwnProperty(id)){
                                //For don`t add last hr
                                last_item++;

                                //Select default one item
                                selected[id] = 1;

                                Product.setData(products[id]);

                                //Add new product
                                custom = $("#page-products-modal .product-custom").clone();
                                custom.find('.product-image').attr('src', Product.topImage());
                                custom.find('.product-articul').html(Product.articul());
                                custom.find('.product-title').html(Product.translate('title'));
                                custom.find('.product-count').val(selected[Product.id()]);

                                //Print prices
                                custom.find('.product-price').html(Product.price(1));
                                custom.find('.product-discount').html(Product.discount(1));

                                //Show or hide product price
                                if (Product.doublePrices(1)){
                                    custom.find('.product-price-area').removeClass('d-none');
                                } else{
                                    custom.find('.product-price-area').addClass('d-none');
                                }

                                //Params for getting product
                                custom.attr('data-product', Product.id());
                                custom.find('.product-to-basket').data('product', Product.id());

                                //Delete hidden classes
                                custom.removeClass('product-custom');
                                custom.removeClass('d-none');

                                //Add product and hr in modal
                                modal.find('.modal-body').append(custom);
                                if (Object.keys(products).length > 1 && Object.keys(products).length !== last_item){
                                    modal.find('.modal-body').append($('<hr class="bg-pink my-4">'));
                                }
                            }
                        }

                        if (Object.keys(products).length > 1){
                            modal.modal('show');
                        } else{
                            custom.find('.product-image').click();
                        }
                    }
                }
            },
        })
    });

    //Show product more info
    $('body').on('click', '#page-products-modal .product-image',function () {
        let modal_2 = $("#show-product-modal");
        let id = $(this).parents('.product-item').data('product');
        let count = selected[id];
        Product.setData(products[id]);

        //Make default
        modal_2.find('.product-images-slider').html('');
        modal_2.find('.product-images-inner').html('');
        modal_2.find('.product-price-area').removeClass('d-none');
        modal_2.find('.product-info ul li a').removeClass('active');
        modal_2.find('.product-info ul li:first-child a').addClass('active');
        modal_2.find('.product-info .tab-content > div').removeClass('show');
        modal_2.find('.product-info .tab-content > div').removeClass('active');
        modal_2.find('.product-info .tab-content > div:first-child').addClass('show');
        modal_2.find('.product-info .tab-content > div:first-child').addClass('active');

        //Set product images
        if (Product.images().length > 0){
            for (let i = 0; i < Product.images().length; i++){
                modal_2.find('.product-images-slider').append($('<li><a class="ns-img" href="/main/products/' + Product.images()[i].image + '"></a></li>'));
                modal_2.find('.product-images-inner').append($('<li><a class="thumb" href="/main/products/' + Product.images()[i].image + '"></a><span>' + (i + 1) + '</span></li>'));
            }
        } else{
            modal_2.find('.product-images-slider').append($('<li><a class="ns-img" href="' + Product.topImage() + '"></a></li>'));
            modal_2.find('.product-images-inner').append($('<li><a class="thumb" href="' + Product.topImage() + '"></a><span>' + (1) + '</span></li>'));
        }
        initSlider();

        //Set product info
        modal_2.find('.product-articul').html(Product.articul());
        modal_2.find('.product-title').html(Product.translate('title'));
        modal_2.find('.product-count').val(count);
        modal_2.find('.product-description').html(Product.translate('description'));
        modal_2.find('.product-composition').html(Product.translate('composition'));
        modal_2.find('.product-reviews').html();

        //Print prices
        modal_2.find('.product-price').html(Product.price(count));
        modal_2.find('.product-discount').html(Product.discount(count));

        //Show or hide product price
        if (Product.doublePrices(count)){
            modal_2.find('.product-price-area').removeClass('d-none');
        } else{
            modal_2.find('.product-price-area').addClass('d-none');
        }

        //Params for getting product
        modal_2.find('.product-item').attr('data-product',id);
        modal_2.find('.product-to-basket').data('product',id);

        modal_2.modal('show');
    });

    //Product count change + -
    $('body').on('click', '.product-count-change',function () {
        let count = parseInt($(this).parents('.num-counter').find('.product-count').val());
        let id = $(this).parents('.num-counter').find('.product-to-basket').data('product');
        let product_item = $('#page-products-modal .product-item[data-product=' + id + ']');
        let product_item_show = $('#show-product-modal .product-item[data-product=' + id + ']');

        //Set count
        if ($(this).data('type') === 'plus'){
            count++;
        } else{
            count--;
            if (count < 1) count = 1;
        }
        selected[id] = count;
        product_item.find('.product-count').val(count);
        product_item_show.find('.product-count').val(count);

        Product.setData(products[id]);

        //Print prices
        product_item.find('.product-price').html(Product.price(count));
        product_item.find('.product-discount').html(Product.discount(count));
        product_item_show.find('.product-price').html(Product.price(count));
        product_item_show.find('.product-discount').html(Product.discount(count));

        //Show or hide product price
        if (Product.doublePrices(count)){
            product_item.find('.product-price-area').removeClass('d-none');
            product_item_show.find('.product-price-area').removeClass('d-none');
        } else{
            product_item.find('.product-price-area').addClass('d-none');
            product_item_show.find('.product-price-area').addClass('d-none');
        }
    });

    //Add to basket
    $('body').on('click', '.product-to-basket',function () {
        let data = {
            product_id: $(this).data('product'),
            count: $(this).parents('.num-counter').find('.product-count').val(),
            type: 'add'
        };

        $.ajax({
            url:'/' + $("html").attr('lang') + '/basket/update',
            type:'post',
            dataType:'json',
            data:data,
            success:function (response) {
                if (response.success){
                    Actions.updateBasketTopMenu(response);
                }
            },
        })
    });
});
