$(function () {
    //Remove from basket
    $('body').on('click', '.product-remove-basket',function () {
        let tr = $(this).parents('.basket-item');

        let data = {
            product_id: tr.data('product'),
            count: $(this).parents('.num-counter').find('.basket-item-count').val(),
            type: 'remove'
        };

        $.ajax({
            url:'/' + $("html").attr('lang') + '/basket/update',
            type:'post',
            dataType:'json',
            data:data,
            success:function (response) {
                if (response.success){
                    tr.remove();
                    tr.remove();
                    Basket.updatePage();
                }
            },
        })
    });

    //Product count change + -
    $('body').on('click', '.product-count-change',function () {
        let count = parseInt($(this).parents('.num-counter').find('.basket-item-count').val());
        let tr = $(this).parents('.basket-item');

        if ($(this).data('type') === 'plus'){
            count++;
        } else{
            count--;
            if (count < 1) count = 1;
        }

        let data = {
            product_id: tr.data('product'),
            count: count,
            type: 'update'
        };

        $.ajax({
            url:'/' + $("html").attr('lang') + '/basket/update',
            type:'post',
            dataType:'json',
            data:data,
            success:function (response) {
                if (response.success){
                    Product.setData(response.products[data.product_id]);
                    tr.find('.basket-item-count').val(count);
                    tr.find('.basket-item-price').html(Product.price(count));
                    tr.find('.basket-item-discount').html(Product.discount(count));
                    tr.find('.basket-item-total').html(count * Product.discount(count));

                    if (Product.doublePrices(count)){
                        tr.find('.basket-item-price-area').removeClass('d-none');
                    } else{
                        tr.find('.basket-item-price-area').addClass('d-none');
                    }

                    Basket.updatePage();
                }
            },
        });
    });

    //Select address
    $('body').on('change', '.delivery_point_area input', function () {
        Basket.updatePage();
    });

    //Select journal
    $('body').on('change', '.select_journal_area input', function () {
        Basket.updatePage();
    });

    //Checkout
    $('body').on('click', '#confirm_order',function () {
        let modal = $("#message-modal");

        $.ajax({
            url:'/' + $("html").attr('lang') + '/checkout',
            type:'post',
            dataType:'json',
            data: {
                address: Basket.getAddress(),
                journal: Basket.getJournal(),
            },
            beforeSend: function() {
                $('#checkout_order').modal('hide');
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                let title = '';
                let text = '';

                if (response.success){
                    //Clear basket in page and top menu
                    let basket_menu = $(".basket-menu");
                    let basket_items = $(".basket-menu .basket-items");

                    //Delete old data
                    basket_items.find('.basket-item').not('.basket-custom').remove();
                    basket_items.find('hr.bg-pink').remove();

                    //Set count, text and total
                    basket_menu.find(".basket-count").html('');
                    basket_menu.find(".basket-full-empty-text").html(trans('your_basket_is_empty'));
                    basket_menu.find(".basket-total").html(0);
                    basket_menu.find('.basket-content-area').addClass('d-none');
                    $(".basket-card").fadeOut(500);

                    //Clear basket page content
                    $('.basket-full').remove();
                    $('.basket-empty').removeClass('d-none');

                    //Set messages
                    title = trans('order_success_title');
                    text = trans('order_success_text');
                }
                else{
                    title = trans('attention');
                    if (response.error === 'max_size_error'){
                        text = trans('max_size_error') + ' ' + '<span class="color-pink font-weight-bold">' + response.size + ' ' + trans('dram') + '</span>';
                    }
                    else if (response.error === 'has_debt'){
                        text = trans('has_debt');
                    }
                    else{
                        text = trans('please_try_later');
                    }
                }

                modal.find('.message-title').html(title + '!');
                modal.find('.message-text').html(text);
                modal.modal('show');
            },
            error: function(){
                modal.find('.message-title').html(trans('something_wrong'));
                modal.find('.message-text').html(trans('please_try_later'));
                modal.modal('show');
            },
            complete: function() {
                $('.preloader').css('display','none');
            }
        })
    });
});
