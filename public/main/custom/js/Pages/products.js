$(function () {
    //Product count change + -
    $('body').on('click', '.product-count-change',function () {
        let count = parseInt($(this).parents('.num-counter').find('.product-count').val());
        let item = $(this).parents('.product-item');
        let params = item.find('.params');
        let product = {
            id: parseInt(params.find('.real-id').html()),
            price: parseInt(params.find('.real-price').html()),
            discount: parseInt(params.find('.real-discount').html()),
            discount_for_more: parseInt(params.find('.real-discount-for-more').html()),
            easy_start: params.find('.real-easy-start').html() !== '0',
            easy_start_step: parseInt(params.find('.real-easy-start-step').html()),
            easy_start_price: parseInt(params.find('.real-easy-start-price').html())
        };

        //Set count
        if ($(this).data('type') === 'plus'){
            count++;
        } else{
            count--;
            if (count < 1) count = 1;
        }
        item.find('.product-count').val(count);

        Product.setData(product);

        //Print prices
        item.find('.product-price').html(Product.price(count));
        item.find('.product-discount').html(Product.discount(count));

        //Show or hide product price
        if (Product.doublePrices(count)){
            item.find('.product-price-area').removeClass('d-none');
            item.find('.num-counter').removeClass('m-top-34');
        } else{
            item.find('.product-price-area').addClass('d-none');
            item.find('.num-counter').addClass('m-top-34');
        }
    });

    //Add to basket
    $('body').on('click', '.product-to-basket',function () {
        let item = $(this).parents('.product-item');
        let data = {
            product_id: item.find('.real-id').html(),
            count: item.find('.product-count').val(),
            type: 'add'
        };

        $.ajax({
            url:'/' + $("html").attr('lang') + '/basket/update',
            type:'post',
            dataType:'json',
            data:data,
            success:function (response) {
                if (response.success){
                    Actions.updateBasketTopMenu(response);
                }
            },
        })
    });
});
