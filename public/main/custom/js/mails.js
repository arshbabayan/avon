$(function () {
    let modal = $("#message-modal");

    //Contact us
    $("#contact-us-form-submit").click(function (e) {
        e.preventDefault();
        let data = $("#contact-us-form-data").serializeArray();
        $.ajax({
            url:'/' + $("html").attr('lang') + '/contact-us-send',
            type:'post',
            dataType:'json',
            data:data,
            beforeSend: function() {
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                $("#contact-us-form-data small").html("");
                if (response.success){
                    $("#contact-us-form-data input, #contact-us-form-data textarea").val("");
                    modal.find('.message-title').html(trans('contact_us_success_title'));
                    modal.find('.message-text').html(trans('contact_us_success_text'));
                    modal.modal('show');
                } else{
                    for (let field in response.errors){
                        $("#contact-us-form-data small." + field).html(response.errors[field][0]);
                    }
                }
            },
            error: function(){
                $("#contact-us-form-data input, #contact-us-form-data textarea").val("");
                $("#contact-us-form-data small").html("");
                modal.find('.message-title').html(trans('something_wrong'));
                modal.find('.message-text').html(trans('please_try_later'));
                modal.modal('show');
            },
            complete: function() {
                $('.preloader').css('display','none');
            }
        })
    });

    //Registration
    $("#registration-form-submit").click(function (e) {
        e.preventDefault();
        let data = $("#registration-form-data").serializeArray();
        let privacy_policy = $("#registration-form-data #privacy_policy");
        $.ajax({
            url:'/' + $("html").attr('lang') + '/registration-send',
            type:'post',
            dataType:'json',
            data:data,
            beforeSend: function() {
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                $("#registration-form-data small").html("");
                if (response.success){
                    $("#registration-form-data input").val("");
                    privacy_policy.val("1");
                    modal.find('.message-title').html(trans('dear') + ' ' + data[0]['value'] + ' ' + data[1]['value']);
                    modal.find('.message-text').html(trans('registration_success_text'));
                    modal.modal('show');
                } else{
                    for (let field in response.errors){
                        let error_text = response.errors[field][0];
                        if (field === 'privacy_policy'){
                            error_text = trans('privacy_policy_error')
                        }
                        $("#registration-form-data small." + field).html(error_text);
                    }
                }
            },
            error: function(){
                $("#registration-form-data small").html("");
                $("#registration-form-data input").val("");
                privacy_policy.val("1");
                modal.find('.message-title').html(trans('something_wrong'));
                modal.find('.message-text').html(trans('please_try_later'));
                modal.modal('show');
            },
            complete: function() {
                $('.preloader').css('display','none');
            }
        })
    });

    //Sign in
    $("#sign-in-form-submit").click(function (e) {
        e.preventDefault();
        let data = $("#sign-in-form-data").serializeArray();
        $.ajax({
            url:'/' + $("html").attr('lang') + '/sign-in',
            type:'post',
            dataType:'json',
            data:data,
            beforeSend: function() {
                $('.preloader').css('display','flex');
            },
            success:function (response) {
                if (response.success){
                    window.location.href = '/' + $("html").attr('lang') + '/profile';
                } else{
                    let sign_in_message = $("#sign-in-form-data .sign-in-message");
                    sign_in_message.html(response.message);
                    sign_in_message.slideDown(500);
                }
            },
            complete: function() {
                $('.preloader').css('display','none');
            }
        })
    });
});
