class Actions{
    static array_sum(array) {
        let sum = 0;
        for (let s_key in array){
            if (array.hasOwnProperty(s_key)){
                sum += array[s_key];
            }
        }
        return sum;
    }

    static updateBasketTopMenu(response, show = true) {
        let basket_cache = response.cache;
        let basket_products = response.products;
        let basket_count = this.array_sum(response.cache);

        let basket_menu = $(".basket-menu");
        let basket_items = $(".basket-menu .basket-items");

        //Delete old data
        basket_items.find('.basket-item').not('.basket-custom').remove();
        basket_items.find('hr.bg-pink').remove();

        //Set count, text and total
        basket_menu.find(".basket-count").html(basket_count > 0 ? basket_count : '');
        basket_menu.find(".basket-full-empty-text").html(basket_count > 0 ? trans('you_added_to_cart') : trans('your_basket_is_empty'));

        if (Object.keys(basket_products).length > 0){
            let last_item = 0;
            for (let id in basket_products) {
                if (basket_products.hasOwnProperty(id)){
                    let basket_item = basket_items.find(".basket-custom").clone();
                    Product.setData(basket_products[id]);

                    //For don`t add last hr
                    last_item++;

                    //Add new product
                    basket_item.find('.basket-item-image').attr('src', Product.topImage());
                    basket_item.find('.basket-item-image').attr('alt', Product.translate('title'));
                    basket_item.find('.basket-item-image').attr('title', Product.translate('title'));
                    basket_item.find('.basket-item-title').html(Product.translate('title'));
                    basket_item.find('.basket-item-total').html(basket_cache[Product.id()]);

                    //Print prices
                    basket_item.find('.basket-item-discount').html(Product.discount(basket_cache[Product.id()]));

                    //Delete hidden classes
                    basket_item.removeClass('basket-custom');
                    basket_item.removeClass('d-none');

                    //Add hr after basket item
                    basket_items.find(".basket-custom").after(basket_item);
                    if (Object.keys(basket_products).length > 0 && Object.keys(basket_products).length !== last_item){
                        basket_item.before($('<hr class="bg-pink">'));
                    }
                }
            }

            basket_menu.find('.basket-content-area').removeClass('d-none');
            if (show){
                $(".basket-card").fadeIn(500);
            }
        }
        else{
            basket_menu.find('.basket-content-area').addClass('d-none');
            $(".basket-card").fadeOut(500);
        }
    }
}

class Product {
    product = {};

    static setData(product) {
        this["product"] = product;
    }

    static id() {
        return this["product"]["id"];
    }

    static articul() {
        return this["product"]["articul"];
    }

    static translate(property) {
        return this["product"]["info"][property];
    }

    static images() {
        return this["product"]["images"];
    }

    static topImage(){
        if (this["product"].images.length > 0){
            return '/main/products/' + this["product"].images[0].image;
        } else{
            return '/main/assets/img/image-placeholder.png';
        }
    }

    static price(count) {
        return this._prices(count)["price"];
    }

    static discount(count) {
        return this._prices(count)["discount"];
    }

    static doublePrices(count){
        return this._prices(count)["double_prices"];
    }

    static easyStart(){
        return this["product"]["easy_start"] === 1;
    }

    static easyStartStep(){
        return this["product"]["easy_start_step"];
    }

    static _prices(count) {
        let price = this["product"].price;
        let discount = this["product"].discount;
        let double_prices = true;

        if (this["product"].discount === 0) {
            double_prices = false;
            discount = this["product"].price;
        }
        if (count === 1) {
            if (this["product"].discount > 0) {
                double_prices = true;
                price = this["product"].price;
                discount = this["product"].discount;
            }
        } else {
            if (this["product"].discount_for_more > 0) {
                double_prices = true;
                price = this["product"].discount > 0 ? this["product"].discount : this["product"].price;
                discount = this["product"].discount_for_more;
            }
        }

        if (User.easyStart() && this.easyStart() && User.easyStartStep() === this.easyStartStep()) {
            double_prices = false;
            discount = this["product"]["easy_start_price"];
        }

        return {
            price: price,
            discount: discount,
            double_prices: double_prices,
        };
    }
}

class User{
    easy_start = false;
    easy_start_step = 0;

    static setUser(){
        $.ajax({
            url:'/' + $("html").attr('lang') + '/set-user',
            type:'post',
            dataType:'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:(response) => {
                this["easy_start"] = response.easy_start;
                this["easy_start_step"] = response.easy_start_step;
            },
        })
    }

    static easyStart(){
        return this["easy_start"];
    }

    static easyStartStep(){
        return parseInt(this["easy_start_step"]);
    }

    static run(){
        this.setUser();
    }
}

class Basket{
    static getAddress(){
        let address = null;
        $('.delivery_point_area input').each(function () {
            if ($(this).is(":checked")){
                address = $(this).val();
            }
        });
        return address;
    }

    static getJournal(){
        let journal = false;
        $('.select_journal_area input').each(function () {
            if ($(this).is(":checked")){
                journal = $(this).val() === 'yes';
            }
        });
        if (journal){
            $('.final_calculation .journal-area').removeClass('d-none');
        } else{
            $('.final_calculation .journal-area').addClass('d-none');
        }
        return journal;
    }

    static updatePage(){
        $.ajax({
            url:'/' + $("html").attr('lang') + '/update-basket-page',
            dataType:'json',
            type: 'post',
            data:{
                address: this.getAddress(),
                journal: this.getJournal(),
            },
            success:function (response) {
                Actions.updateBasketTopMenu(response, false);
                if (Actions.array_sum(response['cache']) > 0){
                    $('.basket-count').html(Actions.array_sum(response['cache']));
                    $('.products_with_discount_total').html(response['products_with_discount_total']);
                    $('.affordable_discount').html(response['affordable_discount']);
                    $('.products_with_discount_finally_total').html(response['products_with_discount_finally_total']);
                    $('.products_not_discount_finally_total').html(response['products_not_discount_finally_total']);
                    $('.products_finally_total').html(response['products_finally_total']);
                    $('.basket_bonus').html(response['basket_bonus']);
                    $('.transportation_costs').html(response['transportation_costs']);
                    $('.service_fee').html(response['service_fee']);
                    $('.packaging').html(response['packaging']);
                    $('.journal').html(response['journal']);
                    $('.finally_total').html(response['finally_total']);

                    if (response['products_with_discount'].length === 0){
                        $('.products_with_discount').remove();
                    }
                    if (response['products_not_discount'].length === 0){
                        $('.products_not_discount').remove();
                    }
                } else{
                    $('.basket-full').remove();
                    $('.basket-empty').removeClass('d-none');
                }
            },
        })
    }
}

User.run();
